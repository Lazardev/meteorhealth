-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 09, 2016 at 09:42 PM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meteorhealth`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `user_id` int(11) NOT NULL,
  `Benign_Prostatic_Hyperplasia` int(11) NOT NULL,
  `Breast_Cancer` int(11) NOT NULL,
  `Colorectal_Cancer` int(11) NOT NULL,
  `Diabetes` int(11) NOT NULL,
  `Erectile_Dysfunction` int(11) NOT NULL,
  `Healthy` int(11) NOT NULL,
  `No_Conditions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`user_id`, `Benign_Prostatic_Hyperplasia`, `Breast_Cancer`, `Colorectal_Cancer`, `Diabetes`, `Erectile_Dysfunction`, `Healthy`, `No_Conditions`) VALUES
(2, 25, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `count` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `tasker` varchar(50) NOT NULL,
  `message` varchar(3500) NOT NULL,
  `chat_at` datetime NOT NULL,
  `status` varchar(35) NOT NULL,
  `message_tasker` varchar(3500) NOT NULL,
  `status_task` varchar(35) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`count`, `user`, `tasker`, `message`, `chat_at`, `status`, `message_tasker`, `status_task`) VALUES
(1, '7', '9', 'Hi', '2016-03-25 17:53:21', 'read', '', ''),
(2, '12', '13', 'Hi,Are u intersted in this trial', '2016-04-04 07:24:18', 'read', '', ''),
(3, '12', '13', '', '2016-04-04 07:29:08', 'new', '', ''),
(4, '7', '9', 'Would you like to come in for the initial screening next Thursday, March 24th? ', '2016-04-05 19:20:14', 'new', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_status` varchar(255) NOT NULL,
  `city_timezone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` longtext NOT NULL,
  `country_currency` varchar(255) NOT NULL,
  `country_currency_code` varchar(255) NOT NULL,
  `country_status` smallint(1) NOT NULL,
  `country_image` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deactivate_report`
--

CREATE TABLE IF NOT EXISTS `deactivate_report` (
  `deactivate_report_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `reason` text NOT NULL,
  `deactivate_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deactivate_report`
--

INSERT INTO `deactivate_report` (`deactivate_report_id`, `user_id`, `reason`, `deactivate_date_time`) VALUES
(1, 5, 'adfafa', '2016-03-09 06:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `email_message`
--

CREATE TABLE IF NOT EXISTS `email_message` (
  `email_message_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_newsletter`
--

CREATE TABLE IF NOT EXISTS `email_newsletter` (
  `email_newsletter_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `email_set_id` int(11) NOT NULL,
  `email_set_type` longtext NOT NULL,
  `email_set_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(10) NOT NULL,
  `payment_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `user_id`, `job_id`, `job_name`, `transaction_id`, `amount`, `payment_timestamp`, `payment_method`, `status`) VALUES
(1, 3, 14, 'Healthy', 'h6fxws', 505, '2016-05-04 18:30:00', 'credits', 'Success'),
(2, 3, 14, 'Healthy', '3t7y3c', 505, '2016-05-04 18:30:00', 'credits', 'Success'),
(3, 3, 14, 'Healthy', 'jstsws', 505, '2016-05-04 18:30:00', 'Braintree', 'Success'),
(4, 3, 14, 'Healthy', '38pnbs', 505, '2016-05-04 18:30:00', 'Braintree', 'Success'),
(5, 3, 14, 'Healthy', 'bd3d3c', 505, '2016-05-04 18:30:00', 'Braintree', 'Success'),
(6, 3, 14, 'Healthy', 'd4m8dh', 505, '2016-05-04 18:30:00', 'Braintree', 'Success'),
(7, 3, 14, 'Healthy', '53hpw7', 505, '2016-05-04 18:30:00', 'Braintree', 'Success'),
(8, 3, 11, 'Diabetes', '7w7vsc', 450, '2016-05-04 18:30:00', 'Braintree', 'Success');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `jposter_id` int(10) NOT NULL,
  `jposted_date` date NOT NULL,
  `job_pay` int(2) NOT NULL,
  `jname` varchar(256) NOT NULL,
  `jdescription` longtext NOT NULL,
  `jact_status` smallint(1) NOT NULL COMMENT '1= Active, 2=Deactive',
  `jwork_status` smallint(1) NOT NULL COMMENT '1=new,2=assign,3=accept,4=conform,5=cancel,6=complete,7=incomplete,8=reject',
  `jbilling_by` smallint(1) NOT NULL COMMENT '1= Hour, 2= Day',
  `jbilling_no_hours` int(10) NOT NULL,
  `jbilling_no_days` int(10) NOT NULL,
  `jposter_price` int(10) NOT NULL,
  `jbidding_type` smallint(1) NOT NULL COMMENT '1=Open Bidding,2=Direct,3=First Bid',
  `jbidding_st_date` date NOT NULL COMMENT 'Open Bidding',
  `jbidding_ed_date` date NOT NULL COMMENT 'Open Bidding',
  `jassign_to_work_id` int(10) NOT NULL,
  `jassign_date` date NOT NULL,
  `jassign_amt` int(10) NOT NULL,
  `jaccept_workid` int(10) NOT NULL,
  `jaccept_date` date NOT NULL,
  `jaccept_amt` int(10) NOT NULL,
  `jconformed_date` date NOT NULL,
  `jcancel_date` date NOT NULL,
  `jcancel_msg` longtext NOT NULL,
  `jcomplete_date` date NOT NULL,
  `jcomplete_amt` int(10) NOT NULL,
  `jfeedback` longtext NOT NULL,
  `jposter_fav` smallint(1) NOT NULL COMMENT '1= enable',
  `jworker_fav` smallint(1) NOT NULL COMMENT '1= enable',
  `jreject_date` date NOT NULL,
  `jreject_msg` longtext NOT NULL,
  `jstar_rate` int(2) NOT NULL,
  `jaddress` varchar(50) NOT NULL,
  `jzipcode` int(20) NOT NULL,
  `jlatitude` varchar(35) NOT NULL,
  `jlongitude` varchar(50) NOT NULL,
  `jbidding_stt_date` date NOT NULL,
  `jbidding_edd_date` date NOT NULL,
  `rate` int(11) NOT NULL,
  `review` varchar(25) NOT NULL,
  `jdaddress` varchar(3500) NOT NULL,
  `djaddress` varchar(3500) NOT NULL,
  `djzipcode` varchar(35) NOT NULL,
  `djlongitude` varchar(25) NOT NULL,
  `djlatitude` varchar(25) NOT NULL,
  `jrefund` varchar(25) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `ethincity` varchar(75) NOT NULL,
  `target_veterans` varchar(25) NOT NULL,
  `target_twins` varchar(25) NOT NULL,
  `inclusion` varchar(250) NOT NULL,
  `exclusion` varchar(250) NOT NULL,
  `add_info` varchar(250) NOT NULL,
  `title_study` varchar(100) NOT NULL,
  `desc_study` varchar(350) NOT NULL,
  `duration` varchar(250) NOT NULL,
  `duration_types` varchar(35) NOT NULL,
  `inperson_visits` varchar(75) NOT NULL,
  `duration_visits` varchar(25) NOT NULL,
  `duration_time` varchar(25) NOT NULL,
  `total_compensation` varchar(25) NOT NULL,
  `travel_expense` varchar(35) NOT NULL,
  `zipcode` varchar(35) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `category_id`, `jposter_id`, `jposted_date`, `job_pay`, `jname`, `jdescription`, `jact_status`, `jwork_status`, `jbilling_by`, `jbilling_no_hours`, `jbilling_no_days`, `jposter_price`, `jbidding_type`, `jbidding_st_date`, `jbidding_ed_date`, `jassign_to_work_id`, `jassign_date`, `jassign_amt`, `jaccept_workid`, `jaccept_date`, `jaccept_amt`, `jconformed_date`, `jcancel_date`, `jcancel_msg`, `jcomplete_date`, `jcomplete_amt`, `jfeedback`, `jposter_fav`, `jworker_fav`, `jreject_date`, `jreject_msg`, `jstar_rate`, `jaddress`, `jzipcode`, `jlatitude`, `jlongitude`, `jbidding_stt_date`, `jbidding_edd_date`, `rate`, `review`, `jdaddress`, `djaddress`, `djzipcode`, `djlongitude`, `djlatitude`, `jrefund`, `gender`, `ethincity`, `target_veterans`, `target_twins`, `inclusion`, `exclusion`, `add_info`, `title_study`, `desc_study`, `duration`, `duration_types`, `inperson_visits`, `duration_visits`, `duration_time`, `total_compensation`, `travel_expense`, `zipcode`) VALUES
(1, 0, 8, '2016-03-14', 0, '0', '0', 1, 2, 1, 15, 0, 2, 3, '0000-00-00', '0000-00-00', 0, '2016-03-15', 32, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', '0', 'Native American', 'Yes', 'Yes', 'Good', 'Good', 'Good', 'Good', 'Good', '10', 'Days', '5', '15', 'Minutes', 'Yes', '0', '560102'),
(2, 3, 8, '2016-03-15', 0, '0', '0', 1, 1, 1, 15, 0, 10, 1, '2016-03-16', '2016-03-30', 6, '2016-03-15', 155, 1, '2016-04-17', 155, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'male', 'Native American', 'Yes', 'Yes', 'Good', 'Good', 'Good', 'Good', 'Good', '10', 'Days', '5', '15', 'Minutes', 'Yes', '0', '560102'),
(3, 3, 8, '2016-03-15', 0, 'Colorectal Cancer', '0', 1, 1, 1, 15, 0, 2, 1, '2016-03-16', '2016-03-31', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'male', 'Native American', 'Yes', 'Yes', 'Good', 'Good', 'Good', 'Good', 'Good', '5', 'Days', '3', '15', 'Minutes', 'Yes', '0', '560102'),
(4, 2, 7, '2016-03-16', 0, 'Breast Cancer', '0', 1, 5, 1, 5, 0, 10, 1, '1970-01-01', '1970-01-01', 9, '2016-03-28', 50, 9, '2016-03-29', 50, '0000-00-00', '2016-04-07', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', '0', 'Native American', 'Yes', 'Yes', '', '', '', '', '', '', 'Days', '', '', 'Minutes', 'Yes', '0', ''),
(5, 2, 7, '2016-03-22', 0, 'Breast Cancer', '0', 1, 1, 1, 10, 0, 50, 1, '2016-03-22', '2016-03-29', 9, '2016-03-28', 500, 4, '2016-04-09', 500, '0000-00-00', '2016-04-07', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Native American', 'Yes', 'Yes', 'Test Test Test', '', '', 'Test Trial 1', '', '10', 'Days', '', '30', 'Minutes', 'Yes', '0', '02163'),
(6, 1, 7, '2016-03-22', 0, 'Benign Prostatic Hyperplasia', '0', 1, 5, 2, 0, 2, 100, 3, '0000-00-00', '0000-00-00', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '2016-04-08', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Asian', 'Yes', 'Yes', '', '', '', 'Test 2', '', '', 'Days', '', '', 'Minutes', 'Yes', '0', ''),
(7, 4, 7, '2016-03-25', 0, 'Diabetes', '0', 1, 5, 1, 10, 0, 75, 3, '0000-00-00', '0000-00-00', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '2016-04-08', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Multiracial', 'No', 'No', 'Must have type 2 diabetes', 'Must not have hypertension', 'This trial will last for 6 months, and starts in May. ', 'Type 2 diabetes study', 'This study is looking to identify hte link between type 2 diabetes and certain genetic mutations.', '6', 'Months', '3', '30', 'Minutes', 'Yes', '0', '02138'),
(8, 5, 7, '2016-03-25', 0, 'Erectile Dysfunction', '0', 1, 5, 1, 5, 0, 200, 1, '2016-03-25', '2016-04-08', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '2016-03-25', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'male', 'Multiracial', 'No', 'No', 'Must have ED', 'Must not have hypertension', '', '', '', '', 'Days', '', '', 'Minutes', 'Yes', '0', ''),
(9, 5, 7, '2016-03-25', 0, 'Erectile Dysfunction', '0', 1, 5, 1, 20, 0, 10, 1, '2016-03-25', '2016-04-08', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '2016-04-07', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'male', 'Multiracial', 'No', 'No', 'Must have ED', 'Must not have hypertension', '', 'ED test trial 1', 'Impact of genetic mutations on ED incidence', '3', 'Months', '1', '30', 'Minutes', 'Yes', '0', '02138'),
(10, 5, 11, '2016-04-01', 0, 'Erectile Dysfunction', '0', 1, 1, 1, 10, 0, 15, 1, '1970-01-01', '1970-01-01', 0, '2016-04-10', 100, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', '0', 'Native American', 'Yes', 'Yes', '', '', '', '', '', '', 'Days', '', '', 'Minutes', 'Yes', '0', ''),
(11, 4, 12, '2016-04-04', 0, 'Diabetes', '0', 1, 1, 2, 0, 1, 500, 1, '2016-04-04', '2016-04-05', 13, '2016-04-04', 450, 13, '2016-04-04', 450, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Native American', 'Yes', 'Yes', 'test', 'test', 'test', 'test', 'test', '3', 'Days', '4', '5', 'Minutes', 'Yes', '0', '600006'),
(12, 1, 12, '2016-04-04', 0, 'Benign Prostatic Hyperplasia', '0', 1, 1, 1, 4, 0, 50, 3, '0000-00-00', '0000-00-00', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Native American', 'Yes', 'Yes', 'test2', 'test2', 'test2', 'test2', 'test2', '2', 'Days', '2', '3', 'Minutes', 'Yes', '0', '600006'),
(13, 6, 7, '2016-04-08', 0, 'Healthy', '0', 1, 1, 2, 0, 10, 50, 1, '2016-04-08', '2016-04-15', 0, '0000-00-00', 0, 0, '0000-00-00', 0, '0000-00-00', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'male', 'Caucasian', 'No', 'No', '', '', 'The purpose of this study is to assess the safety, tolerability of the different vaccine schedules with Ad26.Mos.HIV and Clade C Glycoprotein (gp) 140. For more information on this trial, please visit: https://clinicaltrials.gov/ct2/show/NCT02685020?', '', '', '', 'Days', '', '', 'Minutes', 'Yes', '0', ''),
(14, 6, 3, '2016-04-30', 1, 'Healthy', '0', 1, 1, 0, 0, 0, 0, 1, '2016-05-06', '2016-05-27', 1, '2016-04-30', 505, 2, '2016-05-05', 505, '2016-05-05', '0000-00-00', '', '0000-00-00', 0, '', 0, 0, '0000-00-00', '', 0, '', 0, '0', '0', '0000-00-00', '0000-00-00', 0, '', '', '', '', '0', '0', '', 'female', 'Native American', 'Yes', 'Yes', 'asdfdsf', 'adsfdsaf', 'adsfdasf', 'asdfdsaf', 'jkljlkjlk', '2', 'Days', '2', '2', 'Minutes', 'Yes', '0', '232433');

-- --------------------------------------------------------

--
-- Table structure for table `job_bids`
--

CREATE TABLE IF NOT EXISTS `job_bids` (
  `id` int(11) NOT NULL,
  `job_id` varchar(10) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `bid_amt` varchar(10) NOT NULL,
  `biding_dt` date NOT NULL,
  `bid_commands` text NOT NULL,
  `job_status` smallint(6) NOT NULL COMMENT '1=new,2=assign,3=accept,4=conform,5=cancel,6=complete,7=incomplete,8=reject'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_bids`
--

INSERT INTO `job_bids` (`id`, `job_id`, `userid`, `bid_amt`, `biding_dt`, `bid_commands`, `job_status`) VALUES
(1, '2', '1', '152', '2016-03-15', 'Nothing to say', 3),
(2, '2', '6', '155', '2016-03-15', 'Nothing to say', 2),
(3, '5', '4', '$50', '2016-03-22', 'I would like to do this trial', 3),
(4, '5', '9', '500', '2016-03-25', 'Please select me', 3),
(5, '4', '9', '50', '2016-03-25', '50', 3),
(6, '11', '13', '450', '2016-04-04', 'test2test2test2test2test2test2test2test2test2', 4),
(7, '10', '4', '100', '2016-04-10', 'please select  meee', 1),
(8, '14', '1', '505', '2016-04-30', 'adsfds', 2),
(9, '14', '2', '565', '2016-04-30', 'adsfds', 4);

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `category_id` int(11) NOT NULL,
  `category_job_type` int(11) NOT NULL,
  `category_parent_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_status` smallint(1) NOT NULL,
  `category_description` varchar(1500) NOT NULL,
  `category_image` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`category_id`, `category_job_type`, `category_parent_id`, `category_name`, `category_status`, `category_description`, `category_image`) VALUES
(1, 1, 0, 'Benign Prostatic Hyperplasia', 1, '', ''),
(2, 2, 0, 'Breast Cancer', 1, 'Medicated trials that seek patients with a pre-existing breast cancer condition.', '3499.png'),
(3, 2, 0, 'Colorectal Cancer', 1, '', ''),
(4, 2, 0, 'Diabetes', 1, 'Medicated trials that seek patients with a pre-existing diabetes condition.', '6483.png'),
(5, 2, 0, 'Erectile Dysfunction', 1, '', ''),
(6, 1, 0, 'Healthy', 1, 'Observational studies that do not require any pre-existing conditions.', '6733.png'),
(7, 2, 0, 'No Conditions', 1, 'These medicated trials require participants without any pre-existing conditions (healthy).', '13.png');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `default` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `label`, `status`, `default`) VALUES
(1, 'English', 'en', 1, 0),
(4, 'French', 'fr', 1, 0),
(5, 'Deutsch', 'de', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `user_id` int(11) NOT NULL,
  `customer_id` varchar(50) NOT NULL,
  `last_four` int(11) NOT NULL,
  `card_token` varchar(75) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sitesettings`
--

CREATE TABLE IF NOT EXISTS `sitesettings` (
  `settings_id` int(11) NOT NULL,
  `type` longtext NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sitesettings`
--

INSERT INTO `sitesettings` (`settings_id`, `type`, `description`) VALUES
(1, 'site_online', '2'),
(2, 'site_name', 'MeteorHealth'),
(3, 'site_slogan', 'Accelerating Medical Innovation'),
(4, 'site_slogan_Enable', '1'),
(5, 'site_logo', 'logo.png'),
(6, 'site_favicon', ''),
(7, 'site_title', 'Task Heroes'),
(8, 'site_keywords', 'job'),
(9, 'site_description', 'welcome to Task Heroes'),
(10, 'site_slideshow_Enable', '1'),
(11, 'site_offline_mess', 'The site is Under Construction'),
(12, 'facebook_link', 'http://www.facebook.com/meteorhealth'),
(13, 'twitter_link', 'https://twitter.com/@meteorhealth'),
(14, 'seo_status', '1'),
(15, 'seo_info', ''),
(16, 'facebook_status', '1'),
(17, 'facebook_profile', 'http://www.meteorhealth.com//home/facebook'),
(18, 'facebook_apikey', '1144879248879494'),
(19, 'facebook_applicationkey', '8570dbb6e38f3b1fc3761d69cb31799b'),
(20, 'facebook_link', 'http://www.facebook.com/meteorhealth'),
(21, 'twitter_link', 'https://twitter.com/@meteorhealth'),
(22, 'other_link', ''),
(23, 'other_link_image', ''),
(24, 'paypal_emailid', 'kathadvipul@gmail.com'),
(25, 'job_comission', '10'),
(26, 'mailgun_apikey', 'key-0-sa27gj-0qj93qrovcwdl7l1sw9fg81'),
(27, 'mailgun_version', 'api.mailgun.net/v2/'),
(28, 'mailgun_domain', 'sandbox0e20bcbc7450433995c40fc95b846c13.mailgun.org'),
(29, 'mailgun_fromemail', 'info@meteorhealth.com'),
(30, 'mailgun_replyemail', 'info@meteorhealth.com'),
(31, 'subscription_amount', '0'),
(32, 'subscription_period', '2'),
(33, 'subscription_status', 'Disable'),
(34, 'no_of_post', '2'),
(35, 'promo_code', 'Welcome'),
(36, 'promo_price', '15'),
(37, 'bt_merchant_id', '5kmzcvj59kdn8dm5'),
(38, 'bt_public_key', 'nrmk7gq8q7hdrdt3'),
(39, 'bt_private_key', '6d83795916b792ade7c56b650f92ea75'),
(40, 'bt_cse', 'MIIBCgKCAQEAxr6xsEYGx1CveTjcqbi4WaEsuDSTjLyn8q87FhuuJr9u12W3rt2zcA2eT9v1Qc8xTR9VBgnYcKPFT8ACHJ6BT0nQJIaINcZ50dRLG8WpYp+aUVuuinDd9fcZrUrlslBVRuDtt0YQvQ5DJ9Qo/1o2CHwua3YOfA2Q6odSKD/qBY8UrfOqzq5+tvG53pz8zLzhyppi78h4ZDqa0URe6W2kHlBI9YeO03HU2/If5Gwo7vsKlHMzfcuSNkMxi7A92itq0CdOnZ7CbPR2EgNge5VyHzSb1+qLH2FW4bb2RF/A58+yhGJF51Q2oDSu5lTcFuSlDoXWROfQ+mGHbmyt8mCEBQIDAQAB'),
(41, 'bt_envi', 'sandbox');

-- --------------------------------------------------------

--
-- Table structure for table `site_management_users`
--

CREATE TABLE IF NOT EXISTS `site_management_users` (
  `manage_id` int(11) NOT NULL,
  `admintype` smallint(5) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `status` smallint(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_management_users`
--

INSERT INTO `site_management_users` (`manage_id`, `admintype`, `uname`, `emailid`, `password`, `status`) VALUES
(1, 1, 'Admin', 'admin@meteorhealth.com', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_slideshow`
--

CREATE TABLE IF NOT EXISTS `site_slideshow` (
  `slideshow_id` int(11) NOT NULL,
  `slideshow_image` longtext NOT NULL,
  `slideshow_title` longtext NOT NULL,
  `slideshow_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `state_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staticpages`
--

CREATE TABLE IF NOT EXISTS `staticpages` (
  `stpage_id` int(11) NOT NULL,
  `stpage_name` varchar(255) NOT NULL,
  `stpage_title` text NOT NULL,
  `stpage_content` longtext NOT NULL,
  `stpage_status` smallint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `emailid` longtext NOT NULL,
  `password` longtext NOT NULL,
  `verify_email` smallint(1) NOT NULL,
  `user_type` smallint(1) NOT NULL,
  `signup_date` date NOT NULL,
  `zipcode` int(6) NOT NULL,
  `active_status` smallint(1) NOT NULL,
  `subscription` varchar(35) NOT NULL,
  `subscription_expiry` datetime NOT NULL,
  `subscription_date` datetime NOT NULL,
  `credits` int(11) NOT NULL,
  `status` varchar(35) NOT NULL,
  `promo` int(11) NOT NULL,
  `jobs_pending` int(11) NOT NULL,
  `verification_code` varchar(500) NOT NULL,
  `ip` varchar(35) NOT NULL,
  `bt_merchant_id` varchar(50) NOT NULL,
  `bt_private_key` varchar(50) NOT NULL,
  `bt_public_key` varchar(50) NOT NULL,
  `bt_cse` varchar(250) NOT NULL,
  `bt_envi` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `full_name`, `emailid`, `password`, `verify_email`, `user_type`, `signup_date`, `zipcode`, `active_status`, `subscription`, `subscription_expiry`, `subscription_date`, `credits`, `status`, `promo`, `jobs_pending`, `verification_code`, `ip`, `bt_merchant_id`, `bt_private_key`, `bt_public_key`, `bt_cse`, `bt_envi`) VALUES
(1, 'Lazar', 'lazar@provenlogic.net', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1', 1, 1, '2016-03-04', 560102, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'basic', 0, 0, 'fe9fc289c3ff0af142b6d3bead98a923', '106.51.227.232', '', '', '', '', ''),
(2, 'Steve', 'steve@ecommercemix.com', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1', 1, 1, '2016-03-04', 560102, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'basic', 0, 0, 'd86ea612dec96096c5e0fcc8dd42ab6d', '106.51.227.232', 'asdf', 'adsf', 'adsf', 'adsf', 'sandbox'),
(3, 'PJ', 'pjain@mba2017.hbs.edu', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1', 1, 2, '2016-03-08', 2134, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -505, '', 0, -1, '', '', '', '', '', '', ''),
(4, 'Mariam Melikadze', 'mmelikadze@mba2017.hbs.edu', 'l4kHlItcTIwFl3kJlYLbTIP5k4p8k4H3', 1, 1, '2016-03-09', 2163, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 'basic', 1, 0, 'e70611883d2760c8bbafb4acb29e3446', '128.103.124.136', '', '', '', '', ''),
(5, 'Rafael Telahun', 'rtelahun@gmail.com', 'l4kHlItcTIwFl3kJlYLbTItfjhyy', 1, 2, '2016-03-08', 0, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'agree', 0, 0, '', '', '', '', '', '', ''),
(7, 'Meteor Health', 'contact@meteorhealth.com', 'l4kHlItcTIwFl3kJlYLbTIP5k4p8k4H3', 1, 2, '2016-03-11', 2163, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, -7, 'cc1aa436277138f61cda703991069eaf', '199.94.1.207', '', '', '', '', ''),
(9, 'Mariam Melikadze', 'mariami_m@yahoo.com', 'l4kHlItcTIwFl3kJlYLbTIP5k4p8k4H3', 1, 1, '2016-03-25', 2163, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'basic', 0, 0, '217eedd1ba8c592db97d0dbe54c7adfc', '128.103.124.136', '', '', '', '', ''),
(10, 'Prerit Jain', 'preritjain1988@gmail.com', 'l4kHlItcTIwFl3kJlYLbTFcHRFs_SVcK', 1, 1, '2016-03-25', 2163, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'basic', 0, 0, '2f885d0fbe2e131bfc9d98363e55d1d4', '199.94.1.204', '', '', '', '', ''),
(11, 'Gary Wilson', 'gary@teamtweaks.com', 'l4kHlItcTIwFl3kJlYLbTIt9l3sJT5yy', 1, 1, '2016-04-01', 600001, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'agree', 0, -1, 'ae0eb3eed39d2bcef4622b2499a05fe6', '182.156.95.138', '', '', '', '', ''),
(12, 'santha', 'santha@casperon.in', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1kLyy', 1, 2, '2016-04-04', 600006, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, -2, '64223ccf70bbb65a3a4aceac37e21016', '182.156.95.138', '', '', '', '', ''),
(13, 'Shantha', 'santhaece144@gmail.com', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1kLyy', 1, 1, '2016-04-04', 600006, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'basic', 0, 0, 'da8ce53cf0240070ce6c69c48cd588ee', '182.156.95.138', '', '', '', '', ''),
(16, 'Lazar Savarinathan', 'lazar@appoets.com', 'l4kHlItcTIwFl3kJlYLbTIt9l4h1', 1, 1, '2016-04-29', 560102, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, 0, '6602294be910b1e3c4571bd98c4d5484', '127.0.0.1', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `dob` date NOT NULL,
  `user_image` longtext NOT NULL,
  `address` longtext NOT NULL,
  `about_info` longtext NOT NULL,
  `location_map` longtext NOT NULL,
  `skills` longtext NOT NULL,
  `profile_links` longtext NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `selected_catagories` longtext NOT NULL,
  `availability_time` varchar(90) NOT NULL,
  `available_from` date NOT NULL,
  `available_to` date NOT NULL,
  `right_person` varchar(2500) NOT NULL,
  `not_tasking` varchar(3000) NOT NULL,
  `tasking` varchar(3500) NOT NULL,
  `ethincity` varchar(35) NOT NULL,
  `tobacco` varchar(50) NOT NULL,
  `veteran` varchar(50) NOT NULL,
  `twin` varchar(50) NOT NULL,
  `feet` varchar(50) NOT NULL,
  `inches` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `medical_condition` varchar(50) NOT NULL,
  `prescription` varchar(2500) NOT NULL,
  `travel_distance` varchar(50) NOT NULL,
  `additional` varchar(2500) NOT NULL,
  `studies_parent` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`profile_id`, `user_id`, `gender`, `dob`, `user_image`, `address`, `about_info`, `location_map`, `skills`, `profile_links`, `mobile`, `phone`, `paypal_email`, `selected_catagories`, `availability_time`, `available_from`, `available_to`, `right_person`, `not_tasking`, `tasking`, `ethincity`, `tobacco`, `veteran`, `twin`, `feet`, `inches`, `weight`, `medical_condition`, `prescription`, `travel_distance`, `additional`, `studies_parent`, `username`) VALUES
(1, 1, 'Male', '1991-11-06', '5182.jpg', '14, 14th main Road', 'Im 100% positive', '', 'Ortho', '', '9876543210', '', '', '', '', '0000-00-00', '0000-00-00', 'Dedicated', 'sleeping', 'to be safe', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 2, 'Male', '1991-06-06', '', 'adsfdasfd', '0', '', '0', '', '123412343124', '', '', 'Benign Prostatic Hyperplasia', '', '0000-00-00', '0000-00-00', '0', '0', '0', 'Native American', 'No', 'Yes', 'No', '6', '1', '223', '0', 'adsf', '<100 miles', 'asdfdasfdsf', 'Yes', 'steve'),
(3, 3, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 4, 'Female', '1990-10-29', '', 'One Western Ave\nApt 337', '0', '', '0', '', '6173311689', '', '', '', '', '0000-00-00', '0000-00-00', '0', '0', '0', 'Caucasian', 'No', 'No', 'No', '5', '3', '125', '0', 'n/a', '<50 miles', 'n/a', 'No', 'mmelikadze'),
(5, 5, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 6, 'Male', '1991-11-06', '751.jpg', '88, 14th Main Road', '0', '', '0', '', '9876543210', '', '', '', '', '0000-00-00', '0000-00-00', '0', '0', '0', 'Asian', 'No', 'Yes', 'No', '5', '12', '165', '0', 'Ask you', '<50 miles', 'asfaskfljslkfjdsjlkf', 'Yes', 'Shan'),
(7, 7, '0', '1990-10-29', '7434.png', ' Soldiers Field 203 Batten Boston, MA 02163\n', '', '', '', '', '', '617-331-1689', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 8, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 9, 'Male', '1990-10-29', '', 'One Western Ave, Apt 337', '0', '', '0', '', '6173311689', '', '', '', '', '0000-00-00', '0000-00-00', '0', '0', '0', 'Native American', 'No', 'Yes', 'No', '3', '1', '130', '0', 'No', '<50 miles', 'No', 'Yes', 'mmelikadze2'),
(10, 10, 'Male', '1988-08-21', '', '1358, HBS MAIL CENTER, 500 SOLDIERS FIELD ROAD, BOSTON MA 02163', '0', '', '0', '', '6179028941', '', '', '', '', '0000-00-00', '0000-00-00', '0', '0', '0', 'Asian', 'No', 'No', 'No', '5', '8', '138', '0', 'None', '<50 miles', 'None', 'No', 'PJ'),
(11, 11, '0', '2016-04-06', '', '250 Ellis Road\nTonimbuk', '', '', '', '', '', '07584687455', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 12, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 13, 'Male', '1989-02-08', '', 'Greams Road\nChennai', '0', '', '0', '', '9787148138', '', '', '', 'any', '2016-04-04', '1970-01-01', '0', '0', '0', 'Native American', 'No', 'Yes', 'No', '5', '1', '55', '0', 'No ', '<200 miles', 'NO questions', 'Yes', 'santha'),
(15, 15, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 16, '0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`count`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `deactivate_report`
--
ALTER TABLE `deactivate_report`
  ADD PRIMARY KEY (`deactivate_report_id`);

--
-- Indexes for table `email_message`
--
ALTER TABLE `email_message`
  ADD PRIMARY KEY (`email_message_id`);

--
-- Indexes for table `email_newsletter`
--
ALTER TABLE `email_newsletter`
  ADD PRIMARY KEY (`email_newsletter_id`);

--
-- Indexes for table `email_settings`
--
ALTER TABLE `email_settings`
  ADD PRIMARY KEY (`email_set_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `job_bids`
--
ALTER TABLE `job_bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_category`
--
ALTER TABLE `job_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitesettings`
--
ALTER TABLE `sitesettings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `site_management_users`
--
ALTER TABLE `site_management_users`
  ADD PRIMARY KEY (`manage_id`);

--
-- Indexes for table `site_slideshow`
--
ALTER TABLE `site_slideshow`
  ADD PRIMARY KEY (`slideshow_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `staticpages`
--
ALTER TABLE `staticpages`
  ADD PRIMARY KEY (`stpage_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `count` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deactivate_report`
--
ALTER TABLE `deactivate_report`
  MODIFY `deactivate_report_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `email_message`
--
ALTER TABLE `email_message`
  MODIFY `email_message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_newsletter`
--
ALTER TABLE `email_newsletter`
  MODIFY `email_newsletter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_settings`
--
ALTER TABLE `email_settings`
  MODIFY `email_set_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `job_bids`
--
ALTER TABLE `job_bids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `job_category`
--
ALTER TABLE `job_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sitesettings`
--
ALTER TABLE `sitesettings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `site_management_users`
--
ALTER TABLE `site_management_users`
  MODIFY `manage_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_slideshow`
--
ALTER TABLE `site_slideshow`
  MODIFY `slideshow_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `staticpages`
--
ALTER TABLE `staticpages`
  MODIFY `stpage_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
