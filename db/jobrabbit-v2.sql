-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2016 at 10:33 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `task-parrot`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `user_id` int(11) NOT NULL,
  `Sofware_Engineer` int(11) NOT NULL,
  `Maid` int(11) NOT NULL,
  `Package_Delivery` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `count` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `tasker` varchar(50) NOT NULL,
  `message` varchar(3500) NOT NULL,
  `chat_at` datetime NOT NULL,
  `status` varchar(35) NOT NULL,
  `message_tasker` varchar(3500) NOT NULL,
  `status_task` varchar(35) NOT NULL,
  PRIMARY KEY (`count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_status` varchar(255) NOT NULL,
  `city_timezone` varchar(255) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;


-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` longtext NOT NULL,
  `country_currency` varchar(255) NOT NULL,
  `country_currency_code` varchar(255) NOT NULL,
  `country_status` smallint(1) NOT NULL,
  `country_image` longtext NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


--
-- Table structure for table `deactivate_report`
--

CREATE TABLE IF NOT EXISTS `deactivate_report` (
  `deactivate_report_id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) NOT NULL,
  `reason` text NOT NULL,
  `deactivate_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`deactivate_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_message`
--

CREATE TABLE IF NOT EXISTS `email_message` (
  `email_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`email_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_newsletter`
--

CREATE TABLE IF NOT EXISTS `email_newsletter` (
  `email_newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`email_newsletter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `email_set_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_set_type` longtext NOT NULL,
  `email_set_description` longtext NOT NULL,
  PRIMARY KEY (`email_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(10) NOT NULL,
  `payment_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_method` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `jposter_id` int(10) NOT NULL,
  `jposted_date` date NOT NULL,
  `job_pay` int(2) NOT NULL,
  `jname` varchar(256) NOT NULL,
  `jdescription` longtext NOT NULL,
  `jact_status` smallint(1) NOT NULL COMMENT '1= Active, 2=Deactive',
  `jwork_status` smallint(1) NOT NULL COMMENT '1=new,2=assign,3=accept,4=conform,5=cancel,6=complete,7=incomplete,8=reject',
  `jbilling_by` smallint(1) NOT NULL COMMENT '1= Hour, 2= Day',
  `jbilling_no_hours` int(10) NOT NULL,
  `jbilling_no_days` int(10) NOT NULL,
  `jposter_price` int(10) NOT NULL,
  `jbidding_type` smallint(1) NOT NULL COMMENT '1=Open Bidding,2=Direct,3=First Bid',
  `jbidding_st_date` date NOT NULL COMMENT 'Open Bidding',
  `jbidding_ed_date` date NOT NULL COMMENT 'Open Bidding',
  `jassign_to_work_id` int(10) NOT NULL,
  `jassign_date` date NOT NULL,
  `jassign_amt` int(10) NOT NULL,
  `jaccept_workid` int(10) NOT NULL,
  `jaccept_date` date NOT NULL,
  `jaccept_amt` int(10) NOT NULL,
  `jconformed_date` date NOT NULL,
  `jcancel_date` date NOT NULL,
  `jcancel_msg` longtext NOT NULL,
  `jcomplete_date` date NOT NULL,
  `jcomplete_amt` int(10) NOT NULL,
  `jfeedback` longtext NOT NULL,
  `jposter_fav` smallint(1) NOT NULL COMMENT '1= enable',
  `jworker_fav` smallint(1) NOT NULL COMMENT '1= enable',
  `jreject_date` date NOT NULL,
  `jreject_msg` longtext NOT NULL,
  `jstar_rate` int(2) NOT NULL,
  `jaddress` varchar(50) NOT NULL,
  `jzipcode` int(20) NOT NULL,
  `jlatitude` varchar(35) NOT NULL,
  `jlongitude` varchar(50) NOT NULL,
  `jbidding_stt_date` date NOT NULL,
  `jbidding_edd_date` date NOT NULL,
  `rate` int(11) NOT NULL,
  `review` varchar(25) NOT NULL,
  `jdaddress` varchar(3500) NOT NULL,
  `djaddress` varchar(3500) NOT NULL,
  `djzipcode` varchar(35) NOT NULL,
  `djlongitude` varchar(25) NOT NULL,
  `djlatitude` varchar(25) NOT NULL,
  `drefund` varchar(25) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_bids`
--

CREATE TABLE IF NOT EXISTS `job_bids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` varchar(10) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `bid_amt` varchar(10) NOT NULL,
  `biding_dt` date NOT NULL,
  `bid_commands` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_job_type` int(11) NOT NULL,
  `category_parent_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_status` smallint(1) NOT NULL,
  `category_description` varchar(1500) NOT NULL,
  `category_image` varchar(500) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `default` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `sitesettings`
--

CREATE TABLE IF NOT EXISTS `sitesettings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `sitesettings`
--

INSERT INTO `sitesettings` (`settings_id`, `type`, `description`) VALUES
(1, 'site_online', '2'),
(2, 'site_name', 'Job Rabbit'),
(3, 'site_slogan', 'slogan for the website'),
(4, 'site_slogan_Enable', '2'),
(5, 'site_logo', 'logo.png'),
(6, 'site_favicon', ''),
(7, 'site_title', 'Jobrabbit'),
(8, 'site_keywords', 'job'),
(9, 'site_description', 'welcome to jobrabbit'),
(10, 'site_slideshow_Enable', '1'),
(11, 'site_offline_mess', 'The site is Under Construction'),
(12, 'facebook_link', 'http://www.facebook.com/eCommercemix'),
(13, 'twitter_link', 'http://www.twitter.com/ecommercemix'),
(14, 'seo_status', '1'),
(15, 'seo_info', ''),
(16, 'facebook_status', '1'),
(17, 'facebook_profile', 'http://jobrabbit.info//home/facebook'),
(18, 'facebook_apikey', '243265709210680'),
(19, 'facebook_applicationkey', 'c91f4f5283f743db82a04a3c34daa0c8'),
(20, 'facebook_link', 'http://www.facebook.com/eCommercemix'),
(21, 'twitter_link', 'http://www.twitter.com/ecommercemix'),
(22, 'other_link', ''),
(23, 'other_link_image', ''),
(24, 'paypal_emailid', 'kathadvipul@gmail.com'),
(25, 'job_comission', '20'),
(26, 'mailgun_apikey', 'key-0-sa27gj-0qj93qrovcwdl7l1sw9fg81'),
(27, 'mailgun_version', 'api.mailgun.net/v2/'),
(28, 'mailgun_domain', 'sandbox0e20bcbc7450433995c40fc95b846c13.mailgun.org'),
(29, 'mailgun_fromemail', 'info@samples.com'),
(30, 'mailgun_replyemail', 'info@samples.com'),
(31, 'subscription_amount', '10'),
(32, 'subscription_period', '2'),
(33, 'subscription_status', 'Enable'),
(34, 'no_of_post', '2'),
(35, 'promo_code', 'Welcome'),
(36, 'promo_price', '15');
-- --------------------------------------------------------

--
-- Table structure for table `site_management_users`
--

CREATE TABLE IF NOT EXISTS `site_management_users` (
  `manage_id` int(11) NOT NULL AUTO_INCREMENT,
  `admintype` smallint(5) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `status` smallint(2) NOT NULL,
  PRIMARY KEY (`manage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `site_management_users`
--

INSERT INTO `site_management_users` (`manage_id`, `admintype`, `uname`, `emailid`, `password`, `status`) VALUES
(1, 1, 'Admin', 'admin', 'l4kHlItcTIwFl3kJlYLbTFsHQVG-', 1);
-- --------------------------------------------------------

--
-- Table structure for table `site_slideshow`
--

CREATE TABLE IF NOT EXISTS `site_slideshow` (
  `slideshow_id` int(11) NOT NULL AUTO_INCREMENT,
  `slideshow_image` longtext NOT NULL,
  `slideshow_title` longtext NOT NULL,
  `slideshow_description` longtext NOT NULL,
  PRIMARY KEY (`slideshow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `state_status` varchar(255) NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `staticpages`
--

CREATE TABLE IF NOT EXISTS `staticpages` (
  `stpage_id` int(11) NOT NULL AUTO_INCREMENT,
  `stpage_name` varchar(255) NOT NULL,
  `stpage_title` text NOT NULL,
  `stpage_content` longtext NOT NULL,
  `stpage_status` smallint(1) NOT NULL,
  PRIMARY KEY (`stpage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `emailid` longtext NOT NULL,
  `password` longtext NOT NULL,
  `verify_email` smallint(1) NOT NULL,
  `user_type` smallint(1) NOT NULL,
  `signup_date` date NOT NULL,
  `zipcode` int(6) NOT NULL,
  `active_status` smallint(1) NOT NULL,
  `subscription` varchar(35) NOT NULL,
  `subscription_expiry` datetime NOT NULL,
  `subscription_date` datetime NOT NULL,
  `credits` int(11) NOT NULL,
  `status` varchar(35) NOT NULL,
  `promo` int(11) NOT NULL,
  `jobs_pending` int(11) NOT NULL,
  `verification_code` varchar(500) NOT NULL,
  `ip` varchar(35) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `dob` date NOT NULL,
  `user_image` longtext NOT NULL,
  `address` longtext NOT NULL,
  `about_info` longtext NOT NULL,
  `location_map` longtext NOT NULL,
  `skills` longtext NOT NULL,
  `profile_links` longtext NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `selected_catagories` longtext NOT NULL,
  `availability_time` varchar(90) NOT NULL,
  `available_from` date NOT NULL,
  `available_to` date NOT NULL,
  `right_person` varchar(2500) NOT NULL,
  `not_tasking` varchar(3000) NOT NULL,
  `tasking` varchar(3500) NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
