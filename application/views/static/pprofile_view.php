<div id="content" class="container">
	 <?php  foreach($user_profile as $profile):?>
		<div class="col-xs-10 col-xs-offset-1 task-form-container whiteContainer">
      <!--  <p align="right">
        <a href=" <?php
		if(isset($_SERVER['HTTP_REFERER']))
		{
			$a=$_SERVER['HTTP_REFERER'];
			echo $a;
		}
		?>">Go Back</a>
		</p>-->
		<ul class="nav nav-tabs" role="tablist">
            <li class="active">
                <a href="#profile" data-toggle="tab">Profile</a>
            </li>
            <li class=" ">
                <a href="#reviews" data-toggle="tab">Reviews</a>
            </li>
   </ul>

  <!-- <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
   </ul> -->

  <!-- Tab panes -->
 <div class="tab-content">
        <div class="tab-pane active" id="profile">
                 <div class="animated fadeInLeft">
                        <h3>A few fun facts about me:</h3>

                        <p><i class="fa fa-calendar"></i> I’ve been a Tasker since 2015.</p>
                        
                        <h3>Why I'm your Tasker:</h3>

                        <h4><strong>I'm the right person for the job...</strong></h4>
                        <p>I am always striving to provide a superior service. I’m a very hands-on person & love to multi-task.. I always maintain a positive, cheerful, & friendly personality so I’m great with people. Remarkably good organizational skills. Exceptional cleaning habits. Very great at working with others.</p>

                        <h4><strong>When I'm not tasking...</strong></h4>

                        <p>I currently work nights for the Atlanta Journal Constitution. I'm also a musician, so I spend free time working on musical projects. I'm really into Self-Help information, so I read a lot & listen to plenty of audios on self-improvement.</p>

                        <h4><strong>When I'm tasking I always make sure to...</strong></h4>
                        <p>Leave my clients 100% Satisfied, Happy, & eager to use my services again.</p>


                    </div>
        </div>
        <div class="tab-pane " id="reviews">
        <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>  
        </div>
     </div>
            <div class="jobEntry">
            <h4><?php echo $profile['full_name'];?></h4>
            <table width="100%" border="0">
            	<tr>
                    <td valign="top" width="25%">
                    <br/>
                    <center>
                    <img src="<?php echo base_url(); ?>uploads/profiles/<?php if(!empty($profile['user_image'])){ echo $profile['user_image'];  }else{echo "user-default.jpg";}?>" alt="profile Image" class="img-circle"><!---class="img-thumbnail"--><br/>
                    </center><br/>
                      <b>Gender : </b> <?php 
						switch ($profile['gender'])
						{
							case '1':
							$status ='Male';
							break;
							case '2':
							$status ='Female';
							break;
							
						}
						echo @$status;
					 ?> <br/> <strong>Age : </strong> <?php echo date_diff(date_create($profile['dob']), date_create('today'))->y; ?>
					  <br/>
                      <center>
                      <?php $pro =(explode("*",$profile['profile_links']));?>
                      <?php echo "<a href='".@$pro[0]."'><img src='".base_url()."template/image/facebook_icon.png'></a>"; ?>
                       <?php echo"<a href='".@$pro[1]."'><img src='".base_url()."template/image/twitter_icon.png'></a>";?>
                      </center>
                     
                      
                    </td>
                    <td  valign="top" width="75%">
                    	<blockquote><b>About Me : </b></blockquote>
                        <p style="text-indent:50px;">
						<?php echo $profile['about_info'];?>
                        </p>
                        <br/>
                        <blockquote><b>Skills : </b></blockquote>
                        <p style="text-indent:50px;">
						<?php echo $profile['skills'];?>
                        </p>
                        <br/>
                        <blockquote><b>Reviews: </b></blockquote>
                    
                    	<?php foreach($job_desp as $jobs):
							if(!empty($jobs['jstar_rate'])){
						?> 	<p style="text-indent:50px;">
                        	<b><?php echo $this->db->query("SELECT * FROM users WHERE user_id=". $jobs['jposter_id'])->row()->full_name;?></b>&nbsp;:&nbsp;
                            <?php 
							@$avg = $jobs['jstar_rate'];
							if(@$avg){		
								$rt=round($avg);
								$img="";
								$i=1;
								while($i<=$rt){
								$img=$img."<img src=".base_url()."template/images/star.gif >";
								$i=$i+1;
								}
								while($i<=5){
								$img=$img."<img src=".base_url()."template/images/star2.gif >";
								$i=$i+1;
								}
								echo $img;
							}
							?>      
                            </p>
                            <p style="text-indent:70px;">
                            <?php echo $jobs['jfeedback']; ?>
                           </p>                    		                            
                     	<?php } endforeach; ?>
                    </td>
                    <?php endforeach; ?>
                </tr>
            </table>            
            </div>
		</div>
         
</div>