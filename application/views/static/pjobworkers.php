<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
              if(typeof result['message_tasker'] ==='undefined')
             {
                
             }
              else if(result['message_tasker']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat/'+result['tasker']+'/<?php echo $user_id; ?>">New Message</a>'); // here the wrapper is main div
             
             } }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">



    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">


    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

               <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs">Jobs</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
 <div class="bg-grey-1">
 <section class="container section-lg">
 <div class="col-sm-12 fadeInRight animated panel pad-m">
<?php  foreach($recent_jobs as $jobs):?>
            <h2><?php echo $jobs['jname'];?></h2>
			<div class="col-xs-12">
            <div class="jobEntry">
					<h4>Job Info</h4>
					<ul class="secondaryInfo">
                      	<li title="Posted Date"><i class="fa fa-calendar"></i> Posted Date <?php echo $jobs['jposted_date'];?></li>
                        <li title="Completed Date"><i class="fa fa-calendar"></i> Completed Date <?php echo $jobs['jcomplete_date'];?></li>
						<li title="Bill by"><i class="hourGlass"></i>  <?php 									
										switch ($jobs['jbilling_by'])
										{
											case '1':
											$status ='Hour Based';
											break;
											case '2':
											$status ='Day Based';
											break;
											
										}
										echo $status;?></li>
						<li title="Bidding"><i class="fa fa-gavel"></i>  <?php 
									
										switch ($jobs['jbidding_type'])
										{
											case '1':
											$status ='Yes';
											break;
											case '2':
											$status ='No';
											break;
											
										}
										echo $status;?></li>
                       <li title="Price">Completed <i class="fa fa-dollar"></i> <?php echo $jobs['jposter_price'];?></li>
					</ul>
					<blockquote><b>Description</b></blockquote>
                       		<p style="text-indent:50px;"><?php echo $jobs['jdescription'];?></p>
                    <blockquote><b>Feed back</b></blockquote>
                        	<p style="text-indent:50px;"><?php echo $jobs['jfeedback'];?></p>
                       <?php 
                       $tasker_id = $jobs['jaccept_workid'];
                       $tasker = $this->db->query("SELECT * FROM user_profiles WHERE user_id=".$tasker_id)->row();
                $taskers = $this->db->query("SELECT * FROM users WHERE user_id=".$tasker_id)->row();?>
                   <h4>Worker Details</h4>
            <b>Tasker id</b> <?php echo $jobs['jaccept_workid'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" data-toggle="modal" data-target="#myModal" onclick="profile_view('<?php echo $tasker->user_id;?>','<?php echo $taskers->full_name; ?>','<?php echo $taskers->emailid; ?>', '<?php echo $taskers->zipcode;?>')">View profile</a> 
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Rating :</b> &nbsp;&nbsp;
			
			<?php 
			
	if($jobs['jstar_rate']){		
			 $rt=round($jobs['jstar_rate']);
$img="";
$i=1;
while($i<=$rt){
$img=$img."<img src=".base_url()."template/images/star.gif >";
$i=$i+1;
}
while($i<=5){
$img=$img."<img src=".base_url()."template/images/star2.gif >";
$i=$i+1;
}
echo $img;
}
?>
				</div>
                                        
                                    
                                  <?php endforeach; ?>
			</div>  
			</div>   
</section>
</div>
              <script>
                        function profile_view(id,name,email,zipcode){
                             $(document).ready(function() {

    $.ajax({url : '<?php echo base_url();?>home/profile_view/'+id,
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
         async: true,
        success : function (result) {
            $('#modal-img').attr('src','<?php echo base_url();?>uploads/profiles/'+result['user_image']);
            $('#modal-pname').html(name);
            $('#modal-pemail').html(email);
            $('#modal-pphone').html(result['phone']);
            $('#modal-pzip').html(zipcode);
            $('#modal-right').html(result['right_person']);
            $('#modal-not').html(result['not_tasking']);
            $('#modal-task').html(result['tasking']);

            
            
           //console.log(result['advert']) // The value of your php $row['adverts'] will be displayed
        }
    });
                        });
                    }
                    </script>

    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>



<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.responsive.min.js"></script>

<script src="<?php echo base_url();?>assets/js/responsive.bootstrap.min.js"></script>

    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title" id="myModalLabel">Profile View</h4>
     </div>
     <div class="modal-body">
       <!-- Nav tabs -->
 <!-- Nav tabs -->
   

  <!-- <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
   </ul> -->

  <!-- Tab panes -->
 <div class="tab-content">
        <div class="tab-pane active" id="profile">
                 <div class="animated fadeInLeft">
                        <h3>A few fun facts about me:</h3>
                        <div class="panel panel-default pad-m text-center">
                        
                        <img src="" id="modal-img" class="user-img">
                    <div class="space-sm"></div>
                    <i class="fa fa-user"></i> <span id="modal-pname"></span><br>
                    <i class="fa fa-envelope-o"></i> <span id="modal-pemail"></span><br>
                    <i class="fa fa-phone"></i> <span id="modal-pphone"></span><br>
                    <i class="fa fa-map-marker"> </i><span id="modal-pzip"></span><br>
                    
                    </div>
                        <h3>Why I'm your Tasker:</h3>

                        <h4><strong>I'm the right person for the job...</strong></h4>
                        <div id="modal-right"></div>

                        <h4><strong>When I'm not tasking...</strong></h4>

                        <div id="modal-not"></div>

                        <h4><strong>When I'm tasking I always make sure to...</strong></h4>
                        <div id="modal-task"></div>


                    
                    </div>
        </div>
     </div>
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
     </div>
   </div>
 </div>
</div> 
</body>


</html>
