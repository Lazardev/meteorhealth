<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">



    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">


    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.bootstrap.min.css" rel="stylesheet">

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_worker'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_worker"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li><a href="<?php echo base_url();?>home/dash_worker">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs_task">Trials</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>



            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->


                            

    
<div class="bg-grey-1">
    <section class="container section-lg">


        <!-- Nav tabs -->
        <ul class="inline-tabs">
            <li class="active">
                <a href="#acc" data-toggle="tab">Account</a>
            </li>
            <li class="">
                <a href="#pwd" data-toggle="tab">Password</a>
            </li>
            <li class="">
                <a href="#binfo" data-toggle="tab">Payment Info</a>
            </li>
            <li class="">
                <a href="#trs" data-toggle="tab">Trial History</a>
            </li>
            <li class="">
                <a href="#" data-toggle="modal" data-target="#deactivate">Deactivate</a>
            </li>
           
        </ul>
        <!-- Tab panes -->
        <?php 
               $user_id = $this->session->userdata('user_id');
               $profile = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = '$user_id'")->row();
$profile_user = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row();?>
        <div class="tab-content">
            <div class="tab-pane active" id="acc">
                
                <div class="panel panel-default pad-m text-center">
                    <h4><strong>Account</strong></h4>
                    <div class="space-sm"></div>
                    <?php if($profile->user_image != '') {?>
                    <img src="<?php echo base_url();?>uploads/profiles/<?php echo $profile->user_image; ?>" class="users">
                    <?php } else { ?>
                    <img src="<?php echo base_url();?>uploads/profiles/user-default.jpg" class="users">
                   <?php }?>
                    <!-- <img src="<?php echo base_url();?>uploads/profiles/<?php echo $profile->user_image?>" class="user-img"> -->
                    <div class="space-sm"></div>
                    <p><i class="fa fa-user"></i> <?php echo $profile_user->full_name; ?></p>
                    <p><i class="fa fa-envelope-o"></i> <?php echo $profile_user->emailid; ?></p>
                    <p><i class="fa fa-phone"></i> <?php echo $profile->phone; ?></p>
                    <p><i class="fa fa-map-marker"></i><?php echo $profile_user->zipcode; ?></p>

                      <!--  <a href="<?php echo base_url();?>home/dash_poster" class="btn btn-primary active"> User</a>
                            <a href="<?php echo base_url();?>home/dash_worker" class="btn btn-primary">
                            Tasker</a> -->

                    <br><br>

                    <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-acc">Edit</a>

                </div>

            </div>

            <div class="tab-pane " id="pwd">
            
                <div class="panel panel-default pad-m">
                    <h4><strong>Account</strong></h4>
                   <form action="<?php echo base_url();?>home/account/password/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
                    <div class="form-group">
                            <label>Enter current password:</label>
                            <input type="password" class="form-control" name="old_pwd" placeholder="Enter current password">
                        </div>
                        <div class="form-group">
                            <label>Enter new password:</label>
                            <input type="password" class="form-control" id="pass1" name="new_pwd" placeholder="Enter new password">
                        </div>
                        <div class="form-group">
                            <label>Confirm new password:</label>
                            <input type="password" class="form-control" id="pass2" onkeyup="checkPass(); return false;" name="new_pwd1" placeholder="Confirm new password">
                        </div>
                        <span id="confirmMessage" class="confirmMessage"></span>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        
                    </form>
                    

                </div>

            </div>


            <div class="tab-pane" id="binfo">
                
                <div class="panel panel-default pad-m">
                    <h4><strong>Edit Payment Info</strong></h4>
                    <hr>
                   
                    <form method="post" action="<?php echo base_url();?>home/account_task/payment/<?php echo $this->session->userdata('user_id');?>/" class="no-icon" enctype="multipart/form-data">
                            
                            <div class="form-group">
                                <label>Braintree Merchant Id</label>
                                        <input type="text" class="validate[required] form-control" name="bt_merchant_id" value="<?php echo $profile_user->bt_merchant_id;?>"/>
                                </div>
                            <div class="form-group">
                                <label>Braintree Public Key</label>
                                         <input type="text" class="validate[required] form-control" name="bt_public_key" value="<?php echo $profile_user->bt_public_key;?>"/>
                                 </div>
                             <div class="form-group">
                                <label>Braintree Private Key</label>
                                         <input type="text" class="validate[required] form-control" name="bt_private_key" value="<?php echo $profile_user->bt_private_key;?>"/>
                                </div>
                             <div class="form-group">
                                <label>Braintree Client-Side Encryption Key</label>
                                <div class="controls">
                                    <input type="text" class="validate[required] form-control" name="bt_cse" value="<?php echo $profile_user->bt_cse;?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Braintree Environment</label>
                                   <select name="bt_envi" class="validate[required] form-control">
                                      <option value="<?php echo $profile->bt_envi;?>"><?php $envi = $profile_user->bt_envi;
                                      echo ucfirst($envi);?></option>
                                     <option value="sandbox">Sandbox</option>
                                     <option value="live">Live</option>
                                   </select>
                                
                            </div>
                        <div class="form-actions">
                            <button type="submit"  class="btn btn-primary">Save</button>
                        </div>
                    </form>     
                    

                </div>

            </div>


           

            <div class="tab-pane " id="trs">
               <?php $user_id =  $this->session->userdata('user_id');
        $this->db->order_by("invoice_id", "desc");
        $recent_invoice = $this->db->get_where('invoice', array('user_id' => $user_id))->result_array();
        ?>
                <div class="panel panel-default pad-m">
                    <h4><strong>Transaction History</strong></h4>
                    <hr>

                    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                    <tr>
                                        <th><div>Invoice id</div></th>
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Transaction id</div></th>
                                        <th><div>Date</div></th>
                                        <th><div>Status</div></th>
                                        <th><div>Amount</div></th>                                  
                                    </tr>
                                </thead>
                                <tbody>
                                
                                 <?php  foreach($recent_invoice as $jobs):?>
                                    <tr>
                                        <td><?php echo $jobs['invoice_id'];?></td>
                                        <td><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $jobs['job_name'];?></td>
                                        <td><?php echo $jobs['transaction_id'];?></td>
                                        <td><?php echo $jobs['payment_timestamp'];?></td>
                                        <td><?php echo $jobs['status'];?></td>
                                        <td><?php echo $jobs['amount'];?></td>
                                     
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                        </table>


                    
                    <a href="#" class="btn btn-sm btn-danger">Download</a>

                </div>

            </div>


            <div class="tab-pane" id="review">
                
                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png">
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png">
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png">
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>  

            </div>
            <div class="tab-pane" id="abt">
                <div class="row">
                    <div class="col-sm-12 animated fadeInLeft">
                        <h3>A few fun facts about me:</h3>

                        <p><i class="fa fa-calendar"></i> I’ve been a Tasker since 2015.</p>
                        <p><i class="fa fa-check"></i> I’ve done 114 tasks.</p>
                        <p><i class="fa fa-map-marker"></i> I have a Car if you need one.</p>
                        <p><i class="fa fa-comment"></i> I respond quickly.</p>

                        <h3>Why I'm your Tasker:</h3>

                        <h4><strong>I'm the right person for the job...</h4>
                        <p>I am always striving to provide a superior service. I’m a very hands-on person & love to multi-task.. I always maintain a positive, cheerful, & friendly personality so I’m great with people. Remarkably good organizational skills. Exceptional cleaning habits. Very great at working with others.</p>

                        <h4><strong>When I'm not tasking...</h4>

                        <p>I currently work nights for the Atlanta Journal Constitution. I'm also a musician, so I spend free time working on musical projects. I'm really into Self-Help information, so I read a lot & listen to plenty of audios on self-improvement.</p>

                        <h4><strong>When I'm tasking I always make sure to...</h4>
                        <p>Leave my clients 100% Satisfied, Happy, & eager to use my services again.</p>


                    </div>
                    
                </div>
            </div>
           
        </div>

    </section>
</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>



<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.responsive.min.js"></script>

<script src="<?php echo base_url();?>assets/js/responsive.bootstrap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {

 $('.dtbl').DataTable();
} );

$(window).load(function() {

 $('.dtbl').DataTable();
} );

$(window).resize(function(){
     $('.dtbl').DataTable();
});


</script>

    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>




<!-- Modal -->
<div class="modal fade" id="edit-acc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/account/update/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
               
                <div class="form-group">
                    <label>Profile Photo</label>
                    <input type="file" class="form-control" name="image"  placeholder="Profile Photo">
                </div>
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="form-control" name="fullname" value="<?php echo $profile_user->full_name;?>" placeholder="Full Name">
                </div>
                 <div class="form-group">
                    <label>Date of Birth</label>
                <input type="date" class="form-control" required name="dob" value="<?php if($profile->dob != '0000-00-00') { echo $profile->dob; } else { echo "";}?>" placeholder="DOB">
                </div>
                
                <div class="form-group">
                    <label>Contact Address</label>
                    <textarea class="form-control" name="address" required rows="6"  placeholder="Address"><?php echo $profile->address;?></textarea>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $profile_user->emailid;?>" placeholder="Email">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?php echo $profile->phone;?>" placeholder="Phone">
                </div> 
                <div class="form-group">
                    <label>Zip Code</label>
                    <input type="text" class="form-control" name="zip" value="<?php echo $profile_user->zipcode;?>" placeholder="Zip Code">
                </div>
                <div class="form-group">
                    <label>Skills</label>
                   <textarea class="form-control" rows="5"  name="skills" placeholder="Skills"><?php echo $profile_user->skills;?></textarea>
                </div>
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
         </form>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="deactivate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Deactivate My Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/confirm_deactivation/insert/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
                <div class="form-group">
                    <label>Reason for Deactivation</label>
                    <textarea id="change" class="form-control" required rows="10" name="reason"></textarea>
                </div>

               
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" onclick="return confirm('Are Sure to Deactivate')" class="btn btn-primary">Deactivate My Account</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
</body>


</html>