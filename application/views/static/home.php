<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

   <style>
     @media(max-width: 450px){
      .bg-cover{
        background: url('assets/img/meteor.png') no-repeat !important;
        background-size: cover !important;
      }
    }
   </style>
<!--Google Analytics Script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-76141887-1', 'auto');
  ga('send', 'pageview');
 
</script>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-trn navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">
                <li><a href="<?php echo base_url();?>home/login/participant">Participant Login</a></li>
                <li><a href="<?php echo base_url();?>home/login/provider">Provider Login</a></li>
               <!-- <li><a href="<?php echo base_url();?>home/become_jobrabbit">Become a Meteor</a></li> -->
                <!-- <li><a href="<?php echo base_url();?>home/how_it_works">How it works</a></li> -->
                

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!--  Header Section  -->
    <section class="bg-centerTop70 bg-cover" id="block" style="width: 100%; height: 100%; background-image: url('https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTKKGJkFT3dGsMzhNbqTI8Y7sU6KC_OlfMXxsY3wl4G2nKgczwG') no-repeat;background-size: cover;" data-vide-bg="<?php echo base_url();?>assets/img/MeteorHea1551" data-vide-options="position: 0% 50%">
        <div class="bg-filter sTop">
            <div class="container">
                <div class="jumbotron trn v-center">
                    <h2>Participate in clinical trials near you. Earn money and support medical research!</h2>
                    <div class="space-sm"></div>
                    <p style="width:75%; font-size:14px;" align="justify">Meteor Health is a revolutionary new platform that helps millions of people easily find and participate in clinical trials and academic studies. Earn extra money and help further medical research today! <br /><br />

We automate the entire clinical trial experience by using powerful algorithms to help connect you with suitable trials and studies in your area. Meteor Health does it all – enrollment, payments, reporting and reminders – all in a secure, HIPAA compliant platform. This means your data is secure and only the licensed clinical providers you chose will know your identity. <br /> <br />

Join millions of people like you who are earning money and helping accelerate innovation!</p>
                    <div class="space-sm"></div>
                    <ul class="list-inline">
                        <li>
                            <a href="<?php echo base_url();?>home/signup" class="btn btn-primary btn-xlg scroll"><i class="fa fa-paper-plane-o"></i>Join Free</a>
                        </li>
                       <!-- <li>
                            <a href="<?php echo base_url();?>home/login" class="btn btn-default-trn btn-xlg scroll"><i class="fa fa-sign-in"></i>Login</a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="container section margin-b30">

    <div class="space">
    <div class="space">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="float-box">
                    <i class="fa icon-c fa-lock"></i>
                    <div class="float-text">
                        <h3>SECURE ONBOARDING</h3>
                        <p align="justify">Your private information is only shared with certified and licensed Clinical Providers -  and only after you explicitly consent.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="float-box">
                    <i class="fa icon-c fa-sliders"></i>
                    <div class="float-text">
                        <h3>TAILORED RESULTS</h3>
                        <p align="justify">Filtered listing of trials near you that fit your schedule and preferences</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 ">
                <div class="float-box">
                    <i class="fa icon-c fa-hand-o-up"></i>
                    <div class="float-text">
                        <h3>EFFORTLESS PARTICIPATION</h3>
                        <p align="justify">Digital  appointments, automated reminders, electronic reporting & monitoring. Participation made simple</p>
                    </div>
                </div>
            </div>

          
        </div>
    </section>


<section class="bg-8 bg-center bg-cover how-work">
        <div class="bg-filter-v1 section-lg">
            <div class="container">
                <h2 class="text-center big-title">Making Participating in Trials  Effortless</h2>
                <div class="row">
                    <div class="col-md-5 hidden-xs hidden-sm">
                        <!-- <img class="img-responsive iphone-black" alt="iPhone 5S Minimal Mock Up Black" src="assets/img/iPhone_5S_Minimal_Mock-up_black.png">
                        <img class="img-responsive iphone-white " alt="iPhone 5S Minimal Mock Up White" src="assets/img/iPhone_5S_Minimal_Mock-up.png"> -->
                    </div>
                    <div class="col-md-7 col-sm-12 margin-b15">
                        <div class="space-lg visible-md visible-lg"></div>
                        <div class="space visible-sm visible-xs"></div>
                        <div class="float-box wow fadeInRight">
                            <i class="fa fa-lock hovicon effect-1 sub-a text-primary"></i>
                            <div class="float-text-v1">
                                <h3>Create a Secure  Profile</h3>
                                <p>Answer questions about you and your preferences for trials  and studies</p>
                            </div>
                        </div>
                        <div class="float-box wow fadeInRight" data-wow-delay="0.25s">
                            <i class="fa fa-sliders hovicon effect-1 sub-a text-primary"></i>
                            <div class="float-text-v1">
                                <h3>Find Tailored Trials Near You</h3>
                                <p>Filtered, verified and tailored based on your unique profile</p>
                            </div>
                        </div>
                        <div class="float-box wow fadeInRight" data-wow-delay="0.5s">
                            <i class="fa fa-calendar hovicon effect-1 sub-a text-primary"></i>
                            <div class="float-text-v1">
                                <h3>Book Appointments</h3>
                                <p>Get reminders, sign consents and manage active trials</p>
                            </div>
                        </div>
                        <div class="float-box wow fadeInRight" data-wow-delay="0.5s">
                            <i class="fa fa-credit-card hovicon effect-1 sub-a text-primary"></i>
                            <div class="float-text-v1">
                                <h3>Easy Reporting and Payments</h3>
                                <p>Post appointment reporting as well as direct and easy payments</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>






    <!-- Features section -->
    <div id="learn-more"></div>
    <section class="section">
        <div class="container">
            <h2 class="lg-title lg-title-border">Meteor Health Transforms the Clinical Trial Experience</h2>
            <div class="space"></div>
            <div class="space"></div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="text-center">
                        <i class="fa fa-thumbs-up hovicon effect-3 sub-b easePulse wow" data-wow-delay="0.2s"></i>
                        <h3 class="wow fadeIn m-10-top" data-wow-delay="0.4s">One Stop Source For Trials</h3>
                        <p align="justify" class="wow fadeIn" data-wow-delay="0.4s">No  more searching on Craigslist, no more ads on Facebook or cold calls. Get trial information near you when you want</p>
                    </div>
                    <div class="space"></div>
                </div>
                <div class="col-sm-6">
                    <div class="text-center">
                        <i class="fa fa-shield hovicon effect-3 sub-b easePulse wow" data-wow-delay="0.4s"></i>
                        <h3 class="wow fadeIn m-10-top" data-wow-delay="0.6s">Promoting Your Safety</h3>
                        <p align="justify" class="wow fadeIn" data-wow-delay="0.6s">Ensuring that adverse effect alerts are issued  quickly and preventing trials don’t interfere with your medicines.</p>
                    </div>
                    <div class="space"></div>
                </div>
                <div class="col-sm-6">
                    <div class="text-center">
                        <i class="fa fa-file-text hovicon effect-3 sub-b easePulse wow" data-wow-delay="0.6s"></i>
                        <h3 class="wow fadeIn m-10-top" data-wow-delay="0.8s">Easy Reporting and Tracking</h3>
                        <p align="justify" class="wow fadeIn" data-wow-delay="0.8s">No more calls. All of your post-visit monitoring and questionnaires done easily and electronically</p>
                    </div>
                    <div class="space"></div>
                </div>
                <div class="col-sm-6">
                    <div class="text-center">
                        <i class="fa fa-clipboard hovicon effect-3 sub-b easePulse wow" data-wow-delay="0.8s"></i>
                        <h3 class="wow fadeIn m-10-top" data-wow-delay="1s">Manage Taxes and Records</h3>
                        <p align="justify" class="wow fadeIn" data-wow-delay="1s">Conveniently organized 1099 tax forms and legal documents for your records.</p>
                    </div>
                    <div class="space"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- Services section -->
    <section class="section-lg bg-grey">
        <div class="container">
            <div class="row">
                <!--fadeInLeft-->
                <div class=" col-sm-6 wow fadeInLeft">
                    <h3>Security and Identity Protection</h3>
                    <p>MeteorHealth uses TrueVault to safely secure health and demography data. Your information is never shared without your explicit permission with anyone.</p>
                    <div class="space-sm"></div>                  
                  
                </div>

                <div class=" col-sm-6 wow fadeInLeft">
                    <h3>How Much Does MeteorHealth Cost?</h3>
                    <p>Free for you! MeteorHealth works with Clinical Providers directly. All patient expenses are covered.</p>
                    <div class="space-sm"></div>                  
                  
                </div>
            </div>
        </div>
    </section>


    <section class="section-lg home-price">
        <div class="container">
            <h2 class="lg-title lg-title-border">Clinical Trials Seeking Participants Near You!</h2>
            <div class="space-sm"></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-default prices first bg-primary">
                        <div class="panel-body">
                            <div class="ribbon">Boston</div>

                            <i class="fa fa-star icon-u"></i>

                            <h2 class="val">Observational Sleep Study for 37 Days. Non-Medicated</h2>

                            
                            <div class="space-md"></div>
                            <a href="#" class="btn btn-default-trn">$10,125 <br />For Completion</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default prices  bg-primary">
                        <div class="panel-body ">
                            <div class="ribbon">Boston</div>

                            <i class="fa fa-star icon-u"></i>

                            <h2 class="val">Observational Sleep Study for 32 Days. Non-Medicated</h2>

                            
                            <div class="space-md"></div>
                            <a href="#" class="btn btn-default-trn">$7,500<br />For Completion</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default prices last bg-primary">
                        <div class="panel-body">
                            <div class="ribbon">Boston</div>

                            <i class="fa fa-star icon-u"></i>

                            <h2 class="val">Asthma Treatment Study for 14 Weeks. Medicated.</h2>

                            
                            <div class="space-md"></div>
                            <a href="#" class="btn btn-default-trn">$1,000<br />For Completion</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="">
        <section class="container section margin-b30 ">

        <h2 class="big-title text-center">MeteorHealth is Empowering Patients</h2>
        <p class="big-subtitle text-center">For those with health conditions or in pregnancy:</p>
        <div class="space"></div>
        <div class="space"></div>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-lock"></i>
                    <div class="float-text">
                        <h3>Protecting your Data and Identity</h3>
                        <p align="justify">MeteorHealth keeps your information safe and secure. Your personal identity will never be shared without your consent</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-bell"></i>
                    <div class="float-text">
                        <h3>Real Time Health and Safety Alerts</h3>
                        <p align="justify">Get tailored emergency alerts from local and state municipalities tailored to you and your health.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-group"></i>
                    <div class="float-text">
                        <h3>Stay Updated on New Trials</h3>
                        <p align="justify">Our platform sends alerts on the latest FDA trials or treatments relevant to you.  Find out about potentially life saving treatments near you.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="float-box">
                    <i class="fa icon-c fa-lightbulb-o"></i>
                    <div class="float-text">
                        <h3>Connect with Others in Your Area</h3>
                        <p align="justify">Message boards and support circles that can you help  find  answers to your most pressing and sensitive questions.</p>
                    </div>
                </div>
            </div>

          
        </div>
    </section>
    </div>



  
  

    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="assets/js/min/scripts.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.vide.min.js"></script>
<script>
  $(document).ready(function () {
    $('#block').vide('<?php echo base_url();?>assets/img/MeteorHealt'); // Non declarative initialization
//
//    var instance = $('#block').data('vide'); // Get the instance
//    var video = instance.getVideoObject(); // Get the video object
//    instance.destroy(); // Destroy instance
//
//    $('#block2').vide({
//      'mp4': 'video/ocean',
//      'webm': 'video/ocean',
//      'ogv': 'video/ocean',
//      'poster': 'video/ocean',
//    });
 });
</script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


    <!-- Footer-->
    <footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <h4>Contact</h4>
                    <ul class="info-list md-list i-primary">
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a href="mailto:">contact@meteorhealth.com</a>
                        </li>
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            +1 (617) 331-1689
                        </li>
                        <li>
                            <i class="fa fa-map-marker fa-fw"></i>
                            Soldiers Field 
203 Batten
Boston, MA 02163
                        </li>
                    </ul>
                </div>
                <div class="space-sm visible-sm visible-xs"></div>
                <div class="col-md-4 col-lg-4 col-xs-12">
                    <h4>Quick Links</h4>
                    <ul class="info-list md-list i-primary">
                       <!-- <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>/home/become_jobrabbit">
                                <span class="link-title">Become A Meteor</a>
                        </li> -->
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>/home/how_it_works">
                                <span class="link-title">How it works</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>/home/login/participant">
                                <span class="link-title">Participant Login</a>
                        </li>
<li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>/home/login/provider">
                                <span class="link-title">Provider Login</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>/home/signup">
                                <span class="link-title">Signup</a>
                        </li>
                        
                    </ul>
                </div>
                <div class="space-sm visible-sm visible-xs"></div>
                <div class="col-md-4 col-lg-4  col-xs-12">
                    <h4>About Us</h4>
                    <p>MeteorHealth is a technology platform that seeks to accelerate medical innovation by making clinical trials faster, cheaper and easier to run.</p>
                    <div class="space-sm"></div>
                    <h4>Follow Us</h4>
                    <ul class="circle-icons icons-list">
                        <li>
                            <a href="<?php echo $twitter_link?>" target="_blank" title="Follow us">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                       <li>
                            <a href="<?php echo $facebook_link?>" target="_blank" title="Follow us">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>