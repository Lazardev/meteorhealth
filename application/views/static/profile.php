<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#job_type").on("change",function(){
                var job_type = $("#job_type").val();
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>home/get_category/", //controller url
                    data:"job_type="+job_type,
                    success:function(data)
                    {
                        $('#divCategory').html(data);
                        $('#divSubCategory').find('select').html('<option>-Select One-</option>').attr('disabled', 'disabled');
                    }
                });
            });
           
            $("#divCategory").on("change",function(){
                var category_parent_id = $("#category").val();
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>home/get_subcategory/",
                    data:"category_parent_id="+category_parent_id,
                    success:function(data)
                    {
                        $('#divSubCategory').html(data);
                    }
                });
            });

            $("#divSubCategory").on("change",function(){
                
                     document.getElementById("post_task").disabled=false;
                  
            });
           
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  </script>
    <style>
    .modal-body{
        padding: 15px 0px;
    }
    .nav-tabs{
        width: 100%;
        padding: 0px 35px;
        margin-bottom: 3px;
    }
    .nav-tabs>li{
        width: 50%;
    }
    .nav-tabs>li>a{
        border: 0px;
        border-radius: 4px;
    }
    .nav-tabs{
        border: 0px;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        border: 0px;
        background-color: #3c948b;
        border-color: #3c948b;
        color: #fff;
    }
    .nav-tabs>li>a:hover{
        border: 0px;
        background-color: #3c948b;
        border-color: #3c948b;
        color: #fff;
    }
    .tab-content{
        padding: 15px 15px 0px;border-top: 1px solid #ddd;
    }
    </style>
    <body>
<div class="col-sm-12 fadeInRight animated panel pad-m">
<ul class="nav nav-tabs" role="tablist">
            <li class="active">
                <a href="#profile" data-toggle="tab">Profile</a>
            </li>
            <li class=" ">
                <a href="#reviews" data-toggle="tab">Reviews</a>
            </li>
   </ul>

  <!-- <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
   </ul> -->

  <!-- Tab panes -->
 <div class="tab-content">
        <div class="tab-pane active" id="profile">
                 <div class="animated fadeInLeft">
                        <h3>A few fun facts about me:</h3>

                        <img src="<?php echo base_url();?>uploads/profiles/<?php echo $profile->user_image?>" class="user-img">
                    <div class="space-sm"></div>
                    <p><i class="fa fa-user"></i> <?php echo $profile_user->full_name; ?></p>
                    <p><i class="fa fa-envelope-o"></i> <?php echo $profile_user->emailid; ?></p>
                    <p><i class="fa fa-phone"></i> <?php echo $profile->phone; ?></p>
                    <p><i class="fa fa-map-marker"></i><?php echo $profile_user->zipcode; ?></p>

                        <h3>Why I'm your Tasker:</h3>

                        <h4><strong>I'm the right person for the job...</strong></h4>
                        <p>I am always striving to provide a superior service. I’m a very hands-on person & love to multi-task.. I always maintain a positive, cheerful, & friendly personality so I’m great with people. Remarkably good organizational skills. Exceptional cleaning habits. Very great at working with others.</p>

                        <h4><strong>When I'm not tasking...</strong></h4>

                        <p>I currently work nights for the Atlanta Journal Constitution. I'm also a musician, so I spend free time working on musical projects. I'm really into Self-Help information, so I read a lot & listen to plenty of audios on self-improvement.</p>

                        <h4><strong>When I'm tasking I always make sure to...</strong></h4>
                        <p>Leave my clients 100% Satisfied, Happy, & eager to use my services again.</p>


                    </div>
        </div>
        <div class="tab-pane " id="reviews">
        <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>

                    <div class="testi bg-white row">
                        <div class="left">
                            <img src="<?php echo base_url();?>assets/img/user.png" />
                        </div>
                        <div class="right">
                            <p>"Zadok was wonderful, once again! He is friendly and efficient! "</p>
                            <small>- November 24, 2015</small>
                        </div>
                    </div>  
        </div>
     </div>
     </div>
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



     </body>