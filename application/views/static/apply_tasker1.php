<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />
<script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href=''><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href=""><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li><a href="<?php echo base_url();?>home/dash_worker">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs_task">Trials</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>




            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>
    

<div class="bg-grey-1">
    <section class="container section">

        <h2>Register to become a Participant in clinical trials and studies</h2>

      
        <div class="space"></div>
        
        <div class="panel panel-default pad-m">
            <h3>Tell us about yourself</h3>
            <hr>
            
            <form action="<?php echo base_url();?>home/dash_worker" enctype="multipart/form-data" class="no-icon" method="post">
                <div class="form-group">
                    <label>Username (to ensure anonymity of your medical data):</label>
                    <input type="text"  class="form-control" required name="username" placeholder="Please select a Username">
                </div>
                <div class="form-group">
                    <label>Birthday: </label>
                    <input type="date" id="datepicker" class="form-control" required name="dob" placeholder="DOB">
                </div>

                <div class="form-group">
                    <label>Phone Number:</label>
                    <input type="number" class="form-control" value="<?php echo $mobile;?>" required name="contact" placeholder="Mobile Number">
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <textarea class="form-control" rows="6" name="address" required placeholder="Address"></textarea>
                </div>
                <div class="form-group">
                    <label>Profile Image (Optional)</label>
                                 <div class="controls" >
                                     <input name="image" type="file"/>
                                </div>
                            </div>
                <div class="form-group">
                    <label>Ethnicity</label>
                   <select name="ethincity" required class="form-control">
                        <option value="Native American">Native American</option>
                        <option value="Asian">Asian</option>
                        <option value="Black or African American">Black or African American</option>
                        <option value="Native Hawaiian or other Pacific Islander">Native Hawaiian or other Pacific Islander</option>    
                        <option value="Caucasian">Caucasian</option>
                        <option value="Latino or Hispanic">Latino or Hispanic</option>
                        <option value="Multiracial">Multiracial</option>
                        <option value="Other">Other</option>    
                    </select>
                </div>

                <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" required class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option> 
                        <option value="Transgender">Transgender</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Use Tobacco?</label>
                    <select name="tobacco" required class="form-control">
                        <option value="No">No</option>
                        <option value="I used to in the past">I used to in the past</option> 
                        <option value="Yes">Yes</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Veteran Status?</label>
                    <select name="veteran" required class="form-control">
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Are you a Twin or Triplet</label>
                    <select name="twin" required class="form-control">
                        <option value="No">No</option>
                        <option value="Twin">Twin</option> 
                        <option value="Triplet">Triplet</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Zipcode</label>
                    <input type="text" name="zip" value="<?php echo $zip; ?>" required class="form-control" placeholder="Zip">
                </div>
                <div class="form-group">
                    <label>Height</label>
                    <select name="feet" placeholder="feet" required >
                        <option value="3">3</option>
                        <option value="4">4</option> 
                        <option value="5">5</option>  
<option value="6">6</option>
<option value="7">7</option>  
                    </select>
                    <select name="inches" required placeholder="inches" >
                        <option value="1">1</option>
                        <option value="2">2</option> 
                        <option value="3">3</option>  
                        <option value="4">4</option>
                        <option value="5">5</option> 
                        <option value="6">6</option> 
                        <option value="7">7</option>  
                        <option value="8">8</option>
                        <option value="9">9</option> 
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option> 
                           
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Weight:</label>
                    <input type="number" name="weight" value="" required class="form-control" placeholder="Lbs">
                </div>
               <div class="form-group">
                    <label>Have you ever been diagnosed with any of the following medical conditions?</label> <br />
               <div class="form-group col-sm-4">
                            <input type="checkbox" name="chk_group[]" value="Benign Prostatic Hyperplasia" /> Benign Prostatic Hyperplasia <br />
                            <input type="checkbox" name="chk_group[]" value="Breast Cancer" /> Breast Cancer<br />
                            <input type="checkbox" name="chk_group[]" value="Colorectal Cancer" /> Colorectal Cancer<br />
                            <input type="checkbox" name="chk_group[]" value="Diabetes" /> Diabetes<br />
                            <input type="checkbox" name="chk_group[]" value="Erectile Dysfunction" /> Erectile Dysfunction<br />
                            <input type="checkbox" name="chk_group[]" value="Heart Attack" /> Heart Attack<br />

                        </div>
               <div class="form-group col-sm-4">
                            <input type="checkbox" name="chk_group[]" value="Hypertension" /> Hypertension<br />
                            <input type="checkbox" name="chk_group[]" value="Infertility" /> Infertility<br />
                            <input type="checkbox" name="chk_group[]" value="Kidney Stones" /> Kidney Stones<br />
                            <input type="checkbox" name="chk_group[]" value="Lung Cancer" /> Lung Cancer<br />
                            <input type="checkbox" name="chk_group[]" value="Melanoma" /> Melanoma<br />
                            <input type="checkbox" name="chk_group[]" value="Prostate Cancer" /> Prostate Cancer<br />

                        </div>
               <div class="form-group col-sm-4">
                            <input type="checkbox" name="chk_group[]" value="Rheumatoid Arthritis"/> Rheumatoid Arthritis<br />
                            <input type="checkbox" name="chk_group[]" value="Stroke"/> Stroke<br />
                            <input type="checkbox" name="chk_group[]" value="Urinary Incontinence"/> Urinary Incontinence<br />
                            <input type="checkbox" name="chk_group[]" value="Urinary Tract Infection"/> Urinary Tract Infection<br />
                            
                            <input type="text" name="other" /> Other (We may not support these but we'll let you know as soon as we do)<br />

                        </div>
               </div>
                <div class="form-group">
                    <label>Please list any prescription medication you are taking?</label>
                   <textarea required name="prescription" class="form-control" placeholder=""></textarea>
                </div>
                <div class="form-group">
                    <label>Some Studies may required travelling to a specific site. How far would you be willing to travel for these types of studies?</label>
                    <select name="travel_distance" required class="form-control">
                        <option value="">Select</option>
                        <option value="<50 miles"><50 miles</option>
                        <option value="<100 miles"><100 miles</option> 
                        <option value="<150 miles"><150 miles</option>
                        <option value="<200 miles"><200 miles</option>    
                        <option value="<300 miles"><300 miles</option>    

                    </select>                    
                </div>
                <div class="form-group">
                    <label>Would you like to be contacted to studies related to being a parent?</label>
                    <select name="studies_parent" required class="form-control">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option> 
                            

                    </select>                    
                </div>
                <div class="form-group">
                    <label>Is there any additional information that you think we might find helpful in matching you with studies that you might be interested in?</label>
                   <textarea required name="additional" class="form-control" placeholder=""></textarea>
                </div>
               
            <button type="submit" class="btn btn-primary">Submit</button>
</form>
        </div>
        
    </section>

</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>