

<?php 
$profile = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = '$user_id'")->row();
$profile_user = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row();
$zip = $profile_user->zipcode;
$availability_time = '';
switch ($profile->availability_time) {
    case 'any':
        $availability_time = 'Any Time 8am - 8pm';
        break;
    
    case 'morning':
        $availability_time = 'Morning 8am - 12pm';
        break;
    case 'aft':
        $availability_time = 'Afternoon 12pm - 4pm';
        break;
    case 'eve':
        $availability_time = 'Evening 4pm - 8pm';
        break;

}
?>

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_task_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
              if(typeof result['chat']['message'] ==='undefined')
             {
                
             }
              else if(result['chat']['message']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat_task/<?php echo $user_id; ?>/'+result['chat']['user']+'">New Message</a>'); // here the wrapper is main div
             
             }
             
             $("#assign_not").html(result['jobs_assigned']);
             $("#open_not").html(result['jobs_open']);
             $("#first_not").html(result['jobs_first']);
              }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script>
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  </script>
<style type="text/css">
            div#map {
                position: relative;
            }

            div#crosshair {
                position: absolute;
                top: 192px;
                height: 19px;
                width: 19px;
                left: 50%;
                margin-left: -8px;
                display: block;
                background: url(crosshair.gif);
                background-position: center center;
                background-repeat: no-repeat;
            }
        </style>


    <!--  Boostrap Framework  -->
    <!-- /<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">




</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;
        $zip = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row()->zipcode;
        ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_worker'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_worker"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">
                <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_worker">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                 <li><a href="<?php echo base_url();?>home/jobs_task">Trials</a></li>
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->

    <section class="bg-9 bg-cover bg-center">
        <div class="bg-filter">
            <div class="container section-lg">
                <div class="space"></div>
                <div class="space"></div>
                <!-- <h1 class="top-title">Welcome!</h1> -->
                <div class="sub-text text-center">
                <?php if($profile->user_image != '') {?>
                    <img src="<?php echo base_url();?>uploads/profiles/<?php echo $profile->user_image; ?>" class="users">
                    <?php } else { ?>
                    <img src="<?php echo base_url();?>uploads/profiles/user-default.jpg" class="users">
                   <?php }?>
                    <h2>Hello, <?php echo $profile_user->full_name; ?>.</h2>
                    
                    
                    </div>
                </div>
            </div>
    </section>
<div class="bg-grey-1">
    <section class="container section-lg">

        <!-- Nav tabs -->
        <ul class="inline-tabs">
            <li class="active">
                <a href="#jobs" data-toggle="tab">Trials near you</a>
            </li>
           <!--  <li class="">
                <a href="#hire" data-toggle="tab">Availability</a>
            </li> -->
            <li class="">
                <a href="#review" data-toggle="tab">Preferences</a>
            </li>
            <li class="">
                <a href="#abt" data-toggle="tab">Personal Information</a>
            </li>
           
        </ul>
        <style>
        .t-number{
            position: absolute;
            top: -2px;
            right: -23px;
            width: 20px;
            height: 20px;
            line-height: 20px;
            text-align: center;
            border-radius: 100%;
            background: #3c948b;
            color: #fff;
            display: inline-block;
            box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.1);
            font-size: 12px;
            font-weight: 700;
        }
        .t-number.t-small{
            top: 15px;
            right: 5px;
         } 
         .inline-tabs > li.active a > .t-number{
  background: #fff;
  color: #000;
}
  
        </style>
        <!-- Tab panes -->
        <div class="tab-content">
        <div class="tab-pane active" id="jobs">
          <ul class="inline-tabs">
            <!-- <li class="active">
                <a href="#assigned" data-toggle="tab" style="position: relative;">Assigned Trials <span id="assign_not" class="t-number"></span></a>
            </li> -->
            <li class="">
                <a href="#open" data-toggle="tab">Pending Trials <span id="open_not" class="t-number t-small"></span></a>
            </li>
            <li class="">
                <a href="#first" data-toggle="tab">Upcoming Trials <span id="first_not" class="t-number t-small"></span></a>
            </li>
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="assigned">
       <div class="row">
       <?php
       
        $assigned = $this->db->query("SELECT * FROM jobs WHERE jassign_to_work_id = $user_id AND jbidding_type=2 AND jwork_status=2")->result_array();
            foreach ($assigned as $assign) {
                $cat_id =  $assign['category_id'];
                $cat_name = $this->db->query("SELECT * FROM job_category WHERE category_id = $cat_id")->row()->category_name;
       ?>
                   
                    <div class="col-sm-12 fadeInRight animated panel pad-m clearfix">
                    <div class="item-map" style="padding: 20px;box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.5);">
                        <h3 class="m-no-top" id="<?php echo $assign['jname'].'title';?>"><?php echo $cat_name .' - '.$assign['jname']?></h3> Job assigned to you on <?php echo $assign['jassign_date']; ?> <br />
                        <h4>Trial Description </h4>
                    <div class="col-md-10"><?php echo $assign['desc_study'];
                    $job_id = $assign['job_id'];
                    
            ?></div>

            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="checking(<?php echo $assign['job_id']?>, '<?php echo $cat_name;?>');" class="btn btn-primary pull-right" style="position: relative;top: -34px;">View More</a>
<script>
                        function checking(id,cat_name){
                            $(document).ready(function() {

    $.ajax({url : '<?php echo base_url();?>home/job_details/'+id,
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
        $('#modal-gender').val(result['gender']);
        $('#modal-ethincity').val(result['ethincity']);
        $('#modal-target_veterans').val(result['target_veterans']);
        $('#modal-target_twins').val(result['target_twins']);
        $('#modal-inclusion').val(result['inclusion']);
        $('#modal-exclusion').val(result['exclusion']);
        $('#modal-add_info').val(result['add_info']);
        $('#modal-title_study').val(result['title_study']);
        $('#modal-desc_study').val(result['desc_study']);
        $('#modal-duration').val(result['duration']);
        $('#modal-duration_types').val(result['duration_types']);
        $('#modal-inperson_visits').val(result['inperson_visits']);
        $('#modal-duration_visits').val(result['duration_visits']);
        $('#modal-duration_time').val(result['duration_time']);
        $('#modal-total_compensation').val(result['total_compensation']);
        $('#modal-travel_expense').val(result['travel_expense']);
        $('#modal-zipcode').val(result['zipcode']);
        $('#modal-jpay').html('$    '+result['jassign_amt']);
        $('#modal-jaddress').html(result['jaddress']);
        $('#modal-bidding_from').html(result['jbidding_st_date']); 
        $('#modal-bidding_to').html(result['jbidding_ed_date']);
        $('#modal-job').val(id);
        $('#modal-amt').val(result['jassign_amt']);
            
            
            
           //console.log(result['advert']) // The value of your php $row['adverts'] will be displayed
        }
    });
                        });
                    }
                    </script>
  
                    <?php }?>
        </div>

   </div>
        <div class="tab-pane " id="open">
               <div class="row">
       <?php $assigned = $this->db->query("SELECT * FROM `jobs` WHERE `jbidding_type`=1 AND `jwork_status`=1")->result_array();
            foreach ($assigned as $assign) {
                $job_id = $assign['job_id'];
                $bidded = $this->db->query("SELECT * FROM `job_bids` WHERE `job_id`=$job_id AND `userid`=$user_id")->row();
                if($job_id != $bidded->job_id){
                $cat_id =  $assign['category_id'];
                $cat_name = $this->db->query("SELECT * FROM job_category WHERE category_id = $cat_id")->row()->category_name;
     
switch ($assign['jbilling_by'])
                                        {
                                            case '1':
                                                $price = $assign['jbilling_no_hours']*$assign['jposter_price'];
                                            break;
                                            case '2':
                                                $price = $assign['jbilling_no_days']*$assign['jposter_price'];
                                            break;
                                            
                                        }
                                        
                
       ?>
                   
                    <div class="col-sm-12 fadeInRight animated panel pad-m clearfix">
                    <div class="item-map" style="padding: 20px;box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.5);">
                        <h3 class="m-no-top" id="<?php echo $assign['jname'].'title';?>"><?php echo $cat_name; ?></h3> Job posted on <?php echo $assign['jposted_date']; ?> <br />
                        <h4>Trial Description </h4>
                    <?php echo $assign['desc_study'];

                    $job_id = $assign['job_id'];
                    
            ?>

            <a href="#"  data-toggle="modal" data-target="#myModalopen" onclick="checking_open('<?php echo $assign['job_id']?>',<?php echo $price; ?>,'<?php echo $cat_name;?>');"  class="btn btn-primary pull-right" style="position: relative;top: -34px;">Apply</a>
                       </div>
                    </div>
                    
                    
        <?php }}?>
        </div>
        </div>
        <div class="tab-pane " id="first">
        <div class="row">
       <?php $assigned = $this->db->query("SELECT * FROM `jobs` WHERE  `jbidding_type`=3 AND `jwork_status`=1 ")->result_array();
            foreach ($assigned as $assign) {
                $cat_id =  $assign['category_id'];
                $cat_name = $this->db->query("SELECT * FROM job_category WHERE category_id = $cat_id")->row()->category_name;
     
                switch ($assign['jbilling_by'])
                                        {
                                            case '1':
                                                $price =$assign['jbilling_no_hours']*$assign['jposter_price'];
                                            break;
                                            case '2':
                                                $price =$assign['jbilling_no_days']*$assign['jposter_price'];
                                            break;
                                            
                                        }
                
       ?>
                   
                    <div class="col-sm-12 fadeInRight animated panel pad-m clearfix">
                    <div class="item-map" style="padding: 20px;box-shadow: 0px 0px 2px 0px rgba(0,0,0,0.5);">
                        <h3 class="m-no-top" id="<?php echo $assign['jname'].'title';?>"><?php echo $cat_name;?></h3> Job posted on <?php echo $assign['jposted_date']; ?> <br />
                       
                    <?php 
                    $job_id = $assign['job_id'];
                    
            ?>
            <a href="#" data-toggle="modal" data-target="#myModalfirst" onclick="checking_open('<?php echo $assign['job_id']?>',<?php echo $price; ?>,'<?php echo $cat_name;?>');"  class="btn btn-primary pull-right" style="position: relative;top: -34px;">Apply</a>
            
                    </div>
                    </div>
                    
        <?php }?>
        </div>
        </div>
        </div>
</div>
  <script>
                        function checking_open(id,price,cat_name){
              $(document).ready(function() {

    $.ajax({url : '<?php echo base_url();?>home/job_details/'+id,
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
         async: true,
        success : function (result) {
            console.log(result);
    $('#modal-gender1').val(result['gender']);
        $('#modal-ethincity1').val(result['ethincity']);
        $('#modal-target_veterans1').val(result['target_veterans']);
        $('#modal-target_twins1').val(result['target_twins']);
        $('#modal-inclusion1').val(result['inclusion']);
        $('#modal-exclusion1').val(result['exclusion']);
        $('#modal-add_info1').val(result['add_info']);
        $('#modal-title_study1').val(result['title_study']);
        $('#modal-desc_study1').val(result['desc_study']);
        $('#modal-duration1').val(result['duration']);
        $('#modal-duration_types1').val(result['duration_types']);
        $('#modal-inperson_visits1').val(result['inperson_visits']);
        $('#modal-duration_visits1').val(result['duration_visits']);
        $('#modal-duration_time1').val(result['duration_time']);
        $('#modal-total_compensation1').val(result['total_compensation']);
        $('#modal-travel_expense1').val(result['travel_expense']);
        $('#modal-zipcode1').val(result['zipcode']);
        $('#modal-jdescription1').html(result['jdescription']);
        $('#modal-jaddress1').html(result['jaddress']);
        $('#modal-bidding_from').html(result['jbidding_st_date']); 
        $('#modal-bidding_to').html(result['jbidding_ed_date']);
        $('#modal-price').val(price);
        $('#modal-job1').val(id);
        }
    });
                        });
                    }
                    </script>
            <div class="tab-pane " id="hire">
            <form action="<?php echo base_url();?>home/tasker_update" method="post">
                <div class="row">
                    <div class="col-sm-12 fadeInRight animated panel pad-m">
                    <h3 class="m-no-top">Available</h3> 
                        <label class="form-group" for="from">From</label>
<input type="text" class="form-control" id="from" value="<?php echo date("d/m/Y", strtotime($profile->available_from) );?>" name="from">
<label class="form-group" for="to">To</label>
<input class="form-control" type="text" id="to" value="<?php echo date("d/m/Y", strtotime($profile->available_to) );?>" name="to">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 fadeInRight animated panel pad-m">
                        <h3 class="m-no-top">Available Time Slots</h3>
                        <div class="form-group">
                    
                    <select name="time" required class="form-control">
                    <?php if($profile->availability_time != '') {?>
                        <option value="<?php echo $profile->availability_time; ?>"><?php echo $availability_time; ?></option>
                        <?php }?>
                        <option value="any">Any Time 8am - 8pm</option>
                        <option value="morning">Morning 8am - 12pm</option>
                        <option value="aft">Afternoon 12pm - 4pm</option>                                    
                        <option value="eve">Evening 4pm - 8pm</option>   
                    </select>                    
                </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
</form>
            </div>
            <div class="tab-pane" id="review">
            <div class="testi bg-white row">
            <ul class="inline-tabs">
            <li class="active">
                <a href="#virtual" data-toggle="tab">Medicated</a>
            </li>
            <li class="">
                <a href="#inperson" data-toggle="tab">Non-Medicated</a>
            </li>
            <li class="">
                <a href="#delivery" data-toggle="tab">Observational</a>
            </li>
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="virtual">
        <form action="<?php echo base_url();?>home/tasker_update" method="post">
            Medicated studies test participants' response to medical treatment. <br />
Which type of studies are you willing to participate in? What is your minimum required hourly rate? <br />
                <?php $categories = $this->db->query("SELECT * FROM `job_category` WHERE category_job_type =1")->result_array();
foreach ($categories as $category) {
    $cat_name = str_replace(" ", "_", $category['category_name']);
    $cat_price = $this->db->query("SELECT $cat_name FROM `categories` WHERE user_id =$user_id")->row()->$cat_name;
    ?>
                        <div class="col-sm-6 form-group">
                            <input type="checkbox" name="chk_group[]" <?php if($cat_price !='0' && isset($cat_price)) { ?> checked <?php }?>  value="<?php echo $cat_name; ?>" /> <label ><?php echo $category['category_name']?></label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="form-control" name="<?php echo $cat_name; ?>" value="<?php echo $cat_price;?>"  placeholder="Price per Hour" />
                        </div>
    <?php } ?>
<button type="submit" class="btn btn-primary">Save</button>
                    
</form>
        </div>
               <div class="tab-pane " id="inperson">
        <form action="<?php echo base_url();?>home/tasker_update" method="post">
            Non-Medicated studies test participants' response to changes their daily behavior (e.g., sleep or diet patterns). <br />
Which type of studies are you willing to participate in? What is your minimum required hourly rate? <br />
                <?php $categories = $this->db->query("SELECT * FROM `job_category` WHERE category_job_type =2")->result_array();
foreach ($categories as $category) {
    $cat_name = str_replace(" ", "_", $category['category_name']);
    $cat_price = $this->db->query("SELECT $cat_name FROM `categories` WHERE user_id =$user_id")->row()->$cat_name;

    ?>
                        <div class="col-sm-6 form-group">
                            <input type="checkbox" name="chk_group[]" <?php if($cat_price !='0' && isset($cat_price)) { ?> checked <?php }?>  value="<?php echo $cat_name; ?>" /> <label ><?php echo $category['category_name'].$cat_price?></label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="form-control" name="<?php echo $cat_name; ?>" value="<?php echo $cat_price;?>"  placeholder="Price per Hour" />
                        </div>
    <?php } ?>
<button type="submit" class="btn btn-primary">Save</button>       
</form>
        </div>
               <div class="tab-pane " id="delivery">
        <form action="<?php echo base_url();?>home/tasker_update" method="post">
             Select desired trial category and price per hour. <br />
               <?php $categories = $this->db->query("SELECT * FROM `job_category` WHERE category_job_type =3")->result_array();
foreach ($categories as $category) {
    $cat_name = str_replace(" ", "_", $category['category_name']);
    $cat_price = $this->db->query("SELECT $cat_name FROM `categories` WHERE user_id =$user_id")->row()->$cat_name;
    ?>
                        <div class="col-sm-6 form-group">
                            <input type="checkbox" name="chk_group[]" <?php if($cat_price !='0' && isset($cat_price)) { ?> checked <?php }?>  value="<?php echo $cat_name; ?>" /> <label ><?php echo $category['category_name']?></label>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input type="text" class="form-control" name="<?php echo $cat_name; ?>" value="<?php echo $cat_price;?>"  placeholder="Price per Hour" />
                        </div>
    <?php } ?>
<button type="submit" class="btn btn-primary">Save</button>
                    
</form>
        </div>
        </div>
        </div>
            </div>
            <div class="tab-pane" id="abt">
                <div class="row">
                    <div class="col-sm-12 animated fadeInLeft">
<h3>Summery Statistics about <?php echo $profile->username; ?>:</h3>

                        <p><i class="fa fa-calendar"></i> I’ve been on Meteor since <?php echo  date("Y", strtotime($profile_user->signup_date));?>.</p>
                        <p><i class="fa fa-check"></i> I’ve participated in <?php echo $this->db->query("SELECT * FROM `jobs` WHERE `jassign_to_work_id` = $user_id AND `jwork_status` = 6 ")->num_rows(); ?> trials.</p>
<br />
<a id="myButton" class="pull-right" style="font-size: 14px;font-weight: 400; cursor:pointer;"><i class="fa fa-pencil"></i>Edit</a> <br />

<script>
document.getElementById('myButton').onclick = function() {
    document.getElementById('myInput').readOnly = false;
document.getElementById('myInput').readOnly = false;
document.getElementById('myInput1').readOnly = false;
document.getElementById('myInput2').readOnly = false;
document.getElementById('myInput3').readOnly = false;
document.getElementById('myInput4').disabled = false;
document.getElementById('myInput5').disabled = false;
document.getElementById('myInput6').disabled = false;
document.getElementById('myInput7').disabled = false;
document.getElementById('myInput8').disabled = false;
document.getElementById('myInput9').readOnly = false;
document.getElementById('myInput10').disabled = false;
document.getElementById('myInput11').disabled = false;
document.getElementById('myInput12').readOnly = false;
document.getElementById('myInput13').readOnly = false;
document.getElementById('myInput14').disabled = false;
document.getElementById('myInput15').disabled = false;
document.getElementById('myInput16').readOnly = false;


};
</script>

                        <form action="<?php echo base_url();?>home/dash_worker" enctype="multipart/form-data" class="no-icon" method="post">
                <div class="form-group">
                    <label>Username (to ensure anonymity of your medical data):</label>
                    <input type="text" id="myInput" readonly="readonly" class="form-control" value="<?php echo $profile->username; ?>" required name="username" placeholder="Please select a Username">
                </div>
                <div class="form-group">
                    <label>Birthday: </label>
                    <input type="date" id="myInput1" readonly="readonly" id="datepicker" value="<?php echo $profile->dob; ?>" class="form-control" required name="dob" placeholder="DOB">
                </div>

                <div class="form-group">
                    <label>Mobile Number:</label>
                    <input type="number" id="myInput2" readonly="readonly" class="form-control" value="<?php echo $profile->mobile;?>" required name="contact" placeholder="Mobile Number">
                </div>
                <div class="form-group">
                    <label>Address:</label>
                    <textarea class="form-control" rows="7" id="myInput3" readonly="readonly" rows="6" name="address" required placeholder="Address"><?php echo $profile->address; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Ethnicity</label>
                   <select name="ethincity" id="myInput4" disabled="true" required class="form-control">
                        <option value="<?php echo $profile->ethincity; ?>"><?php echo $profile->ethincity; ?></option>
                        <option value="Native American">Native American</option>
                        <option value="Asian">Asian</option>
                        <option value="Black or African American">Black or African American</option>
                        <option value="Native Hawaiian or other Pacific Islander">Native Hawaiian or other Pacific Islander</option>    
                        <option value="Caucasian">Caucasian</option>
                        <option value="Latino or Hispanic">Latino or Hispanic</option>
                        <option value="Multiracial">Multiracial</option>
                        <option value="Other">Other</option>    
                    </select>
                </div>

                <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" id="myInput5" disabled="true" required class="form-control">
                        <option value="<?php echo $profile->gender; ?>"><?php echo $profile->gender; ?></option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option> 
                        <option value="Transgender">Transgender</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Use Tobacco?</label>
                    <select name="tobacco" id="myInput6" disabled="true" required class="form-control">
                        <option value="<?php echo $profile->tobacco; ?>"><?php echo $profile->tobacco; ?></option>
                        <option value="No">No</option>
                        <option value="I used to in the past">I used to in the past</option> 
                        <option value="Yes">Yes</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Veteran Status?</label>
                    <select name="veteran" id="myInput7" disabled="true" required class="form-control">
                        <option value="<?php echo $profile->veteran; ?>"><?php echo $profile->veteran; ?></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Are you a Twin or Triplet</label>
                    <select name="twin" id="myInput8" disabled="true" required class="form-control">
                        <option value="<?php echo $profile->twin; ?>"><?php echo $profile->twin; ?></option>
                        <option value="No">No</option>
                        <option value="Twin">Twin</option> 
                        <option value="Triplet">Triplet</option>    
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Zipcode</label>
                    <input type="text" id="myInput9" readonly="readonly" name="zip" value="<?php echo $zip; ?>" required class="form-control" placeholder="Zip">
                </div>
                <div class="form-group">
                    <label>Height</label>
                    <select id="myInput10" disabled="true" name="feet" placeholder="feet" required >
                        <option value="<?php echo $profile->feet; ?>"><?php echo $profile->feet; ?></option>
                        <option value="3">3</option>
                        <option value="4">4</option> 
                        <option value="5">5</option>  
<option value="6">6</option>
<option value="7">7</option>  
                    </select>
                    <select id="myInput11" disabled="true" name="inches" required placeholder="inches" >
                        <option value="<?php echo $profile->inches; ?>"><?php echo $profile->inches; ?></option>
                        <option value="1">1</option>
                        <option value="2">2</option> 
                        <option value="3">3</option>  
                        <option value="4">4</option>
                        <option value="5">5</option> 
                        <option value="6">6</option> 
                        <option value="7">7</option>  
                        <option value="8">8</option>
                        <option value="9">9</option> 
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option> 
                           
                    </select>                    
                </div>
                <div class="form-group">
                    <label>Weight:</label>
                    <input type="number" id="myInput12" readonly="readonly" name="weight" value="<?php echo $profile->weight; ?>" required class="form-control" placeholder="Lbs">
                </div>
               
                <div class="form-group">
                    <label>Please list any prescription medication you are taking?</label>
                   <textarea  id="myInput13" readonly="readonly" name="prescription" class="form-control" placeholder=""><?php echo $profile->prescription; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Some Studies may required travelling to a specific site. How far would you be willing to travel for these types of studies?</label>
                    <select id="myInput14" disabled="true" name="travel_distance"  class="form-control">
                        <option value="<?php echo $profile->travel_distance; ?>"><?php echo $profile->travel_distance; ?></option>
                        <option value="<50 miles"><50 miles</option>
                        <option value="<100 miles"><100 miles</option> 
                        <option value="<150 miles"><150 miles</option>
                        <option value="<200 miles"><200 miles</option>    
                        <option value="<300 miles"><300 miles</option>    

                    </select>                    
                </div>
                <div class="form-group">
                    <label>Would you like to be contacted to studies related to being a parent?</label>
                    <select id="myInput15" disabled="true" name="studies_parent" class="form-control">
                        <option value="<?php echo $profile->studies_parent; ?>"><?php echo $profile->studies_parent; ?></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option> 
                            

                    </select>                    
                </div>
                <div class="form-group">
                    <label>Is there any additional information that you think we might find helpful in matching you with studies that you might be interested in?</label>
                   <textarea id="myInput16" readonly="readonly" name="additional" class="form-control" placeholder=""><?php echo $profile->additional; ?></textarea>
                </div>
         <br>
                <button type="submit" class="btn btn-primary">Save</button>              
</form>
                    </div>
                    
                </div>
            </div>
           </div>
        </div>
        </div>

    </section>
</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>



        <!-- Modal -->

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title m-no-top" id="modal-jname"></h4>
              </div>
              <div class="modal-body">
             <form class="icon" action="<?php echo base_url();?>home/job_accept" method="post">
               <a href="#" class="btn btn-primary pull-right" id="modal-jpay" style="position: relative; bottom:9px;"></a>
               <label class="form-group" for="from"> Anything you wanna say(if rejects)</label>
        <textarea name="comments"  rows="5" cols="40" class="form-control" ></textarea>
 <br>
                
                <input name="job_id" type="hidden" id="modal-job"  value="" class="form-control" />
 <div class="form-group">
         <label class="form-group">Ethincity: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-ethincity">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Gender: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-gender">
            </div>
  </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target veterans?: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-target_veterans">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target twins / triplets?: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-target_twins">
            </div>
  </div>
 <div class="form-group">
        <label class="form-group">Briefly Describe Inclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-inclusion"></textarea>
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Briefly Describe Exclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-exclusion"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Additional Information: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-add_info"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Title of the Study: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-title_study">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Simple Description of Study: </label>
            <div class="control">
                <textarea class="form-control" rows="7"  readonly value="" id="modal-desc_study"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Duration: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration"><input  type="text" readonly value="" id="modal-duration_types">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Number of in-person visits required: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-inperson_visits">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Duration of Each Visit: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration_visits">
                <input type="text" readonly value=""  id="modal-duration_time">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Compensation: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-total_compensation">
                </div>
 </div>
 <div class="form-group">
         <label class="form-group">Travel Expenses Covered: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-travel_expense">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Zipcode: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-zipcode"/>
            </div>
 </div>

                </div>

              <div class="modal-footer">

        <input type="submit" name="submit" value="Reject" class="btn btn-default" data-dismiss="modal" />
        <input type="submit" name="submit" value="Accept a Job" class="btn btn-primary" />
        </form>
      </div>
            </div>
          </div>
        </div>


  <div class="modal fade" id="myModalopen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title m-no-top" id="modal-jname1"></h4>
              </div>
              <div class="modal-body">
              <form class="icon" action="<?php echo base_url();?>home/wbid" method="post">
             
              Bidding Starting from <label class="form-group" id="modal-bidding_from"></label>
              to <label class="form-group" id="modal-bidding_to"></label> <br>              
         <input name="job_id" type="hidden" id="modal-job1"  value="" class="form-control" />
                
   <br>
        <div class="form-group"> 
        <label class="form-group">Open Bidding Amount: </label>
            <div class="control">
                <input class="form-control" readonly name="amount" type="text" value="" id="modal-price">
            </div>
  </div>
        <label class="form-group" for="from">Anything you wanna say</label>
        <textarea name="commands" required rows="5" cols="40" class="form-control" ></textarea>
 <br>
              
                <input name="amount" type="hidden" id="modal-amt"  value="" class="form-control" />

 <div class="form-group"> 
        <label class="form-group">Bidding Amount: </label>
            <div class="control">
                <input type="text" class="form-control" required  value="" placeholder="Your Bidding Amount" name="bid_price">
            </div>
  </div>
 <div class="form-group">
         <label class="form-group">Ethincity: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-ethincity1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Gender: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-gender1">
            </div>
  </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target veterans?: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-target_veterans1">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target twins / triplets?: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-target_twins1">
            </div>
  </div>
 <div class="form-group">
        <label class="form-group">Briefly Describe Inclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-inclusion1"></textarea>
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Briefly Describe Exclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly readonly value="" id="modal-exclusion1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Additional Information: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly readonly value="" id="modal-add_info1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Title of the Study: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-title_study1">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Simple Description of Study: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly readonly value="" id="modal-desc_study1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Duration: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration1"><input  type="text" readonly value="" id="modal-duration_types1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Number of in-person visits required: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-inperson_visits1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Duration of Each Visit: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration_visits1">
                <input type="text" readonly value=""  id="modal-duration_time1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Compensation: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-total_compensation1">
                </div>
 </div>
 <div class="form-group">
         <label class="form-group">Travel Expenses Covered: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-travel_expense1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Zipcode: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-zipcode1"/>
            </div>
 </div>

                </div>

              <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Apply</button>
        </form>
      </div>
            </div>
          </div>
        </div>




<div class="modal fade" id="myModalfirst" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title m-no-top" id="modal-jname1"></h4>
              </div>
              <div class="modal-body">

<form class="icon" action="<?php echo base_url();?>home/wbid" method="post">
         <label class="form-group" for="from">Open Bidding Amount</label>
         <input class="form-control" type="text" readonly value="" id="modal-price" />
         <input name="job_id" type="hidden" id="modal-job1"  value="" class="form-control" />
                
   <br>
        
        <label class="form-group" for="from">Anything you wanna say</label>
        <textarea name="commands" required rows="7" cols="40" class="form-control" ></textarea>
 <br>
   
                <input name="amount" type="hidden" id="modal-amt"  value="" class="form-control" />
 <div class="form-group"> 
        <label class="form-group">Your Bidding Amount: </label>
            <div class="control">
                <input type="text" class="form-control" required  value="" placeholder="Your Bidding Amount" name="bid_price" >

            </div>
  </div>
 <div class="form-group">
         <label class="form-group">Ethincity: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-ethincity1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Gender: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-gender1">
            </div>
  </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target veterans?: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-target_veterans1">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Specifically target twins / triplets?: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-target_twins1">
            </div>
  </div>
 <div class="form-group">
        <label class="form-group">Briefly Describe Inclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7"  readonly value="" id="modal-inclusion1"></textarea>
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Briefly Describe Exclusion Criteria: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-exclusion1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Additional Information: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-add_info1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Title of the Study: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-title_study1">
                </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Simple Description of Study: </label>
            <div class="control">
                <textarea class="form-control" rows="7" readonly value="" id="modal-desc_study1"></textarea>
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Duration: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration1"><input  type="text" readonly value="" id="modal-duration_types1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Number of in-person visits required: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-inperson_visits1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Duration of Each Visit: </label>
            <div class="control">
                <input type="text" readonly value="" id="modal-duration_visits1">
                <input type="text" readonly value=""  id="modal-duration_time1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Total Compensation: </label>
                <div class="control">
                    <input class="form-control" type="text" readonly value="" id="modal-total_compensation1">
                </div>
 </div>
 <div class="form-group">
         <label class="form-group">Travel Expenses Covered: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-travel_expense1">
            </div>
 </div>
 <div class="form-group"> 
        <label class="form-group">Zipcode: </label>
            <div class="control">
                <input class="form-control" type="text" readonly value="" id="modal-zipcode1"/>
            </div>
 </div>
                </div>

              <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Apply</button>
        </form>

</div>
            </div>
          </div>
        </div>



    <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript">
          
var map;
      function initialize(lat,longi) {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
          center: {lat: lat, lng: longi},
          zoom: 17
        });
        var latlng = new google.maps.LatLng(lat,longi);
        var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: "Hello World!"
                });
      }
        </script>
         <script type="text/javascript">
          
var map;
      function initialize1(lat,longi) {
        map = new google.maps.Map(document.getElementById('map_canvas1'), {
          center: {lat: lat, lng: longi},
          zoom: 17
        });
        var latlng = new google.maps.LatLng(lat,longi);
        var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: "Hello World!"
                });
      }
        </script>
         <script type="text/javascript">
          
var map;
      function initialized(lat,longi) {
        map = new google.maps.Map(document.getElementById('map_canvasd'), {
          center: {lat: lat, lng: longi},
          zoom: 17
        });
        var latlng = new google.maps.LatLng(lat,longi);
        var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: "Hello World!"
                });
      }
        </script>
    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>