<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<style>
.pagination > li:first-child > a, .pagination > li:first-child > a:hover, .pagination > li:last-child > a,  .pagination > li:last-child > a:hover{
    color: #fff;
    background: #fff;
}
.pagination > li:first-child > a > i, .pagination > li:last-child > a > i{
    color: #3c948b;
    font-size: 20px;
    line-height: 30px;
    position: relative;
    top: 2px;
    left: 3px;
}
.pagination > li:last-child > a > i{
    left: 5px;
 }   
</style>

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
             if(typeof result['message_tasker'] ==='undefined')
             {
                
             }
              else if(result['message_tasker']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat/'+result['tasker']+'/<?php echo $user_id; ?>">New Message</a>'); // here the wrapper is main div
             
             } }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">



    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">


    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

               <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs">Trials</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->


                            

    
<div class="bg-grey-1">
    <section class="container section-lg">


        <!-- Nav tabs -->
        <ul class="inline-tabs">
            <li class="active">
                <a href="#acc" data-toggle="tab">Open Applications</a>
            </li>
            <li class="">
                <a href="#pwd" data-toggle="tab">Assigned Trials</a>
            </li>
            <li class="">
                <a href="#binfo" data-toggle="tab">Applications</a>
            </li>
            <li class="">
                <a href="#red" data-toggle="tab">Confirmed Trials</a>
            </li>
            <li class="">
                <a href="#past" data-toggle="tab">Past Participants</a>
            </li>
           <!-- <li class="">
                <a href="#fav" data-toggle="tab">Favorite Tasker</a>
            </li> -->
            
           
        </ul>
        <!-- Tab panes -->
        <?php 
               $user_id = $this->session->userdata('user_id');
               $profile = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = '$user_id'")->row();
$profile_user = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row();

        
?>
        <div class="tab-content">
            <div class="tab-pane active" id="acc">
                <?php $this->db->order_by("job_id", "desc");
        $this->db->where('jwork_status !=', '5');
        $this->db->where('jwork_status !=', '6'); 
        $recent_jobs   = $this->db->get_where('jobs', array('jbidding_type' => 1,'jposter_id' => $user_id))->result_array();
        ?>
                <div class="panel panel-default pad-m text-center">
                    
                    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                             <thead >
                                   <tr>
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Posted date</div></th>
                                        <th><div>Application Start - End date</div></th>
                                        <th><div>Status</div></th>
                                        <!--
<th><div>Date</div></th>
                                        <th><div>Amount</div></th>
-->
                                       
                                        <th><div>Participant Username</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php  foreach($recent_jobs as $jobs):?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jname;?></td>
                                        
                                        <td><?php echo $jobs['jposted_date'];?></td>
                                        <td><?php echo $jobs['jbidding_st_date'].' to '.$jobs['jbidding_ed_date'];?></td>
                                        <?php switch($jobs['jwork_status']) {
                                             case '1':
                                             echo "<td>New</td><td>-</td><td><a href='". base_url()."home/pjob_bids/".$jobs['job_id']."/".$user_id."'>View all Bids</a> - <a href='".base_url()."home/pjob_cancel/".$jobs['job_id']."' onclick=\"return confirm('Are you sure?');\">Cancel</a></td>";
                                             break;
                                             case '2':
                                             echo "<td>Assigned</td><td>"
                                             /*
.$jobs['jassign_date']."</td><td>"
                                             .$jobs['jassign_amt']."</td><td>"
*/
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jassign_to_work_id'])->row()->full_name)."</td><td>-</td>";
                                             break; 
                                             case '3':
                                             echo "<td>Accepted</td><td>"
                                             /*
.$jobs['jaccept_date']."</td><td>"
                                             .$jobs['jaccept_amt']."</td>"
*/
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name)."</td><td><a href='#' class='btn btn-primary btn-sm' data-toggle='modal' data-target='#confirmation' onclick='confirmation(".$jobs['job_id'].",".$jobs['jassign_amt'].")'>Confirm</a></td>";
                                             break;                                      
                                            case '4':
                                             echo "<td>Confirmed</td><td>"
                                             /*
.$jobs['jconformed_date']."</td><td>"
                                             .$jobs['jaccept_amt']."</td>"
*/
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name)."</td><td><a href='".base_url()."home/pjob_completed/".$jobs['job_id']."'>Complete</a></td>";
                                             break; 
                                             
                                             case '8':
                                             echo "<td><span style=\"color:Red\">Rejected</span> </td><td>-</td><td><a href='". base_url()."home/reassign/".$jobs['job_id']."'>Reassign</a></td>";
                                             break;
                                        }?>
                                    </tr>
                                    
                                    <!-- <div style="display: none;" id="job_id_<?php echo $jobs['job_id'];?>"><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jdescription;?></div>-->
                                    <?php endforeach; ?>
                                </tbody>
                                </table>
                </div>

            </div>

            <div class="tab-pane " id="pwd">
            
                <div class="panel panel-default pad-m">
                     
                     <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                         <thead >
                                   <tr>

                                          <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Status</div></th>
                                        <th><div>Date</div></th>
                                        <th><div>Participant Username</div></th>
                                        <th><div>Amount</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php 
                                   $user_id =  $this->session->userdata('user_id');
                                   $recent_jobs = $this->db->get_where('job_bids',array('job_status' => '3'))->result_array(); 
                                foreach($recent_jobs as $job):
                                  $recent_job = $this->db->get_where('jobs',array('job_id' => $job['job_id']))->result_array(); foreach($recent_job as $jobs):?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo $jobs['job_id'];?></td>
                                        <td><a href="<?php echo base_url();?>home/pjob_view/<?php echo $jobs['job_id']; ?>"><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jname;?></a></td>
                                        <?php 

                                                switch ($job['job_status']) {
                                                    case '1':
                                                        echo "<td>New</td>";
                                                        echo "<td>".$jobs['jposted_date']."</td>";
                                                        break;
                                                    case '2':
                                                        echo "<td>Assigned</td>";
                                                         echo "<td>".$jobs['jassign_date']."</td>";
                                                        break;
                                                    case '3':
                                                        echo "<td>Accepted</td>";
                                                         echo "<td>".$jobs['jaccept_date']."</td>";
                                                        break;
                                                    case '4':
                                                        echo "<td>Confirmed</td>";
                                                           echo "<td>".$jobs['jconfirmed_date']."</td>";
                                                        break;
                                                    case '5':
                                                        echo "<td>Cancelled</td>";
                                                         echo "<td></td>";
                                                        break;
                                                    case '6':
                                                        echo "<td>Completed</td>";
                                                         echo "<td>".$jobs['jcomplete_date']."</td>";
                                                        break;
                                                    case '7':
                                                        echo "<td>Incomplete</td>";
                                                         echo "<td>".$jobs['jcomplete_date']."</td>";
                                                        break;
                                                    case '8':
                                                        echo "<td>Rejected</td>";
                                                         echo "<td>-</td>";
                                                        break;
                                                    
                                                }

                                        ?>
                                     
                                         <td>
                                         <?php
                                         if(!empty($jobs['jaccept_workid'])){
                                            $output = $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name;
                                            echo ucfirst($output);
                                         } else{
                                            echo "-";  
                                         }
                            
                                         ?>
                                     </td>
                                     <td><?php echo $jobs['jassign_amt'] ?></td>
                                     <td>
                                        <?php 
                                                $job_status = $job['job_status'];   
                                                switch ( $job_status) {
                                                    case '1':
                                                       echo "<a href='".base_url()."home/pjob_cancel/".$jobs['job_id']."' onclick=\"return confirm('Are you sure?');\">Cancel</a>";
                                                        break;
                                                    case '2':
                                                        echo "-";
                                                       break;
                                                    case '3':
                                                        echo "<a href='#' class='btn btn-primary btn-sm' data-toggle='modal' data-target='#confirmation' onclick='confirmation(".$jobs['job_id'].",".$jobs['jassign_amt'].",".$jobs['jaccept_workid'].")'>Confirm</a>";
                                                       break;
                                                    case '4':
                                                        echo "<a href='".base_url()."home/pjob_completed/".$jobs['job_id']."'>Complete</a>";
                                                       break;
                                                    case '5':
                                                        echo "-";
                                                       break;
                                                    case '6':
                                                         echo "-";
                                                        break;
                                                    case '7':
                                                        echo "-";
                                                        break;
                                                    case '8':
                                                        echo "<a href='".base_url()."home/pjob_cancel/".$jobs['job_id']."' onclick=\"return confirm('Are you sure?');\">Cancel</a> / <a href='".base_url()."home/pjob_reassign/sam/".$jobs['job_id']."'>Reassign</a>";
                                                        break;
                                                    
                                                }

                                        ?>
                                     </td>
                                    </tr>
                                    
                                    <!-- <div style="display: none;" id="job_id_<?php echo $jobs['job_id'];?>"><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jdescription;?></div>-->
                                    <?php endforeach;endforeach; ?>
                                </tbody>
            </table>
          
             <?php  foreach($recent_jobs as $jobs):?>
                <div style="display:none;" id="job_id_<?php echo $jobs['job_id'];?>" class="fancyboxContent">
                    <h5>Job Description</h5>
                    <form style="padding: 10px; text-align: center;" action="<?php echo base_url().'home/job_update';?>" method="post" >
                 
                            <input type="hidden" name="job_id" value="<?php echo $jobs['job_id'];?>"/>
                          <textarea rows="8" class="form-control" style="text-align: left;padding: 10px;border-radius: 0;" placeholder="Text input" name="jdescription"><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jdescription;?></textarea>
                          <input style="padding: 5px 10px; border-radius: 0; margin-top:10px; background: #ff7b54; color: #FFF;" class="btn" type="submit" value="Update" />
                    </form>
                </div>
             <?php endforeach; ?>
            </div>
            </div>



            <div class="tab-pane" id="binfo">
                
                <div class="panel panel-default pad-m">
                    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      
                    <thead >
                                   <tr>
                                         <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Posted date</div></th>
                                        <th><div>Status</div></th>
                                        <th><div>Date</div></th>
                                        <th><div>Amount</div></th>
                                        <th><div>Participant id</div></th>
                                        <th><div>Participant Username</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php 
                                   $user_id =  $this->session->userdata('user_id');
        $recent_jobs = $this->db->get_where('job_bids',array('job_status' => '4'))->result_array(); 
 
         foreach($recent_jobs as $job):
                                  $recent_job = $this->db->get_where('jobs',array('job_id' => $job['job_id']))->result_array(); foreach($recent_job as $jobs):?>
                                <tr>
                                        <td style="text-align: center;"><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $this->db->query("SELECT * FROM jobs WHERE job_id=".$jobs['job_id'])->row()->jname;?></td>
                                         <td><?php echo $jobs['jposted_date'];?></td>
                                         <?php switch($job['job_status']) {
                                             case '1':
                                             echo "<td>New</td><td>-</td><td>-</td><td>-</td><td>-</td><td><a href='".base_url()."home/pjob_cancel/".$jobs['job_id']."' onclick=\"return confirm('Are you sure?');\">Cancel</a></td>";
                                             break;
                                             case '2':
                                             echo "<td>Assigned</td><td>"
                                             .$jobs['jassign_date']."</td><td>"
                                             .$jobs['jassign_amt']."</td><td>"
                                             .$jobs['jassign_to_work_id']."</td><td>"
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jassign_to_work_id'])->row()->full_name)."</td><td>-</td>";
                                             break; 
                                             case '3':
                                             echo "<td>Accepted</td><td>"
                                             .$jobs['jaccept_date']."</td><td>"
                                             .$jobs['jaccept_amt']."</td><td>"
                                             .$jobs['jaccept_workid']."</td><td>"
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name)."</td><td><a href='#' class='btn btn-primary btn-sm' data-toggle='modal' data-target='#confirmation' onclick='confirmation(".$jobs['job_id'].",".$jobs['jassign_amt'].")'>Confirm</a></td>";
                                             break;                                      
                                            case '4':
                                             echo "<td>Confirmed</td><td>"
                                             .$jobs['jconformed_date']."</td><td>"
                                             .$jobs['jaccept_amt']."</td><td>"
                                             .$jobs['jaccept_workid']."</td><td>"
                                             .ucfirst( $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name)."</td><td><a href='".base_url()."home/pjob_completed/".$jobs['job_id']."'>Complete</a></td>";
                                             break; 
                                              case '8':
                                             echo "<td><span style=\"color:Red\">Rejected</span> </td><td>-</td><td>-</td><td>-</td><td>-</td><td><a href='". base_url()."home/reassign/".$jobs['job_id']."'>Reassign</a></td>";
                                             break;
                                        }?>
                                </tr>
                                    <?php endforeach; endforeach; ?>
                                </tbody>
                                </table>
                    

                </div>

            </div>
          <div class="tab-pane " id="red">
    <?php 
  $user_id =  $this->session->userdata('user_id');
$recent_jobs = $this->db->get_where('job_bids',array('job_status' => '5'))->result_array(); 
                                  
        ?>
        <div class="panel panel-default pad-m">
    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                                    <tr>
                                        <th><div>Posted Date</div></th>
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>confirm date</div></th>
                                        <th><div>Bidding Type</div></th>
                                        <th><div>Accept Amount</div></th>
                                        <th><div>No of</div></th>
                                        <th><div>Total Amount</div></th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                
                                 <?php 
                                 foreach($recent_jobs as $job):
                                  $recent_job = $this->db->get_where('jobs',array('job_id' => $job['job_id']))->result_array(); foreach($recent_job as $jobs):
                                  if(!empty($jobs['jaccept_amt'])){
                                 ?>
                                    
                                    <tr>
                                        <td><?php echo $jobs['jposted_date'];?></td>
                                        <td><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $jobs['jname'];?></td>
                                       <td><?php echo $jobs['jconformed_date'];?></td>
                                        <td>
                                       <?php 
                                    
                                        switch ($jobs['jbilling_by'])
                                        {
                                            case '1':
                                            $status ='Hour Based';
                                            break;
                                            case '2':
                                            $status ='Day Based';
                                            break;
                                            
                                        }
                                        echo $status;?>
                                     </td>
                                      <td><?php echo $jobs['jaccept_amt'];?></td>
                                       <td>
                                            <?php 
                                                switch ($jobs['jbilling_by'])
                                            {
                                                case '1':
                                                echo $jobs['jbilling_no_hours']." Hours";
                                                break;
                                                case '2':
                                                echo $jobs['jbilling_no_days']." Days";
                                                break;
                                                
                                            }                                      
                                            ?>
                                       </td>
                                        <td><?php echo $jobs['jaccept_amt'];?></td>
                                     
                                    </tr>
                                  <?php }
                                  endforeach;
                                  endforeach; ?>
                                </tbody>
 </table>
        </div>
        </div>
            <div class="tab-pane " id="past">
    <?php 
$recent_jobs = $this->db->get_where('jobs', array('jwork_status' => 6,'jposter_id' => $user_id))->result_array();     
    ?>
    <div class="panel panel-default pad-m">
    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    
  <thead >
                                    <tr>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Participant id</div></th>
                                        <th><div>Participant Username</div></th>
                                         <th><div>completed date</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 <?php  foreach($recent_jobs as $jobs):
                                 if(!empty($jobs['jaccept_workid'])){
                                ?>
                                    
                                    <tr>
                                        <td><?php echo $jobs['jname'];?></td>
                                        <td><?php echo $jobs['jaccept_workid'];?></td>
                                        <td>
                                         <?php
                                            $output = $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name;
                                            echo ucfirst($output);
                                         ?>
                                        </td>
                                        <td><?php echo $jobs['jcomplete_date'];?></td>
                                     
                                     <td>
                                        <a href="<?php echo base_url();?>home/pjobworkers/<?php echo $jobs['job_id'];?>">View</a> 
                                     </td>
                                    </tr>
                                  <?php 
                                 }
                                  endforeach; ?>
                              </tbody>
</table>
</div>
                              </div>

<div class="tab-pane " id="fav">
    <?php 
            $recent_jobs = $this->db->get_where('jobs', array('jposter_id' => $user_id,'jposter_fav'=>1,'jwork_status' => 6))->result_array();     
        ?>
        <div class="panel panel-default pad-m">
    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    
  <thead >
                                    <tr>
                                        <th><div>Job Name</div></th>
                                        <th><div>Worker id</div></th>
                                        <th><div>Worker Name</div></th>
                                         <th><div>completed date</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 <?php  foreach($recent_jobs as $jobs):
                                 if(!empty($jobs['jaccept_workid'])){
                                ?>
                                    
                                    <tr>
                                        <td><?php echo $jobs['jname'];?></td>
                                        <td><?php echo $jobs['jaccept_workid'];?></td>
                                        <td>
                                         <?php
                                            $output = $this->db->query("SELECT * FROM users WHERE user_id=".$jobs['jaccept_workid'])->row()->full_name;
                                            echo ucfirst($output);
                                         ?>
                                        </td>
                                        <td><?php echo $jobs['jcomplete_date'];?></td>
                                     
                                     <td>
                                        <a href="<?php echo base_url();?>home/pjobworkers/<?php echo $jobs['job_id'];?>">View</a> 
                                     </td>
                                    </tr>
                                  <?php 
                                 }
                                  endforeach; ?>
                              </tbody>
</table>
</div>
            </div>
           
        </section>

</div>

    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>
<script>
                        function confirmation(id,price,poster){
              $(document).ready(function() {

    $.ajax({
        type : 'GET',
        async: true,
        success : function (result) {
            console.log(result);
            $('#modal-jid').val(id);
            $('#modal-poster').val(poster);
            $('#modal-jprice').val(price);
        }
    });
                        });
                    }
                    </script>
<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>



<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.responsive.min.js"></script>

<script src="<?php echo base_url();?>assets/js/responsive.bootstrap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {

 $('.dtbl').DataTable();
} );

$('#example').dataTable( {
  
} );

$(window).load(function() {

 $('.dtbl').DataTable();
} );

$(window).resize(function(){
     $('.dtbl').DataTable();
});


</script>

    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>




<!-- Modal -->
<div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/pjob_conform" method="post" enctype="multipart/form-data" class="no-icon" method="POST" id="braintree-payment-form">
            <input type="hidden" id='modal-jid' name="job_id" value="" />
            <input type="hidden" id='modal-poster' name="poster" value="" />
           <input type="hidden"  id='modal-jprice' name="amount" value="" />
           
      <div class="form-group">
        <label>Card Number</label>
        <input type="text" size="20" class="form-control" autocomplete="off" data-encrypted-name="number" />
      </div>
      <div class="form-group">
        <label>CVV</label>
        <input type="text" size="4" class="form-control" autocomplete="off" data-encrypted-name="cvv" />
      </div>
      <div class="form-group">
        <label>Expiration (MM/YYYY)</label>
        <input type="text" size="2" class="form-control" name="month" /> / <input class="form-control" type="text" size="4" name="year" />
      </div>
     
      </div>
      <div class="modal-footer">
        <input type="submit" name="submit" class="btn btn-primary" />
        </form>
          <script type="text/javascript" src="https://js.braintreegateway.com/v1/braintree.js"></script>
    <script type="text/javascript">
      var braintree = Braintree.create($bt_envi);
      braintree.onSubmitEncryptForm('braintree-payment-form');
    </script>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="deactivate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Deactivate My Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/confirm_deactivation/insert/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
                <div class="form-group">
                    <label>Reason for Deactivation</label>
                    <textarea id="change" class="form-control" required rows="10" name="reason"></textarea>
                </div>

               
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" onclick="return confirm('Are Sure to Deactivate')" class="btn btn-primary">Deactivate My Account</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
</body>


</html>