<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
  
   <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_content/<?php echo $user; ?>/<?php echo $tasker; ?>",
        type : 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
        	var ArrayContent = result.length;
        	
                                      $('#chat_content').html(result);
             }
                }
            );
        }
        </script>
        <script type="text/javascript">
        $(document).ready(function() {

    // process the form
    $('form').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'message'              : $('input[name=message]').val(),
            };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '<?php echo base_url();?>home/chatting_user/<?php echo $user; ?>/<?php echo $tasker; ?>', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {
$('#message').val('');
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});</script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!-- Chat -->
    <link href="<?php echo base_url(); ?>assets/chat/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/chat/css/font-awesome.min.css" rel="stylesheet">
	<!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/chat/css/style.css" rel="stylesheet">
    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <style>
    .form-lg .btn.search-btn{
        min-height: 44px;
    }
    .box {
    /*background-color:#00FF7F;*/
    height: 55px;
    
    word-wrap: break-word;    
}
    </style>
   
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs">Jobs</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 



            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

   
<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>
    

<div class="bg-grey-1">
    <div class="container">
				<div class="row">
					<div class="col-sm-12">
<div class="chat">
<div id="chat_content">

								</div>
								<hr>
									<div id="formbox" class="chat-textarea">
									<form id="form" class="clearfix" action="<?php echo base_url();?>home/chatting_user/<?php echo $user; ?>/<?php echo $tasker; ?>" method="post">
										<div class="form-group">
											<input type="text" name="message" id="message" class="form-control box" rows="3" placeholder="Enter Message" />
										</div>
										<button class="btn btn-default btn-sm pull-right" type="submit">Send</button>
 									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>





    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url(); ?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url(); ?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url(); ?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url(); ?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url(); ?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>
<script src="<?php echo base_url(); ?>assets/chat/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/chat/js/bootstrap.min.js"></script>
</body>


</html>