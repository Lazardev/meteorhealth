<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href=''><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href=""><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>
    

<div class="bg-grey-1">
    <section class="container section">

        <h2>Register to become a Participant in clinical trials and studies</h2>

        <p align="justify">Thank you for your interest in becoming a trial participant and supporting medical innovation! Please read the below carefully before registering. We are aware you might share personal, sensitive information with us and want to make sure you understand the fineprint.</p>

        <div class="space"></div>
        
        <div class="panel panel-default pad-m">
            <h3>Privacy</h3>
            <hr>
            <p align="justify">We are incredibly serious about privacy. We protect your personal and medical information. We will 

never sell, rent or lease your information.</p>
            <p align="justify">All initial data seen by providers is anonymized. We require your consent each time we release your 

medical and contact information to providers that you’re matched with. Once we release your 

information, Meteor Health is no longer responsible for how your information is handled. If you have 

questions or concerns about privacy after the information is released, contact the researchers or the 

Institutional Review Board overseeing the trial directly.</p>

            <p align="justify">Only information that you choose to provide will become part of your profile – you do not have disclose

all medical information.</p>
            <p align="justify">We will analyze aggregate, de-identified information from our website for ongoing quality evaluations

and development of our tool.</p>
            <p align="justify">Contact us immediately if you observe any unauthorized activity on your profile or if you believe your 

account may have been compromised.</p>
            <p align="justify">You may remove your personal information any time by deactivating your profile. However, this does 

not remove you from any researcher database for which you have authorized the release of your 

information through Meteor Health.</p>


            <h3>Disclaimer</h3>
            <hr>
            <p align="justify">Meteor Health does not provide any medical or healthcare products, services or advice. If you have any

questions about your medical condition, please contact a doctor.</p>
            <p align="justify">We do not guarantee that you will be matched with a clinical. We do not review and endorse any 

research, institution or study or recommend that you participate in specific studies you’ve matched 

with.</p>
            <p align="justify">Matching with a trial means that you have been pre-screened for the initial trial match criteria.

However, many studies have additional selection criteria. Thus, matching with a study does not 

guarantee your immediate participation in the trial and may require the completion of additional steps.</p>
            <h3>Agreement</h3>
            <hr>
            <p align="justify">By agreeing to this Participant Agreement, you consent to the use and disclosure of your personally 

identifiable information on your Meteor Health profile as outlined here. You also agree that Meteor 

Health may access, keep or share your registration information if required to do so by law or in the good 

faith belief that it is necessary to</p>
<ul>
<li>Enforce the terms of this Participant Agreement</li>
<li>Respond to your requests for customer service</li>
<li>Comply with federal, state or local legal process</li>
</ul>
            <p align="justify">By completing this registration process, you represent and warrant that you reside in the United States,

are at least 18 years of age, and that you possess the legal right and ability to enter into this Participant 

Agreement. You agree that if you register on Meteor Health, you will provide accurate information 

about yourself. You agree that if any information you provide is inaccurate, Meteor Health may prohibit 

your access to the platform and deactivate your profile.</p>
            <p align="justify">If you agree to the above, the profile will be added to the Meteor Health participant database as soon as 

you submit this form.</p>
            <p align="justify">Thank you for supporting medical innovation!</p>
<form action="<?php echo base_url();?>home/dash_worker" method="post">
            <div class="checkbox">
              <label><input required type="checkbox" name="accept" value="yes">Agree</label>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </div>
        </form>
    </section>

</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>