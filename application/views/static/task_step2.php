<?php
//require 'facebook/facebook.php';

$categories = $this->db->query("SELECT * FROM `job_category` WHERE `category_id` = '$category_id'")->result_array();

?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
              if(typeof result['message_tasker'] ==='undefined')
             {
                
             }
              else if(result['message_tasker']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat/'+result['tasker']+'/<?php echo $user_id; ?>">New Message</a>'); // here the wrapper is main div
             
             } }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
     <script src="<?php echo base_url();?>template/js/nicEdit.js" type="text/javascript"></script>
     

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $("#direct").click(function() {
        // var test = $(this).val();

        $("div.assign").show();
        // $("#Cars" + test).show();
    });
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">
            <li id="chat"></li>
                 <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                 <li><a href="<?php echo base_url();?>home/jobs_task">Jobs</a></li>
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>

    <ul class="filter-menu bg-grey-2 m-no-bottom">
        <li class="active"><i class="fa fa-pencil"></i> 1. Fill Out Trial Details</li>
        <li class="active"><i class="fa fa-smile-o"></i> 2. Participant Selection</li>
        <li><i class="fa fa-check"></i> 3. Confirm & Book</li>
       
    </ul>
<?php foreach ($categories as $category) {
   ?>
    <div class="bg-grey-2">
   
        <section class="container section">
         <div class="text-center task-top">
                <img src="<?php echo base_url();?>uploads/categories/<?php echo $category['category_image']; ?>">
                <h3 class="text-center"><strong><?php echo $category['category_name']; ?></strong></h3>
                <?php }?>
                </div>
        <div class="panel panel-default pad-m">
        <div class="text-center task-top">
                    <div class="space"></div>
                    <form class="form-inline icon" method="post" action="<?php echo base_url();?>home/task_step3/<?php echo $category_id; ?>/<?php echo $zip;?>" role="form">
                        <div class="form-group">
                            <div class="control">
                <h4>How do you want to select your users?</h4>
                <div class="priceName">
                    <div id="quickAssign" class="radio_button">
                        <input id="openBid" name="jbidding_type" type="radio" value="1" />
                        <b>Open Application



Select patients from users who apply to your trial</b>
                            <p>Bidding open to all Meteor on a period of time.</p>   
                        
                    </div>

                   <!-- <div class="radio_button ">
                        <input id="forBids" name="jbidding_type" type="radio" value="3">
                        <b>Who bid first.</b>
                            <p>Assign to the Meteor who bids first.</p>
                        <label for="forBids">
                            
                        </label>
                    </div>
                        <div class="radio_button ">
                        <input id="direct" name="jbidding_type" type="radio" value="2">
                        <b>Assign to Meteor</b>
                            <p>Search for an individual Meteor and assign the job.</p>
                         
                    </div> -->
                </div>
                </div>
                 <button type="submit" class="btn btn-primary">Next</button>
                </div>
                </form></div>
                </div>

                </section>
                </div>





                <!-- Direct Assign -->
                









    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>
<style> 
.none { display:none; }, 
.showDIV { display:block; } 
</style>


</body>


</html>