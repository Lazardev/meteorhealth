<?php
session_start();

?>
<?php
//require 'facebook/facebook.php';

$categories = $this->db->query("SELECT * FROM `job_category` WHERE `category_id` = '$category_id'")->result_array();

?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<style type="text/css">
#map {
        height: 100%;
      }
      .controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}


.pac-container {
  font-family: Roboto;
}

#type-selector {
  color: #fff;
  background-color: #4d90fe;
  padding: 5px 11px 0px 11px;
}

#type-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;

}
    </style>
    
    <style>
      #target {
        width: 345px;
      }
      </style>
<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        /*setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
             if(typeof result['message_tasker'] ==='undefined')
             {
                
             }
              else if(result['message_tasker']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat/'+result['tasker']+'/<?php echo $user_id; ?>">New Message</a>'); // here the wrapper is main div
             
             } }
                }
            );
        }*/
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">
                <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                 <li><a href="<?php echo base_url();?>home/jobs">Jobs</a></li>
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 
</ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>

    <ul class="filter-menu bg-grey-2 m-no-bottom">
        <li class="active"><i class="fa fa-pencil"></i> 1. Fill Out Trial Details</li>
        <li><i class="fa fa-smile-o"></i> 2. Participant Selection</li>
        <li><i class="fa fa-check"></i> 3. Confirm & Book</li>
       
    </ul>
<?php foreach ($categories as $category) {
   ?>
    <div class="bg-grey-2">
        <section class="container section">
            <div class="text-center task-top">
                <img src="<?php echo base_url();?>uploads/categories/<?php echo $category['category_image']; ?>">
                <h3 class="text-center"><strong><?php echo $category['category_name']; ?></strong></h3>
                
            </div>
        </section>
  
<?php }?>

    <section class="container section margin-b30">


        <div class="panel panel-default pad-m">
            <h5><strong>TARGET DEMOGRAPHICS</strong></h5>

            <form class="icon" action="<?php echo base_url();?>home/task_step2/<?php echo $category_id; ?>" method="post">
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Gender</label>
                            <div class="control">
                        <input type="checkbox" name="gender" value="male"> Male
                        <input type="checkbox" name="gender" value="female"> Female
                        </div>
                </div>
               <div class="form-group">
                     <label class="control-label col-md-4" for="street">Ethincity</label>
                         <div class="control">
                   <select name="ethincity" required class="form-control">
                        <option value="Native American">Native American</option>
                        <option value="Asian">Asian</option>
                        <option value="Black or African American">Black or African American</option>
                        <option value="Native Hawaiian or other Pacific Islander">Native Hawaiian or other Pacific Islander</option>    
                        <option value="Caucasian">Caucasian</option>
                        <option value="Latino or Hispanic">Latino or Hispanic</option>
                        <option value="Multiracial">Multiracial</option>
                        <option value="Other">Other</option>    
                    </select>
                </div>
                 </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="street">Specifically target veterans?</label>
                            <div class="control">
                        <select name="target_veterans" required class="form-control">
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>    
                    </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" for="street">Specifically target twins / triplets?</label>
                            <div class="control">
                        <select name="target_twins" required class="form-control">
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>    
                    </select> 
                   </div>
                        </div>
                        
        </div>
        <div class="panel panel-default pad-m">
            <h5><strong>TARGET DEMOGRAPHICS</strong></h5>

            <form class="icon" action="<?php echo base_url();?>home/task_step2/<?php echo $category_id; ?>" method="post">
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Briefly Describe Inclusion Criteria</label>
                            <div class="control">
                       <textarea class="form-control" name="inclusion"  placeholder="Briefly Describe Inclusion Criteria" ></textarea>
                        </div>
                </div>
               <div class="form-group">
                     <label class="control-label col-md-4" for="street">Briefly Describe Exclusion Criteria</label>
                            <div class="control">
                       <textarea class="form-control"  name="exclusion" value="" placeholder="Briefly Describe Exclusion Criteria" /></textarea>
                        </div>
                </div>
                       <div class="form-group">
                     <label class="control-label col-md-4" for="street">Additional Information</label>
                            <div class="control">
                       <input class="form-control" type="text" name="add_info" value="" placeholder="Additional Information" />
                        </div>
                </div>
                        
        </div>
                <div class="panel panel-default pad-m">
            <h5><strong>INFORMATION FOR PARTICIPANTS</strong></h5>

            <form class="icon" action="<?php echo base_url();?>home/task_step2/<?php echo $category_id; ?>" method="post">
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Title of the Study</label>
                            <div class="control">
                       <input class="form-control" type="text" name="title_study" value="" placeholder="Title of the Study" />
                        </div>
                </div>
               <div class="form-group">
                     <label class="control-label col-md-4" for="street">Simple Description of Study</label>
                            <div class="control">
                       <textarea class="form-control"  name="desc_study" value="" placeholder="Simple Description of Study"></textarea>
                        </div>
                </div>
                       <div class="form-group">
                     <label class="control-label col-md-4" for="street">Total Duration</label>
                            <div class="control">
                       <input class="col-md-8" type="number" name="duration" value="" placeholder="Total Duration" />
                       <select name="duration_types" required class="col-md-4">
                        <option value="Days">Days</option>
                        <option value="Weeks">Weeks</option>   
                        <option value="Months">Months</option>    
                    </select>
              </div>
                </div>
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Number of in-person visits required</label>
                            <div class="control">
                       <input class="form-control" type="number" name="inperson_visits" value="" placeholder="Number of in-person visits required" />
              </div>
                </div>
                 <div class="form-group">
                     <label class="control-label col-md-4" for="street">Duration of Each Visit</label>
                            <div class="control">
                       <input class="col-md-8" type="number" name="duration_visits" value="" placeholder="Duration of Each Visit" />
                       <select class="col-md-4" name="duration_time" required>
                        <option value="Minutes">Minutes</option>
                        <option value="Hours">Hours</option>   
                        <option value="Days">Days</option>    
                    </select>
              </div>
                </div>
              <div class="form-group">
                     <label class="control-label col-md-4" for="street">Total Compensation</label>
                            <div class="control">
                       <input class="form-control" type="number" name="total_compensation" value="" placeholder="Total Compensation" />
              </div>
                </div>
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Travel Expenses Covered</label>
                            <div class="control">
                       <select class="col-md-4" name="travel_expense" required>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>   
                        </select>
              </div>
                </div>
                <div class="form-group">
                     <label class="control-label col-md-4" for="street">Zipcode</label>
                            <div class="control">
                       <input class="form-control" type="text" name="zipcode" value="" placeholder="Zipcode" />
              </div>
                </div>
<input type="text" name="cat_id" hidden="true" value="<?php echo $category_id; ?>"> <center>
  <button type="submit" class="btn btn-primary">Next</button> </center>
            </form>
           </div> 
        </div>
         
    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


  
       <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?> <?php if($this->lang->line('allrights') != '') { echo stripslashes($this->lang->line('allrights')); }  ?></p>
        </div>
    </div>
</footer>
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'long_name',
  country: 'long_name', 
  postal_code: 'short_name'
};
var componentForm1 = {
  street_numbern: 'short_name',
  routen: 'long_name',
  localityn: 'long_name',
  administrative_area_level_1n: 'long_name',
  countryn: 'long_name', 
  postal_coden: 'short_name'
};
$(document).ready(function(){

    


  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
   
    fillInAddress();
  });

  autocomplete1 = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('autocomplete1')),
      { types: ['geocode'] });
  // When the user selects an address from the dropdown,
  // populate the address fields in the form.
  google.maps.event.addListener(autocomplete1, 'place_changed', function() {
    //alert("fjhgyu");
    console.log("Ddd");
    fillInAddress1();
  });

  var mapOptions = {
    zoom: 6
  };
  var mapOptions1 = {
    zoom: 6
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  map1 = new google.maps.Map(document.getElementById('map-canvas1'),
      mapOptions1);


  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);


      map.setCenter(pos);
      map1.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });
  }

});



// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

var newLatLng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());

  var marker = new google.maps.Marker({
      position: newLatLng,
      map: map,
      draggable: true,
      title: 'Hello World!'
  });

  $('#latitude').val(marker.getPosition().lat());
  $('#longitude').val(marker.getPosition().lng());

  google.maps.event.addListener(marker, 'dragend', function(ev){
    $('#latitude').val(marker.getPosition().lat());
    $('#longitude').val(marker.getPosition().lng());
});

  map.setCenter(newLatLng);

  map.setZoom(18);

//console.log(place.geometry.location.toString());
console.log(componentForm);

  for (var component in componentForm) {
//    document.getElementById('component').value = '';
 //   document.getElementById('component').disabled = true;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
   
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}

   
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.






// [START region_fillform]
function fillInAddress1() {
  // Get the place details from the autocomplete object.
  var place1 = autocomplete1.getPlace();

console.log(place1.geometry.location.lat()+"xxbbbbbxxx");


var newLatLng1 = new google.maps.LatLng(12.9100,77.6400);
//console.log(newLatLng1+"hhhhhhhhhhhhhhhhhhhhhhh");
  var marker1 = new google.maps.Marker({
      position: newLatLng1,
      map: map1,
      draggable: true,
      title: 'Hello World!'
  });
  console.log(marker1.getPosition().lng()+"nikita bbbnbas");

  $('#dlatitude').val(marker1.getPosition().lat());
  $('#dlongitude').val(marker1.getPosition().lng());

  google.maps.event.addListener(marker1, 'dragend', function(ev){
    $('#dlatitude').val(marker1.getPosition().lat());
    $('#dlongitude').val(marker1.getPosition().lng());
});

  map1.setCenter(newLatLng1);

  map1.setZoom(18);

//console.log(place.geometry.location.toString());
//console.log(componentForm1+"checking jain");

  for (var component in componentForm1) {
//    document.getElementById('component').value = '';
 //   document.getElementById('component').disabled = true;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  console.log(place1.address_components.length+"check by niki");
  for (var i = 0; i < place1.address_components.length; i++) {
    var addressType1 = place1.address_components[i].types[0]+"n";
    console.log(addressType1+"checkingg by");

    if (componentForm1[addressType1]) {
    
      var val1 = place1.address_components[i][componentForm1[addressType1]];
      //console.log(val1+"nikita jain")
      document.getElementById(addressType1).value = val1;
    }
    else{
        console.log("not coming");
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate1() {
   
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      autocomplete1.setBounds(new google.maps.LatLngBounds(geolocation,
          geolocation));
    });
  }
}

    </script>


</body>


</html>