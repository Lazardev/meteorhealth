<?php
//require 'facebook/facebook.php';

$categories = $this->db->query("SELECT * FROM `job_category` WHERE `category_job_type` = '$cat_id'")->result_array();

?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-trn navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">
<li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs">Trials</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 



            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->

    <section class="bg-cover bg-center" style="background-image:url(<?php echo base_url();?>assets/img/<?php echo $job_img;?>);">
        <div class="bg-filter">
            <div class="container section-lg">
                <!-- <h1 class="top-title">Welcome!</h1> -->
                <div class="sub-text text-center">
                    <h2>Post a New Trial</h2>
                    <p>Let us help you find participants for your trial</p>
                    
                </div>
            </div>
        </div>
    </section>

    <div class="bg-grey-2">
        <section class="container section s-bar">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <?php 
          foreach($categories as $category){?>
                   
                    <div class="panel row task-list m-no-all">
                         <div class="col-md-4 col-sm-6 col-xs-12 r-p-no-left img-bx">
                            <img src="<?php echo base_url();?>uploads/categories/<?php echo $category['category_image']?>">
                        </div> 
                        <div class="col-md-8 col-sm-6 col-xs-12 r-p-no-left btm" >
                            <h3 class="m-no-top" style="color:#3c948b;"><strong><?php echo $category['category_name'];?></strong></h3>
                            <p><?php echo $category['category_description']?></p>
          
                                <a style="margin-bottom:15px;" href="<?php echo base_url();?>home/task_step1/<?php echo $category['category_id'] . "/". $cat_id ;?>" class="btn btn-primary btn-sm">Post your Trials</a>
                        </div>
                    </div>
                <?php }  ?>
                </div>
                <div class="col-md-4 col-sm-12">

                    <h3 class="m-no-top"><strong>Post your Trial</strong></h3>
                    <div class="section-xsm"></div>
                    <div class="float-box">
                        <i class="fa fa-tasks blue-i sm"></i>
                        <div class="float-text">
                            <h4 style="color:#3c948b;"><strong>Select your Task</strong></h4>
                            <p>Describe your recruitment criteria and expectations from participants</p>
                        </div>
                    </div>

                    <div class="float-box">
                        <i class="fa fa-tint blue-i sm"></i>
                        <div class="float-text">
                            <h4 style="color:#3c948b;"><strong>Sign up participants</strong></h4>
                            <p>Schedule appointments with screened participants </p>
                        </div>
                    </div>

                    <div class="float-box">
                        <i class="fa fa-check-square-o blue-i sm"></i>
                        <div class="float-text">
                            <h4 style="color:#3c948b;"><strong>Manage trials</strong></h4>
                            <p>Manage ongoing trials and participants</p>
                        </div>
                    </div>

<div class="float-box">
                        <i class="fa fa-tint blue-i sm"></i>
                        <div class="float-text">
                            <h4 style="color:#3c948b;"><strong>Pay as you go</strong></h4>
                            <p>Process participant reimbursements as milestones are met.</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>






    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>