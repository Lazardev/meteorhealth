<?php
require 'facebook/facebook.php';
$appId = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_apikey'")->row()->description;
$secret = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_applicationkey'")->row()->description;
$facebook_profile = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_profile'")->row()->description;
$facebook = new Facebook(array(
  'appId'  => $appId,
  'secret' => $secret,
));
$user = $facebook->getUser();
if ($user) {
  try {
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}
 
if(isset($user)) {
	$loginUrl = $facebook->getLoginUrl(array(
		'scope'		=> 'user_about_me, user_hometown, email, user_birthday',
		'redirect_uri'	=> $facebook_profile,
	));
}
?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

   <style>
     @media(max-width: 450px){
      .bg-cover{
        background: url('http://meteorhealth.com/assets/img/meteor.png') no-repeat !important;
        background-size: cover !important;
      }
    }
   </style>
</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
            </div>

            <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                    
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>

    <section class="bg-center bg-cover  section-fxd" id="block" style="width: 100%; height: 100%;" data-vide-bg="<?php echo base_url();?>assets/img/MeteorHealt" data-vide-options="position: 0% 50%">
        <div class="bg-filter">
            <div class="hv-center">
                <div class="sign-up">
                    <div class="sign-up-hr hidden-xs"></div>
                    <h1 class="sign-up-title"><?php echo $sign;?> Sign In</h1>
                    <div class="row">
                                       <div class="col-md-6 col-sm-6 icon">
                           <?php echo form_open(base_url().'home/do_login' , array('class' => 'form-signin'));?>
                                <div class="form-group " role="form" >
                                    <div class="control">
                                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="control">
                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                        <i class="fa fa-lock"></i>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success btn-block">
                                    <i class="fa fa-check"></i>Login to your account</button>
                                <div class="space visible-sm visible-xs"></div> <br>
                               <center><a href="#" data-toggle="modal" data-target="#edit-acc">Forgot your Password?</a></center>
                            
                            </form>
                        </div>
                         <div class="col-md-6 col-sm-6">
                        <?php 
				$facebook_status	=	$this->db->get_where('sitesettings' , array('type'=>'facebook_status'))->row()->description;
				if($facebook_status !=2){ ?>
				 <a href="<?php echo $loginUrl; ?>" title=" Sign in with Facebook">
                                <div class="sign-up-btn facebook">
                                    <div class="icon">
                                        <i class="fa fa-facebook"></i>
                                    </div>
                                    <div class="text">
                                        Sign in with Facebook
                                    </div>
                                </div>
                            </a>
				<?php 
				}
				?>
                        
                            <a href="<?php echo base_url();?>home/signup">
                            <button type="submit" class="btn btn-info btn-block">
                                    <i class="fa fa-sign-in"></i>Signup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.vide.min.js"></script>
<script>
  $(document).ready(function () {
    $('#block').vide('<?php echo base_url();?>assets/img/MeteorHealt'); // Non declarative initialization
//
//    var instance = $('#block').data('vide'); // Get the instance
//    var video = instance.getVideoObject(); // Get the video object
//    instance.destroy(); // Destroy instance
//
//    $('#block2').vide({
//      'mp4': 'video/ocean',
//      'webm': 'video/ocean',
//      'ogv': 'video/ocean',
//      'poster': 'video/ocean',
//    });
 });
</script>


</body>


</html>

<div class="modal fade" id="edit-acc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Forgot Your Password</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/forgot_password" method="post" enctype="multipart/form-data" class="no-icon">
               
                
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $profile_user->emailid;?>" placeholder="Email">
                </div>
                
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
         </form>
      </div>
    </div>
  </div>
</div>




