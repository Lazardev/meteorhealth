<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
	<!-- Meta Tags -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">


	<!--  Boostrap Framework  -->
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

	<!--=== CSS ===-->
	<link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

	<!-- Google Fonts - Lato -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

	<!-- Font Awesome Icons -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- CSS Animations -->
	<link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

	<!--  Slippry Slideshow -->
	<link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-trn navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/login/participant'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/login/participant"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li><a href="<?php echo base_url();?>home/become_jobrabbit">Become a Meteor</a></li> 
                <li><a href="<?php echo base_url();?>home/how_it_works">How it works</a></li>
                <li><a href="<?php echo base_url();?>home/login">Login</a></li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!--  Header Section  -->
    <section class="bg-become bg-centerTop70 bg-cover">
        <div class="bg-filter sTop">
            <div class="container">
                <div class="jumbotron trn v-center">
                    <h1>Become a Meteor</h1>
                    <div class="space-sm"></div>
                    <p>From running quick deliveries to working longer-term office assignments, get access to thousands of jobs.</p>
                    <div class="space-sm"></div>
                    <ul class="list-inline">
                        <li>
                            <a href="<?php echo base_url();?>home/signup" class="btn btn-primary btn-xlg scroll"><i class="fa fa-paper-plane-o"></i>Join Free</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>home/login" class="btn btn-default-trn btn-xlg scroll"><i class="fa fa-sign-in"></i>Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="container section margin-b30">

    <h2 class="lg-title lg-title-border">How to get started</h2>
            <div class="space"></div>
            <div class="space"></div>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="float-box">
                    <i class="fa icon-c fa-pencil"></i>
                    <div class="float-text">
                        <h3>Create an account</h3>
                        <p>Log in with Facebook or sign up with your email address.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="float-box">
                    <i class="fa icon-c fa-check"></i>
                    <div class="float-text">
                        <h3>Get background checked</h3>
                        <p>We're big on safety, so we require background checks.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 ">
                <div class="float-box">
                    <i class="fa icon-c fa-search"></i>
                    <div class="float-text">
                        <h3>Start bidding on jobs</h3>
                        <p>Browse our open jobs, find jobs that interest you, and make an offer.</p>
                    </div>
                </div>
            </div>

          
        </div>
    </section>


    


    <section class="section-lg bg-grey">
        <div class="container">
        <h2 class="lg-title lg-title-border">Now Available in</h2>
            <div class="space"></div>
          
            <div class="row">
                <!--fadeInLeft-->
                <ul class="filter-tabs">
                     <li class="active">Atlanta</li>
                     <li class="active">Dallas</li>
                     <li class="active">Houston</li>
                     <li class="active" >Pliladelphia</li>
                     <li class="active" >Washington DC</li>
                 </ul>

            </div>
        </div>
    </section>









  

    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


    <!-- Footer-->
    <footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <h4>Contact</h4>
                    <ul class="info-list md-list i-primary">
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a href="mailto:">info@MeteorHealth.com</a>
                        </li>
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            +21 902 920 889
                        </li>
                        <li>
                            <i class="fa fa-map-marker fa-fw"></i>
                            885 3rd Ave, NY 10022
                        </li>
                    </ul>
                </div>
                <div class="space-sm visible-sm visible-xs"></div>
                <div class="col-md-4 col-lg-4 col-xs-12">
                    <h4>Quick Links</h4>
                    <ul class="info-list md-list i-primary">
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>home/become_jobrabbit">
                                <span class="link-title">Become A Meteor</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>home/how_it_works">
                                <span class="link-title">How it works</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>home/login">
                                <span class="link-title">Login</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-fw"></i>
                            <a href="<?php echo base_url();?>home/signup">
                                <span class="link-title">Signup</a>
                        </li>
                        
                    </ul>
                </div>
                <div class="space-sm visible-sm visible-xs"></div>
                <div class="col-md-4 col-lg-4  col-xs-12">
                    <h4>About Us</h4>
                    <p>Temporibus autem quibusdam et aut officiis debitis ut et voluptates repudiandae sint et molestiae non recusandae.</p>
                    <div class="space-sm"></div>
                    <h4>Follow Us</h4>
                    <ul class="circle-icons icons-list">
                        <li>
                            <a href="<?php echo $twitter_link?>" target="_blank" title="Follow us">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                       <li>
                            <a href="<?php echo $facebook_link?>" target="_blank" title="Follow us">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2015 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>          