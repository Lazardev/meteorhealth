<?php session_start();?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name; ?></title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_task_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
              if(typeof result['chat']['message'] ==='undefined')
             {
                
             }
              else if(result['chat']['message']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat_task/<?php echo $user_id; ?>/'+result['chat']['user']+'">New Message</a>'); // here the wrapper is main div
             
             }
             
             $("#assign_not").html(result['jobs_assigned']);
             $("#open_not").html(result['jobs_open']);
             $("#first_not").html(result['jobs_first']);
              }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">



    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">


    <link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <style>
    .pagination .paginate_button a{
      color: transparent;
      text-align: center;
    }
    .pagination .paginate_button a:hover{
      color: transparent;
    }
    .pagination .paginate_button a > i{
      color: #3c948b;
      position: relative;
      top: 1px;
      font-size: 18px;
    }
    .fa-angle-left{
      left: 3px;
    }
    .fa-angle-right{
      left: 5px;
    }
    .pagination .paginate_button.active a{
      color: #fff;
    }
    </style>
</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_worker'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_worker"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_worker">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs_task">Trials</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->


                            

    
<div class="bg-grey-1">
    <section class="container section-lg">


        <!-- Nav tabs -->
        <ul class="inline-tabs">
            
            <li class="active">
                <a href="#pwd" data-toggle="tab">Assigned Trials for you</a>
            </li>
            <li class="">
                <a href="#red" data-toggle="tab">Confirmed Trials</a>
            </li>
             <li class="">
                <a href="#job" data-toggle="tab">Trial History</a>
            </li>
            <li class="">
                <a href="#bids" data-toggle="tab">My Applications</a>
            </li>
           
        </ul>
        <!-- Tab panes -->
        <?php 
               $user_id = $this->session->userdata('user_id');
               $profile = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = '$user_id'")->row();
$profile_user = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row();

        
?>
        <div class="tab-content">
        <div class="tab-pane " id="bids">
        <div class="panel panel-default pad-m">
                    <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      
                   <thead >
                                    <tr>
                                        <th><div>Participant ID</div></th>
                                        <th><div>Participant Name</div></th>
                                        <th><div>Bid Date</div></th>
                                        <th><div>Bid Amount</div></th>
                                        <th><div>Comments</div></th>
                                        <th><div>Options</div></th>
                                       </tr>
                                </thead>
                                <tbody>
                                 <?php
                                 $this->db->order_by('bid_amt desc'); 
                                 $bids=$this->db->get_where('job_bids', array('userid' => $user_id))->result_array();


                                   foreach($bids as $bid):?>
                                    <tr>
                                        <td ><?php echo $bid['userid'];?></td>
                                         <td>
                                         <?php
                                            $output = $this->db->query("SELECT * FROM users WHERE user_id=".$bid['userid'])->row()->full_name;
                                            echo ucfirst($output);
                                         ?>
                                        </td>
                                        <td><?php echo $bid['biding_dt'];?></td>
                                        <td><?php echo $bid['bid_amt'];?></td>
                                        <td><?php echo $bid['bid_commands'];?></td>
                                         <td>
                                         
                    <?php if($assign_id!= $bid['userid'] && $job_reject !='8' ){

                        $status = $this->db->query("SELECT * FROM users WHERE user_id =". $bid['userid'])->row();
                        ?>
                                      
                                            <a href="<?php echo base_url();?>home/pjob_view/<?php echo $bid['job_id']; ?>/task"> View Trials</td>
                                         
                                    </tr>
                                  <?php }else {
                                      //echo "<span style=\"color:Red\">Rejected</span>";
                                  } 
                                  endforeach; ?>
                                  <?php 
                                  if(!$bids){
                                        echo "<tr><tdcolspan='6'><center>No record found</center></td></tr>";
                                    }
                                    ?>
                                </tbody>
                                </table>
                    

                </div>

        </div>
                       <div class="tab-pane active" id="pwd">
            
                        <div class="panel panel-default pad-m text-center">
                               <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                     
                            <thead>
                                    <tr>
                                        <th><div>Posted Date</div></th>
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Assign date</div></th>
                                        <th><div>Billing By</div></th>
                                        <th><div>Avg Amt</div></th>
                                        <th><div>Bid Amt</div></th>                                        
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                 <?php 
                                 $user_id =  $this->session->userdata('user_id');
                                $recent_jobs = $this->db->get_where('job_bids',array('userid' => $user_id,'job_status' => '3'))->result_array(); 
                                  foreach($recent_jobs as $job):
                                  $recent_job = $this->db->get_where('jobs',array('job_id' => $job['job_id']))->result_array(); 
                                  foreach($recent_job as $jobs):
                                 ?>
                                    
                                    <tr>
                                        <td><?php echo $jobs['jposted_date'];?></td>
                                        <td><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $jobs['jname'];?></td>
                                        <td><?php echo $jobs['jassign_date'];?></td>
                                        <td>
                                       <?php 
                                    
                                        switch ($jobs['jbilling_by'])
                                        {
                                            case '1':
                                            $status =$jobs['jbilling_no_hours']." Hours";
                                            break;
                                            case '2':
                                            $status =$jobs['jbilling_no_days']." Days";
                                            break;
                                            
                                        }
                                        echo $status;?>
                                     </td>
                                      <td>
                                            <?php 
                                            switch ($jobs['jbidding_type'])
                                            {
                                                case '1':
                                                        switch ($jobs['jbilling_by'])
                                                    {
                                                case '1':
                                                echo $jobs['jbilling_no_hours'] * $jobs['jposter_price'] ;
                                                break;
                                                case '2':
                                                echo $jobs['jbilling_no_days'] * $jobs['jposter_price'] ;
                                                break;
                                                    }       
                                                break;
                                                case '2':
                                                echo 'Direct';
                                                break;
                                                case '3':
                                                echo 'First bid';
                                                break;
                                            }
                                            
                                                                               
                                            ?>
                                       </td>
                                        <td><?php echo $jobs['jassign_amt'];?></td>
                                     <td>
                                        <a href="<?php echo base_url();?>home/job_accept/<?php echo $jobs['job_id']; ?>/1/<?php echo $jobs['jassign_amt'];?>">Accept</a> /
                                        <a href="<?php echo base_url();?>home/job_accept/<?php echo $jobs['job_id']; ?>/2">Reject</a>
                                     </td>
                                    </tr>
                                  <?php 
                                  endforeach; endforeach; ?>
                                </tbody>
                                </table>
                    
                    </div>
            </div>



            <div class="tab-pane" id="red">
                
               <div class="panel panel-default pad-m text-center">
                               <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                     
                    <thead>
                                    <tr>
                                     
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Trial Completed Date</div></th>
                                        <th><div>Options</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                 <?php 
                                 $user_id =  $this->session->userdata('user_id');
        $pastjob = $this->db->get_where('jobs', array('jaccept_workid' => $user_id,'jcomplete_date !='=>'0000-00-00'))->result_array();                
        
         foreach($pastjob as $past):?>
                                    <tr>
                                        <td><?php echo $past['job_id'];?></td>
                                        <td><?php echo $past['jname'];?></td>
                                          <td><?php echo $past['jcomplete_date'];?></td>
                                     <td>
                                        <a href="<?php echo base_url();?>home/wjobworkers/<?php echo $past['job_id']; ?>">View</a>
                                     </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                                </table>
                </div>

            </div>

                                   <div class="tab-pane " id="job">
            
                        <div class="panel panel-default pad-m text-center">
                               <table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                     
                            <thead>
                                    <tr>
                                        <th><div>Posted Date</div></th>
                                        <th><div>Trial id</div></th>
                                        <th><div>Trial Name</div></th>
                                        <th><div>Assign date</div></th>
                                        <th><div>Billing By</div></th>
                                        <th><div>Avg Amt</div></th>
                                        <th><div>Bid Amt</div></th>                                        
                                        <th><div>Trial Type</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                 <?php 
                                 $user_id =  $this->session->userdata('user_id');
                                $recent_jobs =$this->db->query("SELECT * FROM jobs WHERE jwork_status= 6 AND 'jaccept_workid' = $user_id")->result_array();     
        ;
                                  foreach($recent_jobs as $jobs):
                                  
                                 ?>
                                    
                                    <tr>
                                        <td><?php echo $jobs['jposted_date'];?></td>
                                        <td><?php echo $jobs['job_id'];?></td>
                                        <td><?php echo $jobs['jname'];?></td>
                                       <td><?php echo $jobs['jassign_date'];?></td>
                                        <td>
                                       <?php 
                                    
                                        switch ($jobs['jbilling_by'])
                                        {
                                            case '1':
                                            $status =$jobs['jbilling_no_hours']." Hours";
                                            break;
                                            case '2':
                                            $status =$jobs['jbilling_no_days']." Days";
                                            break;
                                            
                                        }
                                        echo $status;?>
                                     </td>
                                      <td>
                                            <?php 
                                            switch ($jobs['jbidding_type'])
                                            {
                                                case '1':
                                                        switch ($jobs['jbilling_by'])
                                                    {
                                                case '1':
                                                echo $jobs['jbilling_no_hours'] * $jobs['jposter_price'] ;
                                                break;
                                                case '2':
                                                echo $jobs['jbilling_no_days'] * $jobs['jposter_price'] ;
                                                break;
                                                    }       
                                                break;
                                                case '2':
                                                echo 'N/A';
                                                break;
                                                case '3':
                                                echo 'N/A';
                                                break;
                                            }
                                            
                                                                               
                                            ?>
                                       </td>
                                        <td><?php echo $jobs['jassign_amt'];?></td>
                                     <td>
                                        <?php 
                                            switch ($jobs['jbidding_type'])
                                            {
                                                case '1':
                                                       echo "Open Bid";
                                                break;
                                                case '2':
                                                echo 'Direct';
                                                break;
                                                case '3':
                                                echo 'First bid';
                                                break;
                                            }
                                            
                                                                               
                                            ?>
                                            </td>
                                    </tr>
                                  <?php 
                                  endforeach; ?>
                                </tbody>
                                </table>
                    
                    </div>
            </div>

            
           
        </div>

    </section>
</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>



<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/dataTables.responsive.min.js"></script>

<script src="<?php echo base_url();?>assets/js/responsive.bootstrap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {

 $('.dtbl').DataTable();
} );

$(window).load(function() {

 $('.dtbl').DataTable();
} );

$(window).resize(function(){
     $('.dtbl').DataTable();
});


</script>

    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
            <p>&copy; 2016 MeteorHealth. All rights reserved.</p>
        </div>
    </div>
</footer>




<!-- Modal -->
<div class="modal fade" id="edit-acc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/account/update/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
               
                <div class="form-group">
                    <label>Profile Photo</label>
                    <input type="file" class="form-control" name="image"  placeholder="Profile Photo">
                </div>
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="form-control" name="fullname" value="<?php echo $profile_user->full_name;?>" placeholder="Full Name">
                </div>
                 <div class="form-group">
                    <label>Date of Birth</label>
                <input type="date" class="form-control" required name="dob" value="<?php if($profile->dob != '0000-00-00') { echo $profile->dob; } else { echo "";}?>" placeholder="DOB">
                </div>
                
                <div class="form-group">
                    <label>Contact Address</label>
                    <textarea class="form-control" name="address" required rows="6"  placeholder="Address"><?php echo $profile->address;?></textarea>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $profile_user->emailid;?>" placeholder="Email">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?php echo $profile->phone;?>" placeholder="Phone">
                </div> 
                <div class="form-group">
                    <label>Zip Code</label>
                    <input type="text" class="form-control" name="zip" value="<?php echo $profile_user->zipcode;?>" placeholder="Zip Code">
                </div>
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
         </form>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="deactivate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Deactivate My Account</h4>
      </div>
      <div class="modal-body">
            <form action="<?php echo base_url();?>home/confirm_deactivation/insert/<?php echo $this->session->userdata('user_id');?>" method="post" enctype="multipart/form-data" class="no-icon">
                <div class="form-group">
                    <label>Reason for Deactivation</label>
                    <textarea id="change" class="form-control" required rows="10" name="reason"></textarea>
                </div>

               
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" onclick="return confirm('Are Sure to Deactivate')" class="btn btn-primary">Deactivate My Account</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
</body>


</html>