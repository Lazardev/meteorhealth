<?php
error_reporting(E_ALL);
ini_set( 'display_errors',true); ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#job_type").on("change",function(){
                var job_type = $("#job_type").val();
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>home/get_category/", //controller url
                    data:"job_type="+job_type,
                    success:function(data)
                    {
                        $('#divCategory').html(data);
                        $('#divSubCategory').find('select').html('<option>-Select One-</option>').attr('disabled', 'disabled');
                    }
                });
            });
           
            $("#divCategory").on("change",function(){
                var category_parent_id = $("#category").val();
                $.ajax({
                    type:"POST",
                    url:"<?php echo base_url();?>home/get_subcategory/",
                    data:"category_parent_id="+category_parent_id,
                    success:function(data)
                    {
                        $('#divSubCategory').html(data);
                    }
                });
            });

            $("#divSubCategory").on("change",function(){
                
                     document.getElementById("post_task").disabled=false;
                  
            });
           
        });
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  </script>
    <style>
    .modal-body{
        padding: 15px 0px;
    }
    .nav-tabs{
        width: 100%;
        padding: 0px 35px;
        margin-bottom: 3px;
    }
    .nav-tabs>li{
        width: 50%;
    }
    .nav-tabs>li>a{
        border: 0px;
        border-radius: 4px;
    }
    .nav-tabs{
        border: 0px;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        border: 0px;
        background-color: #3c948b;
        border-color: #3c948b;
        color: #fff;
    }
    .nav-tabs>li>a:hover{
        border: 0px;
        background-color: #3c948b;
        border-color: #3c948b;
        color: #fff;
    }
    .tab-content{
        padding: 15px 15px 0px;border-top: 1px solid #ddd;
    }
    .box{
        display: none;
    }
    </style>
     
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="1"){
            $(".box").not(".red").hide();
            $(".hours").show();
        }
        
        if($(this).attr("value")=="2"){
            $(".box").not(".blue").hide();
            $(".days").show();
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")==""){
            $(".box").not(".red").hide();
            $(".hours").show();
        }
        
        if($(this).attr("value")=="2"){
            $(".box").not(".blue").hide();
            $(".days").show();
        }
    });
});
</script>
</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."home/dash_poster'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account_task">Account</a></li>
                 <li><a href="<?php echo base_url();?>home/jobs_task">Jobs</a></li>
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li> 


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>

    <ul class="filter-menu bg-grey-2 m-no-bottom">
        <li class="active"><i class="fa fa-pencil"></i> 1. Fill Out Trial Details</li>
        <li class="active"><i class="fa fa-smile-o"></i> 2. Participant Selection</li>
        <li class="active"><i class="fa fa-check"></i> 3. Confirm & Book</li>
       
    </ul>

    <div class="bg-grey-2"> 


    <section class="container section ">
        <h3>Confirm & Book</h3>
        <hr>
        <?php if($bidding_type == 1 || $bidding_type == 3){ ?>
        <form class="icon" action="<?php echo base_url();?>home/task_confirm" method="post">
           <!-- <div class="col-sm-12 fadeInRight animated panel pad-m">
<h3 class="m-no-top">How do you want to pay?</h3>
                <input id="taskPriceHour" name="jbilling_by" type="radio" value="1">
                    <b>By hours</b>
                            <p>Meteors will be paid for the hours they worked.</p>
                            <div class="hours box"> <input class="priceIncre" id="workHours" type="text" name="jbilling_no_hours"> hours at $ <input class="priceIncre" id="hourPrice" type="text" name="hour_price"> per hour.</p>
                       </div> <input id="taskPriceProject" name="jbilling_by" type="radio" value="2">
                        
                        <b>By days</b>
                            <p>Meteors will be paid for the days they worked.</p>
                        <label for="taskPriceProject">
                            
                           <div class="days box"> <p class="priceHolder"><input class="priceIncre" id="workDays" type="text" name="jbilling_no_days"> days at $ <input class="priceIncre" id="dayPrice" type="text" name="day_price"> per day.</p></div>
                        </label>
                        </div>
-->

                        <?php if($bidding_type == 1) { ?>
                        <div class="col-sm-12 fadeInRight animated panel pad-m">
                    <h3 class="m-no-top">Bidding Duration</h3> 
                        <label class="form-group" for="from">Starting Date</label>
<input type="text" class="form-control" id="from" value="" name="jbidding_st_date" />
<label class="form-group" for="to">Ending Date</label>
<input class="form-control" type="text" id="to" value="" name="jbidding_ed_date" />
                    </div>
                         <?php }?>
                        <input type="text" value="<?php echo $bidding_type; ?>" name="bidding_type" hidden="true" />
<hr>

            <button type="submit" class="btn btn-primary btn-xlg btn-block">Confirm & Book</button>
                </div>
                </div>
                </form>
                </section>
               
<?php } if($bidding_type == 2){ 
if(!$sel_taskers){
    echo "No Taskers Found";
}
    ?>
                <!-- <section class="container section margin-b30">
            <div class="text-center task-top">
           
                    <form class="form-inline icon" method="post" action="" role="form">
                        <div class="form-group">
                            <div class="control">
                                <select name="price" class="form-control">
                                    <option value="1">Recommended</option>
                                    <option value="2">Price (Lowest to Highest)</option>
                                    <option value="3">Price (Highest to Lowest)</option>                                    
                                  </select>
                                <i class="fa fa-sort"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="control">
                                <input type="date" name="date" class="form-control" placeholder="TASK DATE">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                      

                        <div class="form-group">
                            <div class="control">
                                <select name="time" class="form-control">
                                    <option value="any">ANY TIME 8am - 8pm</option>
                                    <option value="morning">MORNING 8am - 12pm</option>
                                    <option value="aft">AFTERNOON 12pm - 4pm</option>                                    
                                    <option value="eve">EVENING 4pm - 8pm</option>                                    
                                  </select>
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>                         

                        
                      <button type="submit" class="btn btn-default">Filter</button>
                    </form>
            </div>
        </section>
   -->


    <section class="container section margin-b30">
    
  <?php foreach ($sel_taskers as $task) {
              
                $tasker = $this->db->query("SELECT * FROM user_profiles WHERE user_id=".$task)->row();
                $taskers = $this->db->query("SELECT * FROM users WHERE user_id=".$task)->row();
                $price = $this->db->query("SELECT * FROM categories WHERE user_id=".$task )->row();
              
                ?>
                
        <div class="panel row user-list m-no-all">
        <div class="col-md-3 col-sm-4 col-xs-12 left text-center">
        
                     <div class="img-bx">   
                        <img src="<?php echo base_url();?>uploads/profiles/<?php echo $tasker->user_image; ?>">
                    </div>
                    
                    <br>
                    <br>
                    <!--<a class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#profileandview"> Profile & Review</a> --> 
                    <button type="button" class="btn btn-info btn-sm btn-block" onclick="profile_view('<?php echo $tasker->user_id;?>','<?php echo $taskers->full_name; ?>','<?php echo $taskers->emailid; ?>', '<?php echo $taskers->zipcode;?>')" data-toggle="modal" data-target="#myModal">Profile & Review</button>
                    
                </div> 
                <div class="col-md-9 col-sm-8 col-xs-12 right">
                    <h3 class="m-no-top">
                        <strong><?php echo $taskers->full_name; ?></strong>
                        <span class="bg-primary pull-right">$ <?php echo $price->$category_name?>/hr</span>
                    </h3>
                   <?php 
            $jobsf = $this->db->get_where('jobs', array('jaccept_workid' => 2))->result_array();
    $count = $this->db->count_all_results();
     $rate = 0;
     $rt1 = 0;
      foreach($jobsf as $jobs){

    if($jobs['jstar_rate']){   


             $rt1 = $rate + $jobs['jstar_rate'];
             
             }}
         $rt = round($rt1 / $count);
$img="";
$i=1;
while($i<=$rt){
$img=$img."<img src=".base_url()."template/images/star.gif >";
$i=$i+1;
}
while($i<=5){
$img=$img."<img src=".base_url()."template/images/star2.gif >";
$i=$i+1;
}
echo $img;

?>
                   <hr>
                    <h5><strong>About me</strong></h5>
                    <p><?php echo $tasker->about_info; ?></p>
                    <br>
                    <div class="testi row">
                    <form class="icon" action="<?php echo base_url();?>home/task_confirm/<?php echo $tasker->user_id;?>/<?php echo $category_id; ?>/<?php echo $category_name; ?>/2" method="post">
                
                    <!-- <form method="post" action"<?php echo base_url();?>home/task_confirm/<?php echo $tasker->user_id;?>/<?php echo $category_id; ?>/<?php echo $category_name; ?>/2"  role="form"> -->
                        <div class="panel panel-default pad-m">
<h4>How long your work will be?</h4>
                <div class="howToPay">
                    <div class="radio_button">
<input id="taskPriceHour" name="jbilling_by" type="radio" value="1" />
                    <b>By hours</b>
                            <p>Meteors will be paid for the hours they worked.</p>
                        
                        <div class="hours box"><label for="taskPriceHour">
                            
                            <p class="priceHolder">
                            <input class="priceIncre" id="workHours" type="text" name="jbilling_no_hours" /> hours 
                        </p></label></div>
                    </div>

                    <div class="radio_button ">
                        <input id="taskPriceProject" name="jbilling_by" type="radio" value="2" />
                        <b>By days</b>
                            <p>Meteors will be paid for the days they worked (8 Hours per day).</p>
                       <div class="days box"> <label for="taskPriceProject">
                            
                            <p class="priceHolder"><input class="priceIncre" id="workDays" type="text" name="jbilling_no_days" /> days 
                        </label></p>
                        <input type="text" value="<?php echo $price->$category_name?>" name="price" hidden="true" />
                    </div>
                </div></div>
                    </div>

                    
                </div>
                <button type="submit" class="btn btn-primary btn-sm btn-block">Select & Confirm</button> 
            </div>
          </form>
                <?php } ?> 
                  

    </section>
</div>
<?php }?>

                </div>
                 <script>
                        function profile_view(id,name,email,zipcode){
                             $(document).ready(function() {

    $.ajax({url : '<?php echo base_url();?>home/profile_view/'+id,
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
         async: true,
        success : function (result) {
            $('#modal-img').attr('src','<?php echo base_url();?>uploads/profiles/'+result['user_image']);
            $('#modal-pname').html(name);
            $('#modal-pemail').html(email);
            $('#modal-pphone').html(result['phone']);
            $('#modal-pzip').html(zipcode);
            $('#modal-right').html(result['right_person']);
            $('#modal-not').html(result['not_tasking']);
            $('#modal-task').html(result['tasking']);

            
            
           //console.log(result['advert']) // The value of your php $row['adverts'] will be displayed
        }
    });
                        });
                    }
                    </script>

    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <br />
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title" id="myModalLabel">Profile View</h4>
     </div>
     <div class="modal-body">
       <!-- Nav tabs -->
 <!-- Nav tabs -->
   

  <!-- <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
   </ul> -->

  <!-- Tab panes -->
 <div class="tab-content">
        <div class="tab-pane active" id="profile">
                 <div class="animated fadeInLeft">
                        <h3>A few fun facts about me:</h3>
                        <div class="panel panel-default pad-m text-center">
                        
                        <img src="" id="modal-img" class="user-img">
                    <div class="space-sm"></div>
                    <i class="fa fa-user"></i> <span id="modal-pname"></span><br>
                    <i class="fa fa-envelope-o"></i> <span id="modal-pemail"></span><br>
                    <i class="fa fa-phone"></i> <span id="modal-pphone"></span><br>
                    <i class="fa fa-map-marker"> </i><span id="modal-pzip"></span><br>
                    
                    </div>
                        <h4>Why I'm your Tasker:</h4>

                        <div id="modal-right"></div>

                        <h4><strong>When I'm not tasking...</strong></h4>

                        <div id="modal-not"></div>

                        <h4><strong>When I'm tasking I always make sure to...</strong></h4>
                        <div id="modal-task"></div>


                    
                    </div>
        </div>
     </div>
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
     </div>
   </div>
 </div>
</div> 
</body>


</html>