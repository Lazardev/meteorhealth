<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>
    <?php $user_id =$this->session->userdata('user_id'); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
        <script>
        setInterval(function () { autoloadpage(); }, 300); // it will call the function autoload() after each 30 seconds. 
        function autoloadpage() {
            $.ajax({url : "<?php echo base_url();?>home/chat_user_check/<?php echo $user_id; ?>",
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
         async: true,
        success : function (result) {
              if(typeof result['message_tasker'] ==='undefined')
             {
                
             }
              else if(result['message_tasker']!=''){
                    $("#chat").html('<a href="<?php echo base_url();?>home/chat/'+result['tasker']+'/<?php echo $user_id; ?>">New Message</a>'); // here the wrapper is main div
             
             } }
                }
            );
        }
        </script>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url(); ?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url(); ?>assets/css/slippry.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>template/css/bootstrap.min.css" rel="stylesheet">
  <!-- <link href="<?php echo base_url();?>template/css/admin-css.css" rel="stylesheet"> -->
    <!-- Flexslider core CSS -->
  <link href="<?php echo base_url();?>template/css/flexslider.css" rel="stylesheet">
    <!-- Datepicker styles for this template -->
  <link href="<?php echo base_url();?>template/css/datepicker.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>template/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>template/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

    <!-- Custom styles for this template -->
  <link href="<?php echo base_url();?>template/css/style.css" rel="stylesheet">
    <!--  SCustom Style -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbarSettings">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url();?>home/dash_poster"><img src="<?php echo base_url(); ?>assets/img/logo.png"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right ">

                <li id="chat"></li>
                <li><a href="<?php echo base_url();?>home/dash_poster">Home</a></li>
                <li><a href="<?php echo base_url();?>home/account">Account</a></li>
                <li><a href="<?php echo base_url();?>home/jobs">Jobs</a></li> 
                <li><a href="<?php echo base_url();?>home/logout">Logout</a></li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>
    <!-- Header Box -->
   <div class="space"></div>
    <div class="space"></div>
    

<div class="bg-grey-1">
    <section class="container section">
    <div class="space"></div>


        <h2>Job Bids</h2>
<div class="col-xs-10 col-xs-offset-1 task-form-container whiteContainer">
		
			<div class="col-xs-12">
            
				  <?php
				 	$id="";
					$assign_id ="";
					$job_reject ="";
					foreach($job_view as $jobs):
					$id=$jobs['job_id'];
					$assign_id =$jobs['jassign_to_work_id'];
					$job_reject =$jobs['jwork_status'];
                  ?>
				      <div class="jobEntry">
					<h4><a href="#"><?php echo $jobs['jname'];?></a></h4>
					<div align="right">
                        <ul class="secondaryInfo">
                            <li title="Posted Date">Posted <i class="fa fa-calendar"></i> <?php echo $jobs['jposted_date'];?></li>
                            <li title="Bill by"><i class="hourGlass"></i> <?php 									
										switch ($jobs['jbilling_by'])
										{
											case '1':
											$status =$jobs['jbilling_no_hours'].' Hour Based';
											$price =$jobs['jbilling_no_hours']*$jobs['jposter_price'];
											break;
											case '2':
											$status =$jobs['jbilling_no_days'].' Day Based';
											$price =$jobs['jbilling_no_days']*$jobs['jposter_price'];
											break;
											
										}
										echo $status;?></li>
						<li title="Price"><i class="fa fa-dollar"></i> <?php echo $price; ?></li>
                        </ul>
                    </div>
                     <?php endforeach; ?>
                     
					
                    
                    		<table class="dtbl table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                     
                                <thead >
                                    <tr>
                                        <th><div>Worker ID</div></th>
                                        <th><div>Worker Name</div></th>
                                        <th><div>Bid Date</div></th>
                                        <th><div>Bid Amount</div></th>
                                        <th><div>Comments</div></th>
                                        <th><div>Options</div></th>
                                        <th><div>Chat</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 <?php
								 $this->db->order_by('bid_amt desc'); 
								 $bids=$this->db->get_where('job_bids', array('job_id' => $id))->result_array();


								   foreach($bids as $bid):?>
                                	<tr>
                                    	<td ><?php echo $bid['userid'];?></td>
                                         <td>
										 <?php
											$output = $this->db->query("SELECT * FROM users WHERE user_id=".$bid['userid'])->row()->full_name;
											echo ucfirst($output);
									     ?>
										</td>
                                    	<td><?php echo $bid['biding_dt'];?></td>
                                        <td><?php echo $bid['bid_amt'];?></td>
                                        <td><?php echo $bid['bid_commands'];?></td>
                                         <td>
                                         
                    <?php if($assign_id!= $bid['userid'] && $job_reject !='8' ){

                        $status = $this->db->query("SELECT * FROM users WHERE user_id =". $bid['userid'])->row();
                        ?>
                                      
                                            <a href="#" data-toggle="modal" data-target="#myModal" onclick="profile_view('<?php echo $bid['userid'];?>','<?php echo $output; ?>','<?php echo $status->emailid; ?>', '<?php echo $status->zipcode;?>')">View profile</a> -
                                            <a href="<?php echo base_url();?>home/pjob_assign/<?php echo $jobs['job_id'];?>/<?php echo $bid['userid'];?>/<?php echo $bid['bid_amt']; ?>/" onclick="return confirm('Are you sure to Assign?');">Assign</a> 
                                         </td>
                                         <td> 
                                         <a href="<?php echo base_url();?>home/chat/<?php echo $bid['userid'];?>/<?php echo $user_id;?>" >Chat</a> 
                                         </td>
                                    </tr>
                                  <?php }else {
									  //echo "<span style=\"color:Red\">Rejected</span>";
								  } 
								  endforeach; ?>
								  <?php 
								  if(!$bids){
										echo "<tr><tdcolspan='6'><center>No record found</center></td></tr>";
									}
									?>
                                </tbody>
                                </table>
                    
                    </div>
				</div>
		

			</div>

		</div>
      <script>
                        function profile_view(id,name,email,zipcode){
                             $(document).ready(function() {

    $.ajax({url : '<?php echo base_url();?>home/profile_view/'+id,
        type : 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
         async: true,
        success : function (result) {
            $('#modal-img').attr('src','<?php echo base_url();?>uploads/profiles/'+result['user_image']);
            $('#modal-pname').html(name);
            $('#modal-pemail').html(email);
            $('#modal-pphone').html(result['phone']);
            $('#modal-pzip').html(zipcode);
            $('#modal-right').html(result['right_person']);
            $('#modal-not').html(result['not_tasking']);
            $('#modal-task').html(result['tasking']);

            
            
           //console.log(result['advert']) // The value of your php $row['adverts'] will be displayed
        }
    });
                        });
                    }
                    </script>
       
     <div class="space"></div>
    <div class="space"></div>
        
 
    </section>

</div>




    <!-- Scripts-->
    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Modernizr -->
<script src="<?php echo base_url(); ?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url(); ?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url(); ?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url(); ?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url(); ?>assets/js/min/scripts.min.js"></script>

<!--[if lt IE 9]>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->



    <!-- Footer-->
    <footer class="footer">


    <div class="footer-bottom">
        <div class="container">
           <p>&copy; <?php echo date("Y");?>, <?php echo $site_name;?>. All rights reserved.</p>
        </div>
    </div>
</footer>

</body>


</html>



