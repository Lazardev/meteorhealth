<?php
require 'facebook/facebook.php';
$appId = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_apikey'")->row()->description;
$secret = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_applicationkey'")->row()->description;
$facebook_profile = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_profile'")->row()->description;
$facebook = new Facebook(array(
  'appId'  => $appId,
  'secret' => $secret,
));
$user = $facebook->getUser();
if ($user) {
  try {
    $user_profile = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }
}
 
if(isset($user)) {
	$loginUrl = $facebook->getLoginUrl(array(
		'scope'		=> 'user_about_me, user_hometown, email, user_birthday',
		'redirect_uri'	=> $facebook_profile,
	));
}
?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="js-no ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="js-no ie10"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
   <?php include(__DIR__."/../sitesettings_info.php"); ?>
        
        <title><?php echo $site_name;?></title>

    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">


    <!--  Boostrap Framework  -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--=== CSS ===-->
    <link href="<?php echo base_url();?>assets/css/themes/green.css" rel="stylesheet" id="colors">

    <!-- Google Fonts - Lato -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- CSS Animations -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/3.1.1/animate.min.css" rel="stylesheet">

    <!--  Slippry Slideshow -->
    <link href="<?php echo base_url();?>assets/css/slippry.min.css" rel="stylesheet">

    <!--  SCustom Style -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.growl.js" type="text/javascript"></script>
    <link href="<?php echo base_url();?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    
</head>
<body>
<?php if($this->session->flashdata('flash_message') != ""):?>
        <script type="text/javascript">
  
  $.growl({ title: "", message: "<?php echo $this->session->flashdata('flash_message') ?>" });
</script>
        <?php endif;?>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <?php
            if(!empty($site_logo)){
                echo "<a class='navbar-brand' href='".base_url()."'><img src='".base_url()."uploads/{$site_logo}' /> </a>";
            }
            else{
        ?>
            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            <?php }?>
            </div>

            
            <!-- /.navbar-collapse -->
        </div>
    </nav>

    <section class="bg-8 bg-center bg-cover  section-fxd">
        <div class="bg-filter">
            <div class="hv-center">
                
                <div class="sign-up">
                    <div class="sign-up-hr hidden-xs"></div>
                    <h1 class="sign-up-title">Create new account</h1>
                    <div class="row">
                    <?php if($this->session->flashdata('flash_message')): ?>
						<p style="color: red;text-align: center;" class='flashMsg flashSuccess'> <b><?=$this->session->flashdata('flash_message')?></b> </p>
						<?php endif ?>
                        <div class="col-md-6 col-sm-6 icon" role="form">
                         <form action="<?php echo base_url();?>home/signup/user_create" onsubmit="return checkPass()" method="post" enctype="multipart/form-data" >
                    
                              <div class="form-group ">
                                    <div class="control">
                                        <input type="text" class="form-control" required name="full_name" placeholder="Full name">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="control">
                                        <input type="email" class="form-control" required name="email" placeholder="Email">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="control">
                                        <input type="password" class="form-control" required id="pass1" name="password" placeholder="Password">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                </div>
                                 <div class="form-group">
                            <div class="control">
                            <input type="password" required class="form-control" id="pass2"  name="new_pwd1" placeholder="Confirm new password">
                            <i class="fa fa-lock"></i>
                        </div>
                        <span id="confirmMessage" class="confirmMessage"></span>
                        </div>
                                <div class="form-group ">
                                    <div class="control">
                                        <input type="text" required class="form-control" name="zipcode" placeholder="Zipcode">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success btn-block">
                                    <i class="fa fa-check"></i>Create New Account</button>
                                <div class="space visible-sm visible-xs"></div>
                            </form>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <?php 
				$facebook_status	=	$this->db->get_where('sitesettings' , array('type'=>'facebook_status'))->row()->description;
				if($facebook_status !=2){ ?>
                            <a href="<?php echo $loginUrl; ?>" title=" Sign up with Facebook">
                                <div class="sign-up-btn facebook">
                                    <div class="icon">
                                        <i class="fa fa-facebook"></i>
                                    </div>
                                    <div class="text">
                                        Sign up with Facebook
                                    </div>
                                </div>
                            </a>
                            <?php 
				}
				?>			<a href="<?php echo base_url();?>home/login">
                            <button type="submit"  class="btn btn-info btn-block">
                                    <i class="fa fa-sign-in"></i>Signin</button> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--Back to top-->
<a href="#" class="back-to-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Modernizr -->
<script src="<?php echo base_url();?>assets/js/min/modernizr.custom.min.js"></script>

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="<?php echo base_url();?>assets/js/min/bootstrap-hover-dropdown.min.js"></script>

<!-- LESS preprocessor -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/1.7.4/less.min.js"></script>

<!-- WOW.js - loading animations -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/wow/0.1.6/wow.min.js"></script>

<!-- Knobs - our skills -->
<script src="http://cdn.jsdelivr.net/jquery.knob/1.2.9/jquery.knob.min.js"></script>

<!-- Slippry - Slideshow -->
<script src="<?php echo base_url();?>assets/js/min/slippry.min.js"></script>

<!-- Mixitup plugin - Portfolio Filter Grid -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/mixitup/1.5.6/jquery.mixitup.min.js"></script>

<!-- Make sticky whatever elements -->
<script src="http://cdn.jsdelivr.net/jquery.sticky/1.0.0/jquery.sticky.min.js"></script>

<!-- Smooth sroll -->
<script src="http://cdn.jsdelivr.net/jquery.nicescroll/3.5.4/jquery.nicescroll.min.js"></script>

<!-- Contact Form -->
<script src="<?php echo base_url();?>assets/js/min/contact-form.min.js"></script>

<!-- Must be last of all scripts -->
<script src="<?php echo base_url();?>assets/js/min/scripts.min.js"></script>
<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
        alert("Passwords do not match.");
            return false;
    }
}  
</script>

</body>


</html>



