
<!-- START Template Main -->
<section id="main" role="main">
    <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Language Packs</h4>
                <select id="take_lang" class="form-control pull-right" style="width:30%;" onchange="get_lang()">
                    <option value="">Choose Language</option>
                    <?php 
                    foreach($languages as $language){

                        echo '<option value="'.$language->label.'">'.$language->name.'</option>';

                    }
                    ?>


                </select>
            </div>
        </div>
        <!-- Page Header -->

        <!-- START row -->
        <div class="row">
            <div class="col-md-12">
                <!-- START panel -->
                <div class="panel panel-primary">
                    <!-- panel heading/header -->
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="panel-icon mr5"><i class="ico-table22"></i></span>All the Languages</h3>
                        <!-- panel toolbar -->
                        <div class="panel-toolbar text-right">
                            <!-- option -->
                            <div class="option">
                                <button class="btn up" data-toggle="panelcollapse"><i class="arrow"></i></button>
                                <button class="btn" data-toggle="panelremove" data-parent=".col-md-12"><i class="remove"></i></button>
                            </div>
                            <!--/ option -->
                        </div>
                        <!--/ panel toolbar -->
                    </div>
                    <!--/ panel heading/header -->
                    <!-- panel toolbar wrapper -->
                    <div class="panel-toolbar-wrapper pl0 pt5 pb5">
                        <!-- <div class="panel-toolbar pl10">
                            <div class="checkbox custom-checkbox pull-left">  
                                <input type="checkbox" id="customcheckbox" value="1" data-toggle="checkall" data-target="#table1">  
                                <label for="customcheckbox">&nbsp;&nbsp;Select all</label>  
                            </div>
                        </div> -->
                        <div class="panel-toolbar text-right">
                            <!-- <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-default"><i class="ico-upload22"></i></button>
                                <button type="button" class="btn btn-sm btn-default"><i class="ico-archive2"></i></button>
                            </div>

                            <button type="button" class="btn btn-sm btn-danger delete_users"><i class="ico-remove3"></i></button> -->
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#add_new_lang"><i class="ico-plus"></i> ADD NEW LANGUAGE</button>
                        </div>
                    </div>
                    <!--/ panel toolbar wrapper -->


                    <!-- panel body with collapse capabale -->
                    <div class="table-responsive panel-collapse pull out">
                        <table class="table table-bordered table-hover" id="table1">
                            <thead>
                                <tr>
                                    <th width="3%" class="text-center"><i class="ico-long-arrow-down"></i></th>

                                    <th>Name</th>
                                    <th >label</th>
                                    <th width="20%"></th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                               foreach($languages as $language){
                                
                                echo "<tr>
                                <td>
                                    <div class='checkbox custom-checkbox nm'>  
                                        <input type='checkbox' id='customcheckbox-".$language->id."' name='checkboxlist' value='".$language->id."' data-toggle='selectrow' data-target='tr' data-contextual='success'>
                                        <label for='customcheckbox-".$language->id."'></label>   
                                    </div>
                                </td>
                                <td>".$language->name."</td>
                                <td>".$language->label."</td>
                                <td class='text-center'>
                                    <!-- button toolbar -->
                                    <div class='toolbar'>
                                        <div class='btn-group'>
                                        <a type='button' class='btn btn-sm btn-info' href='language/view/".$language->id."'>View</a>";
                                        if($language->status){
                                            echo "<a type='button' class='btn btn-sm btn-warning' href='language/suspend/".$language->id."'>Suspend</a>";
                                        }else{
                                            echo "<a type='button' class='btn btn-sm btn-success' href='language/unsuspend/".$language->id."'>Unsuspend</a>";
                                        }

                                        echo "</div>
                                    </div>
                                    <!--/ button toolbar -->
                                </td>
                        </tr>";

                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!--/ panel body with collapse capabale -->
    </div>
</div>
</div>
<!--/ END row -->


</div>
<!--/ END Template Container -->

<!-- START To Top Scroller -->
<a href="#" class="totop animation" data-toggle="waypoints totop" data-marker="#main" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="-50%"><i class="ico-angle-up"></i></a>
<!--/ END To Top Scroller -->
</section>
<!--/ END Template Main -->
<div class="modal fade" id="add_new_lang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add New Language</h4>
    </div>
    <form action="language/add" method="post" accept-charset="utf-8" enctype="multipart/form-data">    <div class="modal-body">
        <div class="form-group">
            <label for="lang_name">Language Name</label>
            <input type="text" name="lang_name" class="form-control" id="plan_name" placeholder="Enter Language Name | For Ex: English, Spanish" required>
        </div>
        <div class="form-group">
            <label for="lang_label">Language Label</label>
            <input type="text" name="lang_label" class="form-control" id="no_of_credits" placeholder="Ex: en, es" required>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
    </form></div>
</div>
</div>

<script>



function get_lang() {
    var take_lang = $("#take_lang").val();
    window.location="language/changelang?label="+take_lang;
}


</script>
