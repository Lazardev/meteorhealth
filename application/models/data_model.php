<?php

class data_model extends CI_Model
{
    public function users_count_month($date)
    {
        $query = $this->db->query("select * from t_entity where  DATE_FORMAT(Create_Dt, '%Y-%m') = '2014-03'");
        $count = $query->num_rows();
        return $count;
    }
    public function users_count()
    {
        $query = $this->db->query("select *  from t_entity where Create_Dt=NOW()");
        $count = $query->num_rows();
        
        return $count;
    }

    public function website_info()
    {
        $query = $this->db->query("select setting_value from t_settings where id=1");
        return $query->result();
    }

    public function website_info_get(){

        $query = $this->db->query("select setting_value from t_settings where id=1");
        return $query->result();

        
    }

    public function change_defalut($new_lang){

    $query = $this->db->query("UPDATE  `languages` SET `default`=0 WHERE `label`!='$new_lang'");


      if ($query) {
            return 1;
            
        } else {
            return 0;
        }




    }

    public function users_count_date($date)
    {
        $query = $this->db->query("select * from t_entity where date(Create_Dt)='$date'");
        $count = $query->num_rows();
        return $count;
    }
    public function new_male_users_count($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE date(t_entity.Create_Dt)='$date' and t_entity_details.sex=1");
        $count = $query->num_rows();
        
        return $count;
    }
    public function new_female_users_count($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE date(t_entity.Create_Dt)='$date' and t_entity_details.sex=2 ");
        $count = $query->num_rows();
        
        return $count;
    }
    public function users_count_date_month($date)
    {
        $query = $this->db->query("select * from t_entity where DATE_FORMAT(Create_Dt, '%m-%Y') = '$date'");
        $count = $query->num_rows();
        return $count;
    }
    public function new_male_users_count_month($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE DATE_FORMAT(t_entity.Create_Dt, '%m-%Y') ='$date' and t_entity_details.sex=1");
        $count = $query->num_rows();
        
        return $count;
    }
    public function new_female_users_count_month($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE DATE_FORMAT(t_entity.Create_Dt, '%m-%Y') ='$date' and t_entity_details.sex=2 ");
        $count = $query->num_rows();
        
        return $count;
    }
    public function users_count_date_year($date)
    {
        $query = $this->db->query("select * from t_entity where DATE_FORMAT(Create_Dt, '%Y') = '$date'");
        $count = $query->num_rows();
        return $count;
    }
    public function new_male_users_count_year($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE DATE_FORMAT(t_entity.Create_Dt, '%Y') ='$date' and t_entity_details.sex=1");
        $count = $query->num_rows();
        
        return $count;
    }
    public function new_female_users_count_year($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE DATE_FORMAT(t_entity.Create_Dt, '%Y') ='$date' and t_entity_details.sex=2 ");
        $count = $query->num_rows();
        
        return $count;
    }


    public function users_count_date_week($date)
    {
        $query = $this->db->query("select * from t_entity where WEEK(Create_Dt) = '$date'");
        $count = $query->num_rows();
        return $count;
    }
    public function new_male_users_count_week($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE WEEK(t_entity.Create_Dt) ='$date' and t_entity_details.sex=1");
        $count = $query->num_rows();
        
        return $count;
    }
    public function new_female_users_count_week($date)
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE WEEK(t_entity.Create_Dt) ='$date' and t_entity_details.sex=2 ");
        $count = $query->num_rows();
        
        return $count;
    }
    public function chat_count()
    {
        $query = $this->db->query("select * from t_chatmessages");
        $count = $query->num_rows();
        return $count;
    }
    
    public function male_count_today()
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE t_entity_details.sex=1 and MONTH(t_entity.Create_Dt)=MONTH(NOW()) and YEAR(t_entity.Create_Dt)=YEAR(NOW()) and DAY(t_entity.Create_Dt)=DAY(NOW()) ");
        $count = $query->num_rows();
        
        return $count;
    }
    public function female_count_today()
    {
        $query = $this->db->query("SELECT t_entity_details.First_Name,t_entity.Create_Dt FROM t_entity_details Inner join t_entity on t_entity.Entity_Id=t_entity_details.Entity_Id WHERE t_entity_details.sex=2 and MONTH(t_entity.Create_Dt)=MONTH(NOW()) and YEAR(t_entity.Create_Dt)=YEAR(NOW()) and DAY(t_entity.Create_Dt)=DAY(NOW()) ");
        $count = $query->num_rows();
        
        return $count;
    }
    
    public function count_users() {
        return $this->db->count_all("t_entity_details");
    }
 
    public function fetch_users($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->select('*');
        $this->db->from('t_entity_details');
        $this->db->join('t_entity', 't_entity_details.entity_id=t_entity.entity_id');
        $this->db->join('t_city','t_city.City_id=t_entity_details.city');
        $this->db->order_by("t_entity.Create_Dt", "DESC");
        $query = $this->db->get();

        // $query = $this->db->query("SELECT * FROM (t_entity_details inner join t_entity on t_entity_details.entity_id=t_entity.entity_id inner join t_city on t_city.City_id=t_entity_details.city)");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    
    public function users_list()
    {
        $query = $this->db->query("SELECT * FROM (t_entity_details inner join t_entity on t_entity_details.entity_id=t_entity.entity_id inner join t_city on t_city.City_id=t_entity_details.city)");
        return $query->result();
        
    }
    public function users_list_search($string)
    {
        $query = $this->db->query("SELECT * FROM (t_entity_details inner join t_entity on t_entity_details.entity_id=t_entity.entity_id inner join t_city on t_city.City_id=t_entity_details.city) where t_entity_details.First_Name like '%$string%' or t_entity_details.Last_Name like '%$string%'");
        return $query->result();
        
    }

     public function see_language($label)
    {
        $query = $this->db->query("SELECT * FROM `languages` WHERE `label`='$label'");
        $count = $query->num_rows();
        return $query->result();
    }

    public function get_defalut()
    {
        $query = $this->db->query("SELECT * FROM `languages` WHERE `default`=1");
        $count = $query->num_rows();
        return $query->result();
    }

    public function get_users_details(){
         $query = $this->db->query("SELECT * FROM `t_entity_details`");
         $count = $query->num_rows();
         return $count;
    }
    


    public function get_language($label)
    {

        
        $query = $this->db->query("UPDATE  `languages` SET `default`=1 WHERE `label`='$label'");
        if ($query) {
            return 1;
            
        } else {
            return 0;
        }
        
    }

    
    public function addadmin($username, $password)
    {
        $query = $this->db->query("insert into t_admin (admin_username,admin_password) values('$username','$password')");
        if ($query) {
            return 1;
            
        } else {
            return 0;
        }
        
    }
    public function admin_lists(){
        $query = $this->db->query("SELECT * from t_admin");
        return $query->result();

    }


    
    public function delete_user($eid)
    {
        $query_entity        = $this->db->query("delete from t_entity where Entity_Id=$eid");
        $query_entity_detail = $this->db->query("delete from t_entity_details where Entity_Id=$eid");
        $query_images = $this->db->query("delete from t_images where entity_id=$eid");
        $query_spotlight = $this->db->query("delete from spotlight where user_id=$eid");
        $query_messages = $this->db->query("delete from t_chatmessages where sender=$eid or receiver=$eid");
        $query_education = $this->db->query("delete from t_education where Entity_Id=$eid");
        $query_likes = $this->db->query("delete from t_likes where Entity1_Id=$eid or Entity2_Id=$eid");
        $query_notifications = $this->db->query("delete from t_notifications where sender=$eid or reciever=$eid");
        $query_preferences = $this->db->query("delete from t_preferences where Entity_Id=$eid");
        $query_user_friends = $this->db->query("delete from t_user_friends where entity_id=$eid");
        $query_user_likes = $this->db->query("delete from t_user_likes where entity_id=$eid");
        $query_users_credits = $this->db->query("delete from users_credits where user_id=$eid");

        return 1;
    }
    public function multiple_delete_user($eid)
    {
        $this->db->where_in('Entity_Id', $eid);
        $query_entity = $this->db->delete('t_entity');
        $this->db->where_in('Entity_Id', $eid);
        $query_entity_detail = $this->db->delete('t_entity_details');
        if ($query_entity && $query_entity_detail) {
            return 1;
        } else {
            return 0;
        }
        
    }
    
    public function suspend($id)
    {
        $data = array(
            'Status' => "0"
        );
        if ($this->db->where('Entity_id', $id)->update('t_entity', $data)) {
            return 1;
        } else {
            return 0;
        }
        
    }
    
    public function unsuspend($id)
    {
        $data = array(
            'Status' => "1"
        );
        if ($this->db->where('Entity_id', $id)->update('t_entity', $data)) {
            return 1;
        } else {
            return 0;
        }
        
    }
    public function delete_admin($id){
        if ($this->db->query("delete from t_admin where aid=$id")) {
            return 1;
        } else {
            return 0;
        }

    }
    public function suspend_admin($id)
    {
        $data = array(
            'status' => "0"
        );
        if ($this->db->where('aid', $id)->update('t_admin', $data)) {
            return 1;
        } else {
            return 0;
        }
        
    }
    
    public function unsuspend_admin($id)
    {
        $data = array(
            'status' => "1"
        );
        if ($this->db->where('aid', $id)->update('t_admin', $data)) {
            return 1;
        } else {
            return 0;
        }
        
    }
    public function edit_admin($id,$username,$password)
    {
        $data = array(
            'admin_username' => $username,
            'admin_password' => $password
            );
        if($this->db->where('aid',$id)->update('t_admin', $data)){
            return 1;
        }else{
            return 0;
        }

    }
    
    
    
}






