<?php

class language_model extends CI_Model {

	public function get_all_language(){
		$query = $this->db->query('SELECT * FROM languages');
		return $query->result();
	}

	public function add($lang_name,$lang_label){
		$query = $this->db->query("SELECT count(*) AS count from languages WHERE name = '$lang_name' OR label = '$lang_label'")->row_array();
		if($query['count'] <= 0){
			$query = $this->db->query("INSERT INTO languages (id,name,label,status) VALUES('null','$lang_name','$lang_label','1')");
			if($query)
			{
				return 1;
			}else {
				return 0;
			}
		}else{
			return 2;
		}
		
	}
	public function get_label($id)
	{
		$query = $this->db->get_where('languages', array('id' => $id))->row();
		$label = $query->label;
		return $label;
	}
	public function get_language($id)
	{
		$query = $this->db->get_where('languages', array('id' => $id))->row();
		$name = $query->name;
		return $name;
	}
	public function get_lang($label)
	{
		$query = $this->db->get_where('languages', array('label' => $label))->row();
		$name = $query->name;
		return $name;
	}
	public function list_files($id,$path)
	{
		$this->load->helper('directory');
		$query = $this->db->get_where('languages', array('id' => $id))->row();
		$label = $query->label;
		//die($label);
		//die("../app/lang/$label/");
		$dir_map=directory_map("../app/lang/$label/",false,true);
		//die(var_dump($dir_map));
		return $dir_map;

	}
	public function suspend($id){
		$data = array(
			'status' => 0
			);
		if($this->db->where('id', $id)->update('languages', $data))
		{
			return 1;
		}else{
			return 0;
		}
	}
	public function unsuspend($id){
		$data = array(
			'status' => 1
			
			);
		if($this->db->where('id', $id)->update('languages', $data))
		{
			return 1;
		}else{
			return 0;
		}
	}

}