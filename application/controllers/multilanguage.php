<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class language extends CI_Controller
{
	protected $path = '../../app/lang/';
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') === FALSE) {
			redirect('login');
		}
	}
	public function get(){
		$this->load->model('language_model');
        $this->load->model('data_model');
		$data['languages'] = $this->language_model->get_all_language();
		$page_data['page_name']  = 'language';
        $page_data['page_title'] = 'Administrator Dashboard';
        $this->load->view('admin/index',$page_data);
		}


    public function changelang(){

       $new_lang = $this->input->get('label');
       $this->load->model('data_model');
      


       $current_lang =  $this->data_model->see_language($new_lang);

     

       if($current_lang[0]->default == 1){

        redirect('language', 'refresh');

       }else{

         //$get_langvalue = $this->data_model->get_language($label);
        $current_lang =  $this->data_model->get_defalut();
        
        
         
        // die($current_lang[0]->label);

         $lang_check = file_get_contents('../app/config/app.php');
        //die($lang_check);
        $change_file = "'locale' => '".$current_lang[0]->label."'";
        $change_need = "'locale' => '".$new_lang."'";

        $change_file_one = "'fallback_locale' => '".$current_lang[0]->label."'";
       $change_need_one = "'fallback_locale' => '".$new_lang."'";
        //die($change_need);
       $string = str_replace($change_file,$change_need,$lang_check);

       $string_one = str_replace($change_file_one,$change_need_one,$string);

       //die($string_one);

       if(file_put_contents('../app/config/app.php', $string)) {

        $change_app_another = file_put_contents('../app/config/app.php', $string_one);

        $get_langvalue = $this->data_model->get_language($new_lang);

        $change_defalut = $this->data_model->change_defalut($new_lang);
        redirect('language', 'refresh');
       }    
       // die(print_r(htmlentities($string)));


       }

       //$data = file_get_contents('../app/config/app.php');
       //str_replace("world","Peter","Hello world!");
       
    }

	public function add()
	{
		$this->load->model('language_model');
		$lang_name=$this->input->post('lang_name');
		$lang_label=$this->input->post('lang_label');

		$this->load->helper('directory');

       // die(print_r($this->path));
		$srcdir = '../app/lang/en';
		$dstdir = '../app/lang/'.$lang_label;

        //die($dstdir);

        //creating the destination directory
		if(!is_dir($dstdir)){

			mkdir($dstdir,0777,true);

        }

        //Mapping the directory
			$dir_map=directory_map($srcdir);
           // die(print_r($dir_map));


			foreach($dir_map as $object_key=>$object_value)
			{
				if(is_numeric($object_key))
                copy($srcdir.'/'.$object_value,$dstdir.'/'.$object_value);//This is a File not a directory
            else
                directory_copy($srcdir.'/'.$object_key,$dstdir.'/'.$object_key);//this is a directory
        }
        $result = $this->language_model->add($lang_name,$lang_label);
        if($result == 1)
        {
        	redirect('language', 'refresh');
        } elseif ($result == 2) {
        	echo "Duplicate entry not allowed <a href=\"../language\">Return Back</a>";
        }else{
        	echo "failed";
        }


    }

    public function save()
    {

       //$fromdetails = Config::get('../app/mail.from');
        //$fromdetails = Config::get('package::mail.from');

        //die('hi');
        //die($fromdetails);


         $this->load->model('data_model');
    	$this->load->helper('file');
    	$label = $this->input->get('label');
    	$filename = $this->input->get('file');
    	$data = $this->input->post();
        $string ="";
        $save = '<?php return array( ';
        foreach ($data as $key => $value) {
                $string.='"'.$key.'" => "'.$value.'" ,';
            }
        $string = '<?php return array('.$string.') ?>';
            // die('string');
            $contents = $string;

            //die('../app/lang/'.$label.'/'.$filename);

        
    		$file = '../app/lang/'.$label.'/'.$filename;
            //$putthis = file_put_contents($file, $contents);

            //die($putthis);
           // $handle = fopen($file, "w");

    		//$current = file_get_contents($file);
			// Append a new person to the file
            //die(print_r($current));
			$current = $contents;
            //die(htmlentities($current));
           // die($current);
			// Write the contents back to the file
			if(file_put_contents($file, $current))
    		{

               // $data2['info'] = $this->data_model->website_info();
                //$email_file = file_get_contents('../app/config/mail.php');

               // $data3['info_current'] = $this->data_model->website_info_get();


               //die(print_r($data2['info'][0]->setting_value));
                //$string_one = str_replace($data2['info'][0]->setting_value,$data3['info_current'][0]->setting_value,$email_file);


                //die($email_file);
    			redirect('language', 'refresh');
    		} else {
    			echo "failed";
    		}

    	}

    	public function view($id)
    	{
            
    		$this->load->model('language_model');
            $this->load->model('data_model');

    		$data['files'] = $this->language_model->list_files($id,$this->path);
    		$data['language'] = $this->language_model->get_language($id);
    		$data['label'] = $this->language_model->get_label($id);
    		$data['id'] = $id;
            
    		$data2['info'] = $this->data_model->website_info();
            $this->load->view('header',$data2);
    		$this->load->view('specific_language_view',$data);
    		$this->load->view('footer');

    	}

    	public function file_edit()
    	{
            $this->load->model('data_model');
    		$this->load->helper('file');
    		$this->load->model('language_model');
    		$label = $this->input->get('label');
    		$filename = $this->input->get('file');

            //die($label);
             //die($filename);
            //die($this->path.$label.'/'.$filename);
    		$data['array1'] = include('../app/lang/'.$label.'/'.$filename);
    		$data['array2'] = include('../app/lang/'.$label.'/'.$filename);
    		$data['language'] = $this->language_model->get_lang($label);
    		$data['label'] = $label;
    		$data['filename'] = $filename;
    		$data2['info'] = $this->data_model->website_info();
            $this->load->view('header',$data2);
    		$this->load->view('file_edit',$data);
    		$this->load->view('footer');


    	}


    	public function suspend($id)
    	{
    		$this->load->model('language_model');
    		if($this->language_model->suspend($id))
    		{
    			redirect('language', 'refresh');
    		} else {
    			echo "failed";
    		}



    	}
    	public function unsuspend($id)
    	{
    		$this->load->model('language_model');
    		if($this->language_model->unsuspend($id))
    		{
    			redirect('language', 'refresh');
    		} else {
    			echo "failed";
    		}


    	}



    }
