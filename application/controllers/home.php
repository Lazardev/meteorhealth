<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('password_func.php');
require_once "braintree_lib/lib/Braintree.php";
class Home extends CI_Controller {

	function __construct()
    {
		
        parent::__construct();
        $this->load->database();
		$config['allowed_types'] = 'gif|jpg|jpeg|JPG|JPEG|png';
        $this->load->library('upload', $config); 
    }	
	
	/***DEFAULT NOR FOUND PAGE*****/
    function four_zero_four()
    {
        $this->load->view('four_zero_four');
    }
	
	function offline()
	{
		$this->load->view('static/underconstruction');
	}
	
	public function index()
	{
		$page_data['slideshow']   = $this->db->get('site_slideshow')->result_array();
        // $page_data['head']='header.php';
        // $page_data['foot']='footer.php';
         $page_data['page_name']  = 'home';
        $this->load->view('static/index',$page_data);	
	}
	
	public function how_it_works ()
	{
		 $page_data['page_name']  = 'howitworks';
        $this->load->view('static/index',$page_data);
	}

	public function become_jobrabbit ()
	{
		
		$page_data['page_name']  = 'become_jobrabbit';
        $this->load->view('static/index',$page_data);
	}
	
	public function page ()
	{
		$this->db->where('stpage_id',$this->input->get('page'));
        $page_data['staticpages']   =   $this->db->get('staticpages')->result_array();
        $page_data['head']='header.php';
        $page_data['foot']='footer.php';
		$page_data['page_name']  = 'page';
		$this->load->view('static/index',$page_data);
	}
	
	public function signup($param1 = '')
	{
		if ($param1 == 'user_create') {
        $data['full_name']       = $this->input->post('full_name');
		$data['emailid']       = $this->input->post('email');
		$data['password'] =_base64_encrypt($this->input->post('password'),'bytes789');
		$data['verify_email']       ='0';
		$data['user_type']       = '1';
		$data['verification_code'] = md5( rand(0,1000) );
		$data['signup_date']       = date('Y-m-d H:i:s');
		$data['zipcode']       = str_replace(' ', '', $this->input->post('zipcode'));
		$data['active_status']       = '1';
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}		
		$data['ip'] = $ip;
	
     
		$page_data   = $this->db->get('users')->result_array();
			foreach($page_data as $row){
				if($data['emailid'] == $row['emailid'])
				{
				$this->session->set_flashdata('flash_message','User Mail already Exist Try Again');
				redirect(base_url() . 'home/signup/', 'refresh');
				}
			}
		
		$this->db->trans_start();
		$this->db->insert('users', $data);
		$user_id = $this->db->insert_id();
		$this->db->query("INSERT INTO `user_profiles` (`profile_id`, `user_id`, `gender`, `dob`, `user_image`, `address`, `about_info`, `location_map`, `skills`, `profile_links`, `mobile`, `phone`, `paypal_email`)VALUES(NULL, '".$user_id."', '0', '', '', '', '', '', '', '', '', '', '') ");
		$this->db->trans_complete();
			$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";
   }
		$this->email_model->Registration($data);
		$this->session->set_flashdata('flash_message','Successfully registered, please check your Mail');
		redirect(base_url() . 'home/login/', 'refresh');
		}
		$page_data['page_name']  = 'signup';
		$page_data['page_title'] = 'Signup';
        $this->load->view('static/index',$page_data);
	}

	public function email_verify($param1 = '')
	{
		
		$status = $this->db->query("SELECT * FROM `users` WHERE verification_code = '$param1'")->row()->verify_email;
		if($status==1)
		{
			$this->session->set_flashdata('flash_message','Activation Link Expired or Already Activated try to login');
				redirect(base_url() . 'home/signup/', 'refresh');
		}
		else
		{
		$data['verify_email']       =1;
		$this->db->where('verification_code', $param1);
		 	$this->db->update('users', $data);
		 $this->session->set_flashdata('flash_message','Thanks!.. Your email has been Verified and account Activated');
		redirect(base_url() . 'home/login/', 'refresh');
		}

	}
	
	function reg_msg(){
		$page_data['page_name']  = 'reg_msg';
		$page_data['page_title'] = 'reg_msg';
        $this->load->view('static/index',$page_data);	
	}
	
	public function login ($param1 = ''){
		$page_data['page_name']  = 'login';
if($param1 == 'participant')
{
                $page_data['sign']  = 'Participant';
}
if($param1 == 'provider')
{
                $page_data['sign']  = 'Provider ';
}
		$page_data['page_title'] = 'Login';
        $this->load->view('static/index',$page_data);
	}
	public function choose_jobs ($param1){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$page_data['page_name']  = 'choose_jobs';
		$page_data['cat_id'] = $param1;
        $this->load->view('static/index',$page_data);
	}
		public function dash_poster(){
		$user_id =$this->session->userdata('user_id');
		$this->session->set_userdata('checking', '1');
		$profile = $this->db->get_where('user_profiles', array('user_id' =>$user_id))->result_array();
			
		$page_data['page_name']  = 'dash_poster';
		$page_data['page_title'] = 'Dash Poster';
        $this->load->view('static/index',$page_data);
	}
	public function jobs($param1 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =$this->session->userdata('user_id');
		 $status = $this->db->query("SELECT * FROM `jobs` WHERE `jposter_id` = $user_id")->result_array();
		$page_data['page_name']  = 'jobs';
		$page_data['page_title'] = 'Dash Poster';
		$page_data['jobs'] = $status;
        $this->load->view('static/index',$page_data);
		}
		public function jobs_task($param1 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =$this->session->userdata('user_id');
		 $status = $this->db->query("SELECT * FROM `jobs` WHERE `jposter_id` = $user_id")->result_array();
		$page_data['page_name']  = 'jobs_task';
		$page_data['page_title'] = 'Dash Tasker';
		$page_data['user_id']  = $user_id;
		$page_data['jobs'] = $status;
        $this->load->view('static/index',$page_data);
		}

	// Tasker 
	public function job_details($param1 = ''){
		
		// $user_id =$this->session->userdata('user_id');
		 $status = $this->db->query("SELECT * FROM `jobs` WHERE `job_id` = $param1")->row();
		 
       
           echo json_encode($status); 
		}
	public function profile_view($param1 = ''){
		
		$user_id =$this->session->userdata('user_id');
		$status = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = $param1")->row();
		 
		echo json_encode($status); 
		}
		
	public function dash_worker(){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =$this->session->userdata('user_id');
		 if($this->input->post('accept') !='')
		 {
		 	$data['status'] = 'agree';
		 	$this->db->where('user_id', $user_id);
		 	$this->db->update('users', $data);

		 }
		 if($this->input->post('dob') !='')
		 {
		 		
			$rand = rand(0000, 9999);
			 @$path=$_FILES['image']['name'];
			 $extension = pathinfo($path,PATHINFO_EXTENSION);
			 $filename=$rand .'.'. $extension;
			 if(!empty($path) ){
				  $moved = move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/profiles/'.$filename );
					if( $moved ) {
						$data['user_image']       = $filename;       
					} else {
						$this->session->set_flashdata('flash_message','upload Failed');
						redirect(base_url() . 'home/dash_worker', 'refresh');  
					}   
			 }
		 $data['dob'] = $this->input->post('dob');
		 $data['mobile'] = $this->input->post('contact');
		 $data['address'] = $this->input->post('address');
		 $data['skills'] = $this->input->post('skills');
		 $data['about_info'] = $this->input->post('about');
		 $data['gender'] = $this->input->post('gender');
		 $data['not_tasking'] = $this->input->post('not_tasking');
		 $data['right_person'] = $this->input->post('right');
		 $data['tasking'] = $this->input->post('tasking');
                 $data['tobacco'] = $this->input->post('tobacco');
                 $data['ethincity'] = $this->input->post('ethincity');
                 $data['veteran'] = $this->input->post('veteran');
                 $data['twin'] = $this->input->post('twin');
                 $data['feet'] = $this->input->post('feet');
                 $data['inches'] = $this->input->post('inches');
                 $data['username'] = $this->input->post('username');
                 $data['weight'] = $this->input->post('weight');
                 $data['medical_condition'] = $this->input->post('medical_condition');
                 $data['prescription'] = $this->input->post('prescription');
                 $data['travel_distance'] = $this->input->post('travel_distance');
                 $data['additional'] = $this->input->post('additional');
                 $data['studies_parent'] = $this->input->post('studies_parent');
		 $data1['zipcode'] = str_replace(' ', '', $this->input->post('zip'));
		 $data1['status'] = 'basic';
		 $this->db->where('user_id', $user_id);
		 $this->db->update('user_profiles', $data);
		 $this->db->where('user_id', $user_id);
		 $this->db->update('users', $data1);

		 }

		
		$this->session->set_userdata('checking', '1');
		$profile = $this->db->get_where('user_profiles', array('user_id' =>$user_id))->row();
		$status = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row()->status;
		$u = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$user_id'")->row();
		if($status == '')
		{
		$page_data['page_name']  = 'apply_tasker';
		//$page_data['page_title'] = 'Dash Worker';
        $this->load->view('static/index',$page_data);
		}
		else if($status == 'agree')
		{
		$page_data['page_name']  = 'apply_tasker1';
		$page_data['zip'] = $u->zipcode;
		$page_data['mobile'] = $profile->mobile;
		//$page_data['page_title'] = 'Dash Worker';
        $this->load->view('static/index',$page_data);
		}
		else if($status == 'basic')
		{
		$page_data['page_name']  = 'tasker';
		$page_data['user_id'] = $user_id;
		//$page_data['page_title'] = 'Dash Worker';
        $this->load->view('static/index',$page_data);
		}
		else
		{
		$page_data['page_name']  = 'dash_worker';
		$page_data['page_title'] = 'Dash Worker';
        $this->load->view('static/index',$page_data);
    }
	}

	public function tasker_update(){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =$this->session->userdata('user_id');
		if($this->input->post('from') !='')
		 {
		 	$from = date("Y-m-d", strtotime($this->input->post('from')) );
		 	$data['available_from'] = $from;
		 	$to = date("Y-m-d", strtotime($this->input->post('to')) );
		 	$data['available_to'] = $to;
		 	$data['availability_time'] = $this->input->post('time');
		 	$this->db->where('user_id', $user_id);
		 	$this->db->update('user_profiles', $data);

		 }
		 if($this->input->post('chk_group') !='')
		 {
		 	$categories = array();
		 	$optionArray = $this->input->post('chk_group');
		 	
    			for ($i=0; $i<count($optionArray); $i++) 
    			{
        		$categories[] = str_replace("_", " ", $optionArray[$i]); 
    			}
    			$data['selected_catagories'] = implode(",", $categories);
    			foreach ($categories as $category) {
    				$data1[str_replace(" ", "_",$category)] = $this->input->post(str_replace(" ", "_",$category));
    			}
    			$query = $this->db->query("SELECT * FROM categories WHERE user_id = $user_id");
            $rows =  $query->num_rows();

             if($rows == 0) {
     		$data1['user_id'] =  $user_id;
		  	$this->db->insert('categories', $data1);
		  }
		  else
		 {
		 	$this->db->where('user_id', $user_id);
		 	$this->db->update('categories', $data1);

		 }
		 	$this->db->where('user_id', $user_id);
		 	$this->db->update('user_profiles', $data);
		 }
		 if($this->input->post('right') !='')
		 {
		 $data['tobacco'] = $this->input->post('tobacco');
                 $data['veteran'] = $this->input->post('veteran');
                 $data['twin'] = $this->input->post('twin');
                 $data['feet'] = $this->input->post('feet');
                 $data['inches'] = $this->input->post('inches');
                 $data['username'] = $this->input->post('username');
                 $data['weight'] = $this->input->post('weight');
                 $data['medical_condition'] = $this->input->post('medical_condition');
                 $data['prescription'] = $this->input->post('prescription');
                 $data['travel_distance'] = $this->input->post('travel_distance');
                 $data['additional'] = $this->input->post('additional');
                 $data['studies_parent'] = $this->input->post('studies_parent');

		 		$this->db->where('user_id', $user_id);
		 		$this->db->update('user_profiles', $data);
		 }
		$page_data['page_name']  = 'tasker';
		$page_data['page_title'] = 'Dash Poster';
		$page_data['user_id'] = $user_id;
        $this->load->view('static/index',$page_data);
	}








	 function do_login(){
        $email = $this->input->post('email');
        $pass  = _base64_encrypt($this->input->post('password'),'bytes789');
		
        if($email=="" & $pass=="" ){
             $page_data['page_title'] = 'Login';
             $this->session->set_flashdata('flash_message','Incorrect Emailid & Password');
             $this->load->view('home/login',$page_data);
        }else {
            $query = $this->db->get_where('users', array('emailid' => $email,'password' => $pass));
            if ($query->num_rows() > 0) {
                $row = $query->row();
                if (!empty($row)) {
					if($row->active_status==1 && $row->verify_email==1){
                    $this->session->set_userdata('user_login', '1');
                    $this->session->set_userdata('user_id', $row->user_id);
                    $this->session->set_userdata('full_name', $row->full_name);
                    $this->session->set_userdata('user_type', $row->user_type);
					$this->session->set_flashdata('flash_message','Welcome');
                    redirect( base_url() . 'home/dashboard', 'refresh');  
					}else {
					$this->session->set_flashdata('flash_message','Login Failed, Please Activate your mail');
                    redirect(base_url() . 'home/login', 'refresh');
					}
                 }
            }else{
                    $this->session->set_flashdata('flash_message','Login Failed');
                    redirect(base_url() . 'home/login', 'refresh');
                 }    
        }
    }	
	public function facebook(){
			require 'facebook/facebook.php';
			$appId = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_apikey'")->row()->description;
			$secret = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_applicationkey'")->row()->description;
			$facebook_profile = $this->db->query("SELECT * FROM `sitesettings` WHERE `type` = 'facebook_profile'")->row()->description;
			$facebook = new Facebook(array(
			  'appId'  => $appId,
			  'secret' => $secret,
			));
			$user = $facebook->getUser();
			if ($user) {
			  try {
				$user_profile = $facebook->api('/me?fields=id,name,email');
			  } catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			  }
			}
			$email = $user_profile['email'];
			if($email=='')
			{
				$this->session->set_flashdata('flash_message','Facebook Login Unsuccessful');
				redirect( base_url() . 'home/login', 'refresh');
			}
			$query = $this->db->get_where('users', array('emailid' => $email));
            if ($query->num_rows() > 0) {
                $row = $query->row();
                if (!empty($row)) {
					if($row->active_status==1){
                    $this->session->set_userdata('user_login', '1');
                    $this->session->set_userdata('user_id', $row->user_id);
                    $this->session->set_userdata('full_name', $row->full_name);
                    $this->session->set_userdata('user_type', $row->user_type);
					$this->session->set_flashdata('flash_message','Welcome');
                    redirect( base_url() . 'home/dashboard', 'refresh');  
					}
                 }
			 }else{
				$this->db->trans_start();
				$data['full_name']       = $user_profile['name'];
				$data['emailid']         = $email;
				$rand = rand(0000,9999);
				$data['password'] =_base64_encrypt($rand,'bytes789');
				$data['verify_email']    = '1';
				$data['user_type']       = '0';
				$data['signup_date']     = date('Y-m-d H:i:s');
				$data['active_status']   = '1';
				$this->db->insert('users', $data);
				$user_id = $this->db->insert_id();
				$this->db->query("INSERT INTO `user_profiles` (`profile_id`, `user_id`, `gender`, `dob`, `user_image`, `address`, `about_info`, `location_map`, `skills`, `profile_links`, `mobile`, `phone`)VALUES(NULL, '".$user_id."', '0', '', '', '', '', '', '', '', '', '') ");
				$this->db->trans_complete(); 
				$this->session->set_userdata('user_login', '1');
                $this->session->set_userdata('user_id', $user_id);
				$this->session->set_userdata('full_name', $data['full_name']);
				$this->session->set_userdata('user_type', '0');
				$this->session->set_flashdata('flash_message','Welcome');
                redirect( base_url() . 'home/dashboard', 'refresh'); 
			 }   
	}
	
	function logout(){
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        $this->session->set_flashdata('flash_message','Successfully Logout');
        redirect( base_url() . '/', 'refresh');
    }
	
	public function dashboard($param1 = '',$param2 = ''){
		if ($this->session->userdata('user_login') != 1){
        redirect(base_url() . 'home/login', 'refresh');
		}
		$type =$this->session->userdata('user_type');
			
			$user_id =$this->session->userdata('user_id');
			$this->session->set_userdata('checking', '1');
			$profile = $this->db->get_where('user_profiles', array('user_id' =>$user_id))->result_array();
			/*foreach ($profile as $prof) {
				//$prof['user_image']==""
				if ($prof['gender']=="" OR $prof['dob']=="" OR $prof['about_info']=="" OR $prof['skills']==""){
					$this->session->set_flashdata('flash_message','Please Update Your profile');
					$this->session->set_userdata('checking', '0');
					redirect(base_url() . 'home/profile', 'refresh');
				}
				
				if ($prof['address']=="" OR $prof['mobile']==""){
					$this->session->set_flashdata('flash_message','Please fill Your Information');
					$this->session->set_userdata('checking', '0');
				redirect(base_url() . 'home/account', 'refresh');
				
			}
			
			if( $prof['paypal_email']==""){
				$this->session->set_flashdata('flash_message','Add Your paypal Account');
				$this->session->set_userdata('checking', '0');
				redirect(base_url() . 'home/wpayment_settings', 'refresh');
			}
				
			}*/			
			//$page_data['head']='dash_header.php';
			//$page_data['foot']='dash_footer.php';
			if($type=='1'){	redirect(base_url() . 'home/dash_worker', 'refresh'); }
			if($type=='2'){	redirect(base_url() . 'home/dash_poster', 'refresh'); }
			
			
		
			
	}
	
	function choose_type ($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		if ($param1 == 'update_user') {
			$data['user_type']       = $param2;
			$user_id =$this->session->userdata('user_id');
			$this->db->where('user_id', $user_id);
			$this->db->update('users', $data);
			$this->session->set_userdata('user_type', $data['user_type']);
			$this->session->set_flashdata('flash_message','Successfully Updated');
			redirect(base_url() . 'home/dashboard', 'refresh');
		}
	
	}

	public function search()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');

	    	$title = $this->input->post('search');

		    $searchTerms = explode(' ', $title);


		    foreach($searchTerms as $term)
		    {
		    	$query = $this->db->query("SELECT * FROM `jobs` WHERE `jaddress` like '%".$term."%' AND `jwork_status` = 1 AND `jwork_status` != 2");
		    }

		  $page_data['recent_jobs'] = $query->result_array();
		  $page_data['head']='dash_header.php';
		  $page_data['foot']='dash_footer.php';
		  $page_data['page_name']  = 'wcategory_jobs';
		  $this->load->view('static/index',$page_data);
	}

	public function view_map()
	{
	  if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');

	  $page_data['job_view']   = $this->db->get_where('jobs', array('jwork_status' => 1,'jwork_status !=' => 2))->result_array();
	 // $page_data['job_view'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
	 // var_dump($page_data['job_view']);
	  // $page_data['head']='dash_header.php';
	  // $page_data['foot']='dash_footer.php';
	  $page_data['page_name']  = 'view_map';
	  $this->load->view('static/index',$page_data);
	}


	
	public function forgot_password()
	{
		$data['email']       = $this->input->post('email');
		$query = $this->db->get_where('users', array('emailid' => $data['email']));
					if ($query->num_rows() > 0) {
		$output = $this->db->query("SELECT * FROM `users` WHERE `emailid`='".$data['email']."'")->row()->password;
		$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";
   }
		$data['password'] =_base64_decrypt($output,'bytes789');
		$this->email_model->forgot_password($data);
		
		$this->session->set_flashdata('flash_message','Please check your mail');
		redirect(base_url() . 'home/login', 'refresh');
		}else{
		$this->session->set_flashdata('flash_message','Error : Please provide correct email ID');
		redirect(base_url() . 'home/login', 'refresh');
		}
    }
	
	public function profile($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		if ($param1 == 'update') {
			$user_imagetxt       = $this->input->post('user_imagetxt');
        $data['gender']       = $this->input->post('gender');
		$data['dob']       = $this->input->post('dob');
		$data['about_info']       = $this->input->post('about_info');
		$data['location_map']       = $this->input->post('location_map');
		//$data['user_image']       = $this->input->post('user_image');
		$data['skills']       = $this->input->post('skills');
		$data['profile_links']       = $this->input->post('face').'*'.$this->input->post('twitter');
		$path=@$_FILES['user_image']['name'];
			if(!empty($path)){
				$extension = end(explode('.', $_FILES['user_image']['name']));
				$rand = mt_rand(00000,99999);
				$filename =$rand.".".$extension;
	$config['image_library'] = 'gd2';
	$config['source_image'] = $_FILES['user_image']['tmp_name'];
	$config['new_image'] = 'uploads/profiles/'.$filename;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = FALSE;
	$config['width'] = 200;
	$config['height'] = 200;
	// Load the Library
	$this->load->library('image_lib', $config);
	
	// resize image
	$this->image_lib->resize();
	// handle if there is any problem
	if ( ! $this->image_lib->resize()){
		$this->session->set_flashdata('flash_message','upload Failed');
						redirect(base_url() . 'home/profile/', 'refresh');  
	}else{
		$user_id =  $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$this->db->update('user_profiles', array('user_image' => $filename  )); 
		$file = "uploads/profiles/".$user_imagetxt; // get all file names
      if(is_file($file))
        unlink($file); // delete file  
	}
				/*$moved = move_uploaded_file($_FILES['user_image']['tmp_name'], 'uploads/profiles/'.$filename );
				if( $moved ) {
					$user_id =  $this->session->userdata('user_id');
						$this->db->where('user_id', $user_id);
						$this->db->update('user_profiles', array('user_image' => $filename  ));    
					} else {
						$this->session->set_flashdata('flash_message','upload Failed');
						redirect(base_url() . 'home/profile/', 'refresh');  
					}*/
			}
		
		//print_r($data);
		$this->db->from('job_category');
		$cat_count = $this->db->count_all_results();
		$ck ="";
		for($i=0;$i<=$cat_count;$i++){
			$sa= $this->input->post('ck-'.$i);
			if(!empty($sa))
			{
			$ck = $ck . $this->input->post('ck-'.$i).','; 	
			}
		}
		$data['selected_catagories']       =$ck;
		$this->db->where('user_id', $param2);
        $this->db->update('user_profiles', $data);
		 $this->session->set_flashdata('flash_message','Updated Successfully');
		  redirect(base_url() . 'home/profile/', 'refresh');
		}
		
		$user_id =  $this->session->userdata('user_id');
		$page_data['edit_data'] = $this->db->get_where('user_profiles', array(
                'user_id' => $user_id
            ))->result_array();
	
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'profile';
        $this->load->view('static/index',$page_data);
	}

	function account($param1 = '', $param2 = '') {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if ($param1 == 'update') {
			$rand = rand(0000, 9999);
			 @$path=$_FILES['image']['name'];
			 $extension = pathinfo($path,PATHINFO_EXTENSION);
			 $filename=$rand .'.'. $extension;
			 if(!empty($path) ){
				  $moved = move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/profiles/'.$filename );
					if( $moved ) {
						$data['user_image']       = $filename;       
					} else {
						$this->session->set_flashdata('flash_message','upload Failed');
						redirect(base_url() . 'home/dash_poster', 'refresh');  
					}   
			 }
			$dat['full_name'] = $this->input->post('fullname');
			$dat['emailid'] = $this->input->post('email');
			$dat['zipcode'] = str_replace(' ', '', $this->input->post('zip'));
			$data['address'] = $this->input->post('address');
			$data['dob'] = $this->input->post('dob');
			$data['phone'] = $this->input->post('phone');
				$this->db->where('user_id', $param2);
				$this->db->update('users', $dat);
				$this->db->where('user_id', $param2);
				$this->db->update('user_profiles', $data);
				 $this->session->set_flashdata('flash_message','Updated Successfully');
				redirect(base_url() . 'home/account/', 'refresh');
		}
		if ($param1 == 'password') {

			$old_pass 		=_base64_encrypt($this->input->post('old_pwd'),'bytes789');
			$new_pass_en	=_base64_encrypt($this->input->post('new_pwd'),'bytes789');
			$new_pass 		= $this->input->post('new_pwd');
			
				$output = $this->db->query("SELECT * FROM `users` WHERE `user_id`=".$param2)->row()->password;
				if($output == $old_pass){
					if($output != $new_pass_en)
					{
						$data['password']       = $new_pass_en;
						$this->db->where('user_id', $param2);
						$this->db->update('users', $data);
						$this->session->set_flashdata('flash_message','Password Changed!');
						redirect(base_url() . 'home/account/', 'refresh');
					}else{
						$this->session->set_flashdata('flash_message','Error : Same as the old password!');
						redirect(base_url() . 'home/account/', 'refresh');
					}
				}else{
					$this->session->set_flashdata('flash_message','Error : Password Mismatch!');
					redirect(base_url() . 'home/account/', 'refresh');
				}
			
				 $this->session->set_flashdata('flash_message','Updated Successfully');
				redirect(base_url() . 'home/account/', 'refresh');
		}
		if ($param1 == 'paypal') {

					
						$data['paypal_email']   =  $this->input->post('paypal_email');
						$this->db->where('user_id', $param2);
						$this->db->update('user_profiles', $data);
						$this->session->set_flashdata('flash_message','Updated Successfully!');
						redirect(base_url() . 'home/account/', 'refresh');
			}
		if ($param1 == 'promo') {
			$credits = $this->db->query("SELECT * FROM `users` WHERE `user_id`=".$param2)->row()->credits;
				
						$promo_code = $this->db->query("SELECT * FROM sitesettings WHERE type='promo_code'")->row()->description;
						$promo_price = $this->db->query("SELECT * FROM sitesettings WHERE type='promo_price'")->row()->description;
						$promo_status = $this->db->query("SELECT * FROM users WHERE user_id=$param2")->row()->promo;
						if($promo_code == $this->input->post('promo_code') && $promo_status == 0){
							$data['credits'] = $credits + $promo_price;
							$data['promo'] = 1;
							$this->db->where('user_id', $param2);
						$this->db->update('users', $data);
						$this->session->set_flashdata('flash_message','$15 added to your Credits!');
						redirect(base_url() . 'home/account/', 'refresh');
					}
						else
						{
						$this->session->set_flashdata('flash_message','Wrong Promocode or Already applied!');
						redirect(base_url() . 'home/account/', 'refresh');
						}
			}
			


    	
		$page_data['page_name']  = 'account';
        $this->load->view('static/index',$page_data);
    }
	

	function account_task($param1 = '', $param2 = '') {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if ($param1 == 'update') {
			$rand = rand(0000, 9999);
			 @$path=$_FILES['image']['name'];
			 $extension = pathinfo($path,PATHINFO_EXTENSION);
			 $filename=$rand .'.'. $extension;
			 if(!empty($path) ){
				  $moved = move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/profiles/'.$filename );
					if( $moved ) {
						$data['user_image']       = $filename;       
					} else {
						$this->session->set_flashdata('flash_message','upload Failed');
						redirect(base_url() . 'home/dash_worker', 'refresh');  
					}   
			 }
			$dat['full_name'] = $this->input->post('fullname');
			$dat['emailid'] = $this->input->post('email');
			$dat['zipcode'] = str_replace(' ', '', $this->input->post('zip'));
			$data['address'] = $this->input->post('address');
			$data['skills'] = $this->input->post('skills');
			$data['dob'] = $this->input->post('dob');
			$data['phone'] = $this->input->post('phone');
				$this->db->where('user_id', $param2);
				$this->db->update('users', $dat);
				$this->db->where('user_id', $param2);
				$this->db->update('user_profiles', $data);
				 $this->session->set_flashdata('flash_message','Account Updated Successfully');
				redirect(base_url() . 'home/account_task/', 'refresh');
		}
		if ($param1 == 'password') {

			$old_pass 		=_base64_encrypt($this->input->post('old_pwd'),'bytes789');
			$new_pass_en	=_base64_encrypt($this->input->post('new_pwd'),'bytes789');
			$new_pass 		= $this->input->post('new_pwd');
			
				$output = $this->db->query("SELECT * FROM `users` WHERE `user_id`=".$param2)->row()->password;
				if($output == $old_pass){
					if($output != $new_pass_en)
					{
						$data['password']       = $new_pass_en;
						$this->db->where('user_id', $param2);
						$this->db->update('users', $data);
						$this->session->set_flashdata('flash_message','Password Changed!');
						redirect(base_url() . 'home/account_task/', 'refresh');
					}else{
						$this->session->set_flashdata('flash_message','Error : Same as the old password!');
						redirect(base_url() . 'home/account_task/', 'refresh');
					}
				}else{
					$this->session->set_flashdata('flash_message','Error : Password Mismatch!');
					redirect(base_url() . 'home/account_task/', 'refresh');
				}
			
				 $this->session->set_flashdata('flash_message','Updated Successfully');
				redirect(base_url() . 'home/account_task/', 'refresh');
		}
		if ($param1 == 'payment') {

					
			$data['bt_merchant_id'] 				= $this->input->post('bt_merchant_id');
			$data['bt_public_key'] 				= $this->input->post('bt_public_key');
			$data['bt_private_key'] 				= $this->input->post('bt_private_key');
			$data['bt_cse'] 				= $this->input->post('bt_cse');
			$data['bt_envi'] 				= $this->input->post('bt_envi');
			$this->db->where('user_id', $param2);
        $this->db->update('users', $data);
						$this->session->set_flashdata('flash_message','Updated Successfully!');
						redirect(base_url() . 'home/account_task/', 'refresh');
			}
		


    	
		$page_data['page_name']  = 'account_task';
        $this->load->view('static/index',$page_data);
    }


	function changepassword($param1 = '', $param2 = '') {
			if ($this->session->userdata('user_login') != 1)
       		 redirect(base_url() . 'home/login', 'refresh');
			 
		if ($param1 == 'update_pass') {
			$old_pass 		=_base64_encrypt($this->input->post('old_pass'),'bytes789');
			$new_pass_en	=_base64_encrypt($this->input->post('new_pass'),'bytes789');
			$new_pass 		= $this->input->post('new_pass');
			$confirm_pass  	= $this->input->post('confirm_pass');
			if($new_pass == $confirm_pass){
				$output = $this->db->query("SELECT * FROM `users` WHERE `user_id`=".$param2)->row()->password;
				if($output == $old_pass){
					if($output != $new_pass_en)
					{
						$data['password']       = $new_pass_en;
						$this->db->where('user_id', $param2);
						$this->db->update('users', $data);
						$this->session->set_flashdata('flash_message','Password Changed!');
						redirect(base_url() . 'home/changepassword/', 'refresh');
					}else{
						$this->session->set_flashdata('flash_message','Error : Password Mismatch!');
						redirect(base_url() . 'home/changepassword/', 'refresh');
					}
				}else{
					$this->session->set_flashdata('flash_message','Error : Password Mismatch!');
					redirect(base_url() . 'home/changepassword/', 'refresh');
				}
			}else{
				$this->session->set_flashdata('flash_message','Error : Password Mismatch!');
				redirect(base_url() . 'home/changepassword/', 'refresh');
			}
		}
		
    	$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'changepassword';
        $this->load->view('static/index',$page_data);
    }
	
	function wpayment_settings($param1 = '', $param2 = '') {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if ($param1 == 'update') {
			$data['user_id']				= $param2;
			$data['bt_merchant_id'] 				= $this->input->post('bt_merchant_id');
			$data['bt_public_key'] 				= $this->input->post('bt_public_key');
			$data['bt_private_key'] 				= $this->input->post('bt_private_key');
			$data['bt_cse'] 				= $this->input->post('bt_cse');
			$data['bt_envi'] 				= $this->input->post('bt_envi');
			$this->db->where('user_id', $param2);
        $this->db->update('users', $data);
		 $this->session->set_flashdata('flash_message','Updated Successfully');
		redirect(base_url() . 'home/wpayment_settings/', 'refresh');
		}
		$user_id =  $this->session->userdata('user_id');
		$page_data['edit_data'] = $this->db->get_where('user_profiles', array(
                'user_id' => $user_id
            ))->result_array();
    	$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wpayment_settings';
        $this->load->view('static/index',$page_data);
    }
	
	function confirm_deactivation($param1 = '', $param2 = '') {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if ($param1 == 'insert') {
			$data['user_id']				= $param2;
			$data['reason'] 				= $this->input->post('reason');
			$data['deactivate_date_time'] 	= date('Y-m-d H:i:s');
			
			$this->db->trans_start();
				$this->db->insert('deactivate_report', $data);
				$dat['active_status'] 	= '2';
				$this->db->where('user_id', $param2);
				$this->db->update('users', $dat);
			$this->db->trans_complete();
			
			$this->session->unset_userdata();
        	$this->session->sess_destroy();
			$this->session->set_flashdata('flash_message','Deactivated Successfully');
			redirect(base_url(), 'refresh');
		}
		
    	
		$page_data['page_name']  = 'account';
        $this->load->view('static/index',$page_data);
    }
	
	/***** Transaction *****/
	function transaction() 
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$this->db->order_by("invoice_id", "desc");
		$page_data['recent_invoice'] = $this->db->get_where('invoice', array('user_id' => $user_id))->result_array();
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'invoice';
        $this->load->view('static/index',$page_data);
	}
	
	 /******MANAGE BILLING / INVOICES WITH STATUS*****/
    function invoice($param1 = '', $param2 = '', $param3 = '')
    {
session_start();
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		        if ($param1 == 'make_payment') {
            $invoice_id      = rand(00000,99999);
            //$system_settings = 'sushanth2005-facilitator@gmail.com';
			$system_settings = $this->db->query("SELECT * FROM sitesettings WHERE type='paypal_emailid'")->row()->description;
           
            /****TRANSFERRING USER TO PAYPAL TERMINAL****/
            $this->paypal->add_field('rm', 2);
            $this->paypal->add_field('no_note', 0);
            $this->paypal->add_field('item_name', $param2);
            $this->paypal->add_field('amount',$param3);
            $this->paypal->add_field('custom', '134');
			$this->paypal->add_field('currency_code', 'USD');
			$user_id =  $this->session->userdata('user_id');
			$output = $this->db->query("SELECT * FROM user_profiles WHERE user_id=".$user_id)->row()->paypal_email;		
			$this->paypal->add_field('login_email',$output);		
            $this->paypal->add_field('business', $system_settings);
            $this->paypal->add_field('notify_url', base_url() . 'home/invoice/paypal_ipn');
            $this->paypal->add_field('cancel_return', base_url() . 'home/invoice/paypal_cancel');
            $this->paypal->add_field('return', base_url() . 'home/invoice/paypal_success');
            $this->paypal->submit_paypal_post();
            // submit the fields to paypal
        }
        if ($param1 == 'paypal_ipn') {
            if ($this->paypal->validate_ipn() == true) {
                $ipn_response = '';
                foreach ($_POST as $key => $value) {
                    $value = urlencode(stripslashes($value));
                    $ipn_response .= "\n$key=$value";
                }
             /*   $data['payment_details']   = $ipn_response;
                $data['payment_timestamp'] = strtotime(date("m/d/Y"));
                $data['payment_method']    = 'paypal';
                $data['status']            = 'paid';
                $invoice_id                = $_POST['custom'];
                $this->db->where('invoice_id', $invoice_id);
                $this->db->update('invoice', $data);
				$this->db->trans_start();
				$sample[] = $this->session->userdata('temp_job');
				$this->db->insert('jobs', $sample);
				$job_id = $this->db->insert_id();
				$data['job_id']   = '$job_id';
                $data['user_id']   = $this->session->userdata('user_id');				
				$data['job_name']   =$sample['jname'];
				$data['transaction_id']   = $_POST['txn_id'];
				$data['status']            = $_POST['payment_status'];
				$data['amount']   = $_POST['mc_gross'];
                $data['payment_timestamp'] = date("Y-m-d");
                $data['payment_method']    = 'paypal';
				$this->db->insert('invoice', $data);
				$this->db->trans_complete(); */
            }
        }
        if ($param1 == 'paypal_cancel') {
			//$this->session->set_flashdata('flash_message', 'Payment Cancelled');
            redirect(base_url() . 'home/cancel', 'refresh');
        }
        if ($param1 == 'paypal_success') {	
        if(isset($_SESSION['subscription']))
        {
        	$this->db->trans_start();

			// $job_temp=$this->session->userdata('temp_job');
			//$data['job_id']=$job_temp['job_id'];
			$data['user_id']   =  $this->session->userdata('user_id');				
			$data['job_name']   ='subscription';	
			$data['transaction_id']   = $_POST['txn_id'];
			$data['status']            = $_POST['payment_status'];
			$data['amount']   = $_POST['mc_gross'];
			$data['payment_timestamp'] = date("Y-m-d");
			$data['payment_method']    = 'paypal';
			$this->db->insert('invoice', $data);
			 $data1['subscription']       = $_POST['mc_gross'];
			 $data1['subscription_date']       = date('Y-m-d');
			 $months = $this->db->query("SELECT * FROM sitesettings WHERE type='subscription_period'")->row()->description;
			 $data['post'] = $this->db->query("SELECT * FROM sitesettings WHERE type='no_of_post'")->row()->description;
			 $data1['subscription_expiry'] = strtotime("+".$months." months", strtotime($data1['subscription_date']));
             $data1['subscription_expiry'] = strftime ( '%Y-%m-%d' , $data1['subscription_expiry'] );
             $data1['subscription']       = $_POST['mc_gross'];
             $data1['jobs_pending'] = $data['post'];
			 $data['subscription_date']       = date('Y-m-d');
			 $months = $this->db->query("SELECT * FROM sitesettings WHERE type='subscription_period'")->row()->description;
			 $data['subscription_expiry'] = strtotime("+".$months." months", strtotime($data['subscription_date']));
             $data['subscription_expiry'] = strftime ( '%Y-%m-%d' , $data['subscription_expiry'] );

			// $data1['jwork_status']       = '4';
			// $data1['job_pay']       = '1';			
			 $this->db->where('user_id', $data['user_id']);
			 $this->db->update('users', $data1);
			 $this->db->trans_complete();
			 $data['email']   = $this->db->query("SELECT * FROM users WHERE user_id=".$data['user_id'])->row()->emailid;
			 $site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
			$this->email_model->paypal_success1($data);
            $this->session->set_flashdata('flash_message', 'Payment Successfully');
			     
            redirect(base_url() . 'home/success', 'refresh');

        }
        else if(isset($_SESSION['credit_amount'])) {
        	$user_id = $this->session->userdata('user_id');
        	$promo_status = $this->db->query("SELECT * FROM users WHERE user_id=$user_id")->row()->credits;
        		$data1['credits'] = $_SESSION['credit_amount'] + $promo_status;
        		$this->db->where('user_id', $user_id);
				$this->db->update('users', $data1);
				$data['user_id']   =  $this->session->userdata('user_id');				
			$data['job_name']   ='User Credit Balance';	
			$data['transaction_id']   = $_POST['txn_id'];
			$data['status']            = $_POST['payment_status'];
			$data['amount']   = $_POST['mc_gross'];
			
			$data['payment_timestamp'] = date("Y-m-d");
			$data['payment_method']    = 'paypal';
			$this->db->insert('invoice', $data);
			$data['total'] = $data1['credits'];
				$this->session->set_flashdata('flash_message', 'Payment Successfully');
			    $data['email']   = $this->db->query("SELECT * FROM users WHERE user_id=".$data['user_id'])->row()->emailid;
			    $site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
			$this->email_model->paypal_success2($data);
            redirect(base_url() . 'home/success', 'refresh');
        	}	
        else
        {
			$this->db->trans_start();
			$job_temp=$this->session->userdata('temp_job');
			$data['job_id']=$job_temp['job_id'];
			$data['user_id']   = $this->db->query("SELECT * FROM jobs WHERE job_id=".$data['job_id'])->row()->jposter_id;				
			$data['job_name']   =$job_temp['job_name'];	
			$data['transaction_id']   = $_POST['txn_id'];
			$data['status']            = $_POST['payment_status'];
			$data['amount']   = $_POST['mc_gross'];
			$data['payment_timestamp'] = date("Y-m-d");
			$data['payment_method']    = 'paypal';
			$this->db->insert('invoice', $data);
			$data1['jaccept_amt']       = $_POST['mc_gross'];
			$data1['jconformed_date']       = date('Y-m-d');
			$data1['jwork_status']       = '4';
			$data1['job_pay']       = '1';			
			$this->db->where('job_id', $data['job_id']);
			$this->db->update('jobs', $data1);
			$this->db->trans_complete();
			$data['email']   = $this->db->query("SELECT * FROM users WHERE user_id=".$data['user_id'])->row()->emailid;
			$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
			$this->email_model->paypal_success($data);
           // $this->session->set_flashdata('flash_message', 'Payment Successfully');
            redirect(base_url() . 'home/success', 'refresh');
        }
    }
		
		$page_data['head']='dash_header.php';
		$page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pay_disp';
		$this->load->view('static/index',$page_data);
		
    }
    public function sub_credits( $param1, $param2)
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    $this->db->trans_start();

    $job_amount	= $param2;
		$credits = $this->db->query("SELECT * FROM users WHERE user_id=".$param1)->row()->credits;
		if($credits<$job_amount)
		{
			$this->session->set_flashdata('flash_message','You do not have enough balance to do this transaction');
		redirect(base_url() . 'home/dash_poster', 'refresh');	
		}	
			$count = rand(000000, 999999);
			$data['user_id']   =  $param1;				
			$data['job_name']   ="Subscription";	
			$data['transaction_id']   = 'crd'.$count;
			$data['status']            = 'Success';
			$data['amount']   = $job_amount;
			$data['payment_timestamp'] = date("Y-m-d");
			$data['payment_method']    = 'credits';
			$this->db->insert('invoice', $data);
			$data1['subscription']       = $job_amount;
			 $data1['subscription_date']       = date('Y-m-d');
			 $months = $this->db->query("SELECT * FROM sitesettings WHERE type='subscription_period'")->row()->description;
			 $data['post'] = $this->db->query("SELECT * FROM sitesettings WHERE type='no_of_post'")->row()->description;
			 $data1['subscription_expiry'] = strtotime("+".$months." months", strtotime($data1['subscription_date']));
             $data1['subscription_expiry'] = strftime ( '%Y-%m-%d' , $data1['subscription_expiry'] );
             $data1['subscription']       = $job_amount;
             $data1['jobs_pending'] = $data['post'];
			 $data['subscription_date']       = date('Y-m-d');
			 $months = $this->db->query("SELECT * FROM sitesettings WHERE type='subscription_period'")->row()->description;
			 $data['subscription_expiry'] = strtotime("+".$months." months", strtotime($data['subscription_date']));
             $data['subscription_expiry'] = strftime ( '%Y-%m-%d' , $data['subscription_expiry'] );

		$data1['credits'] = $credits - $job_amount;
		$this->db->where('user_id', $data['user_id']);
		$this->db->update('users', $data1);
		$this->db->trans_complete();
		$data['email']   = $this->db->query("SELECT * FROM users WHERE user_id=".$data['user_id'])->row()->emailid;
		$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
		$this->email_model->paypal_success1($data);
        	
		$this->session->set_flashdata('flash_message','Payment Successful for subscription');
		redirect(base_url() . 'home/jobs', 'refresh');	





	}
		function success(){
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pay_success';
        $this->load->view('static/index',$page_data);
		}
		function steps(){
			if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'steps';
        $this->load->view('static/index',$page_data);
		}
		function cancel(){
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pay_cancel';
        $this->load->view('static/index',$page_data);
		}
		
		 function get_category()
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
        $job_type = $_REQUEST['job_type'];  
        $result = $this->crud_model->get_category($job_type);        
        echo  '<select name="category" id="category" class="form-control" required="required">';
        echo  '<option value="0">-Select One-</option>';
        foreach($result as $key=>$val)
        {                
            echo '<option value="'.$key.'">'.$val.'</option>';
        }
        echo '</select>';
    }
	
	 function get_subcategory()
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
        $category_parent_id = $_REQUEST['category_parent_id'];  
        $result = $this->crud_model->get_subcategory($category_parent_id);        
        echo  '<select name="sub_categroy" class="form-control" required="required">';
        echo  '<option value="0">-Select One-</option>';
        foreach($result as $key=>$val)
        {                
               echo '<option value="'.$key.'">'.$val.'</option>';
        }
        echo '</select>';
	}
	
	function post_a_job($param1 = '')
	 {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		if ($param1 == 'job_create') {
			$sa= $this->input->post('sub_categroy');
				if(empty($sa)){
					 $data['category_id']       = $this->input->post('category');
				}else {
					 $data['category_id']       = $this->input->post('sub_categroy');
				}
			   $data['jposter_id']=$this->session->userdata('user_id');
			   $data['jposted_date'] =date('Y-m-d');
			   $data['jname'] = $this->input->post('jname');
			   $data['jdescription'] =$this->input->post('jdescription');
			   $data['jact_status']       = '1';			
			   $data['jbilling_by']       = $this->input->post('jbilling_by');
			   $data['jbilling_no_hours']       = $this->input->post('jbilling_no_hours');
			   $data['jbilling_no_days'] =$this->input->post('jbilling_no_days');
			   $price="";
			   if($this->input->post('jbilling_by')==1) {
				    $data['jposter_price']       = $this->input->post('hour_price');
				    $price= $this->input->post('hour_price')*$this->input->post('jbilling_no_hours') ;
			   }elseif($this->input->post('jbilling_by')==2){
			   		$data['jposter_price']       = $this->input->post('day_price');
					$price=$this->input->post('day_price')*$this->input->post('jbilling_no_days');
			   }
			  $data['jbidding_type']    = $this->input->post('jbidding_type');
			  $data['jbidding_st_date'] =$this->input->post('jbidding_st_date');
			  $data['jbidding_ed_date'] = $this->input->post('jbidding_ed_date');
			  $data['jassign_to_work_id'] = $this->input->post('jassign_to_work_id');
			  if($this->input->post('jbidding_type')=='2'){
			  	$data['jassign_date']      = date('Y-m-d');
			  	$data['jassign_amt']       = $price;
			  	$data['jwork_status']     = '2';	
			  }else {
			  	$data['jassign_date']      = '0000-00-00';
			  	$data['jassign_amt']       = '0';  
			  	$data['jwork_status']      = '1';	
			  }
			  $data['jaccept_workid']      = '0';
			  $data['jaccept_date' ]       = '0000-00-00';
			  $data['jcancel_date']       =  '0000-00-00';
			  $data['jcancel_msg']       =  '';
			  $data['jaccept_amt']         = '0';
			  $data['jconformed_date']      = '0000-00-00';
			  $data['jcomplete_date']      = '0000-00-00';
			  $data['jcomplete_amt']       ='0';
			  $data['jfeedback']           = '';  
			  $data['jposter_fav']         = '0';
			  $data['jworker_fav']         = '0';
			  $data['jreject_date']       =  '0000-00-00';
			  $data['jreject_msg']       =  '';
			  $data['jstar_rate']       =  '0';
			  $data['job_pay']       =  '0';
			  $data['jlongitude']   = $this->input->post('lon');
			  $data['jlatitude']   = $this->input->post('lat');
			  $addres = $this->input->post('address') .', '. $this->input->post('zip');
			  $data['jaddress']  = $addres;
			  $data['jzipcode'] = $this->input->post('zip');
			  $this->db->insert('jobs', $data);
			  $zip = str_replace(' ', '', $this->input->post('zip'));

			  $zipDB = $this->db->query("SELECT *FROM `users` WHERE `zipcode` =".$zip." AND `user_type` = 1")->result_array();
			  foreach ($zipDB as $zips) 
			  {
			  	$this->email_model->notification($zips);
			  }

			  $this->session->set_flashdata('flash_message','job posted Successfully');
				redirect(base_url() . 'home/dashboard', 'refresh');
			
		}
		$user_id =  $this->session->userdata('user_id');
		$page_data['users']   = $this->db->get_where('jobs', array('jposter_fav' => 1,'jposter_id' => $user_id))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'post_a_job';
        $this->load->view('static/index',$page_data);
	 }	
	 
	 public function job_update ($param1 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$job_id      = $param1;
		$data['jdescription']       = $this->input->post('jdescription');
		$data['jname']       = $this->input->post('jname');
		if($this->input->post('type') == 'Hourly')
		{
			$data['jbilling_by']       = 1;
			$data['jbilling_no_hours']       = $this->input->post('duration');
		}
		else
		{
			$data['jbilling_by']       = 2;
			$data['jbilling_no_hours']       = $this->input->post('duration');
		}
		$data['jposted_date']       = $this->input->post('jposted_date');
		$this->db->where('job_id', $job_id);
		$this->db->update('jobs', $data);
		$this->session->set_flashdata('flash_message','Successfully Updated');
			redirect(base_url() . 'home/jobs', 'refresh');
	
	}
	function reassign($param1 = '') {
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$data['jwork_status']       = '1';
		$data['jassign_to_work_id']       = '0';
		$data['jassign_date']       = '0000-00-00';
		$data['jaccept_amt']       = '0';
		$data['jassign_to_work_id']       = '0';
		$data['jassign_amt']       = '0';
		$data['jaccept_workid']       = '0';
		$data['jaccept_date']       = '0000-00-00';
	$this->db->where('job_id', $param1);
		$this->db->update('jobs', $data);	
		$this->session->set_flashdata('flash_message','Job Reassigned');
			redirect(base_url() . 'home/jobs', 'refresh');
	}
		 
	 function openbids()
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$this->db->order_by("job_id", "desc");
		$this->db->where('jwork_status !=', '5');
		$this->db->where('jwork_status !=', '6'); 
		$page_data['recent_jobs']   = $this->db->get_where('jobs', array('jbidding_type' => 1,'jposter_id' => $user_id))->result_array();
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'popenbids';
        $this->load->view('static/index',$page_data);
    }
	
	function pjob_bids($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$page_data['job_view'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['job_views'] = $this->db->get_where('job_bids', array('job_id' => $param1))->result_array();	
		$page_data['page_name']  = 'pjob_bids';
        $this->load->view('static/index',$page_data);
	}
	function pjob_view($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$page_data['job_view'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
        $page_data['user_id'] = $param2;
        if($param2 != '')
        {
        	$page_data['task']='tasker';
        }
		$page_data['page_name']  = 'pjob_view';
        $this->load->view('static/index',$page_data);
	}
	
	function pjob_assign($param1 = '',$param2 = '',$param3 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		//$job_name = $this->db->query("SELECT * FROM jobs WHERE job_id=".$param1)->row()->jname;
		//$data['job_id']         = $param1;
		$data['jwork_status']       = '1';
        $data1['job_status']       = '2';
		$data['jassign_date']       = date('Y-m-d');
		$data['jassign_to_work_id']  = $param2;
		$data['jassign_amt']       =  $param3;	
		$this->db->where('job_id', $param1);
		$this->db->update('jobs', $data);
		$this->db->where('job_id', $param1);
		$this->db->update('job_bids', $data1);
		 $this->session->set_flashdata('flash_message','Job Assigned Successfully');
		redirect(base_url() . 'home/jobs', 'refresh');		
	}
	
	function pprofile_view($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$this->db->select('*'); // Select field
		$this->db->from('users'); // from Table1
		$this->db->join('user_profiles','users.user_id = user_profiles.user_id','INNER'); // Join table1 with table2 based on the foreign key
		$this->db->where('users.user_id',$param1); 
		
		// Set Filter
		$page_data['user_profile'] = $this->db->get()->result_array();
		$this->db->order_by("job_id", "desc");
		$this->db->limit(20);
		$page_data['job_desp'] = $this->db->get_where('jobs', array('jaccept_workid' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pprofile_view';
        $this->load->view('static/index',$page_data);
	}
	function credits($param1 = '',$param2 = '')
	{
		session_start();
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    	$amount = $this->input->post('credits');
    	$_SESSION['credit_amount'] = $amount;
            
    	$title = $this->input->post('ucredits');
    	redirect(base_url() . 'home/invoice/make_payment/'.$title.'/'.$amount.'/', 'refresh');		
		
	}
	function stripe($param1 = '',$param2 = '')
	{
		try {

	require_once(APPPATH.'libraries/Stripe/lib/Stripe.php');
	Stripe::setApiKey("sk_test_76rvqe8M1VH2uCa1sjVfL0Au");

	$charge = Stripe_Charge::create(array(
  "amount" => $param1,
  "currency" => "usd",
  "card" => $_POST['stripeToken'],
  "description" => "Charge for Facebook Login code."
));
	//send the file, this line will be reached if no error was thrown above
	echo "<h1>Your payment has been completed. We will send you the Facebook Login code in a minute.</h1>";


  //you can send the file to this email:
  echo $_POST['stripeEmail'];
}

catch(Stripe_CardError $e) {
	
}

//catch the errors in any way you like

 catch (Stripe_InvalidRequestError $e) {
  // Invalid parameters were supplied to Stripe's API

} catch (Stripe_AuthenticationError $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)

} catch (Stripe_ApiConnectionError $e) {
  // Network communication with Stripe failed
} catch (Stripe_Error $e) {

  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {

  // Something else happened, completely unrelated to Stripe
}
	}
	function pjob_conform($param1 = '',$param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
				$user_id =  $this->session->userdata('user_id');
				$job_amount	= $this->input->post('amount');
				$jid = $this->input->post('job_id');
				$poster = $this->input->post('poster');
				$data['user_id'] = $user_id;
                   $bidding_type = $this->db->query("SELECT * FROM jobs WHERE job_id=".$jid)->row()->jbidding_type;
                   $owner_data   = $this->db->query("SELECT * FROM users WHERE user_id=".$user_id)->row();
		
		$data['job_id']       = $this->input->post('job_id');	
		$job_amount	= $this->input->post('amount');
		$bt_merchant_id	=	$this->db->get_where('sitesettings' , array('type'=>'bt_merchant_id'))->row()->description;
		 $bt_public_key	=	$this->db->get_where('sitesettings' , array('type'=>'bt_public_key'))->row()->description;
		 $bt_private_key	=	$this->db->get_where('sitesettings' , array('type'=>'bt_private_key'))->row()->description;
		 $bt_cse	=	$this->db->get_where('sitesettings' , array('type'=>'bt_cse'))->row()->description;
		 $bt_envi	=	$this->db->get_where('sitesettings' , array('type'=>'bt_envi'))->row()->description;
Braintree_Configuration::environment($bt_envi);
Braintree_Configuration::merchantId($bt_merchant_id);
Braintree_Configuration::publicKey($bt_public_key);
Braintree_Configuration::privateKey($bt_private_key);
$result = Braintree_Transaction::sale(array(
    "amount" => $job_amount,
    "creditCard" => array(
        "number" => $_POST["number"],
        "cvv" => $_POST["cvv"],
        "expirationMonth" => $_POST["month"],
        "expirationYear" => $_POST["year"]
    ),
    "options" => array(
        "submitForSettlement" => true
    )
));

if ($result->success) {
			$data['job_name']= $this->db->query("SELECT * FROM jobs WHERE job_id=".$jid)->row()->jname;
			
			$data['job_name']   =$data['job_name'];	
			$data['transaction_id']   = $result->transaction->id;
			$data['status']            = 'Success';
			$data['amount']   = $job_amount;
			$data['payment_timestamp'] = date("Y-m-d");
			$data['payment_method']    = 'Braintree';
			$this->db->insert('invoice', $data);
			$data1['jaccept_amt']       = $job_amount;
			$data1['jconformed_date']       = date('Y-m-d');
			$data_bids['job_status']       = '4';
			            $this->db->where('job_id', $jid);
                        $this->db->where('userid', $poster);
						$this->db->update('job_bids', $data_bids);
		$data['email']   = $this->db->query("SELECT * FROM users WHERE user_id=".$user_id)->row()->emailid;
		$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
			$this->email_model->paypal_success($data);
		$this->session->set_flashdata('flash_message','Payment Successful');
		redirect(base_url() . 'home/jobs', 'refresh');	
}
 else {
    $this->session->set_flashdata('flash_message','Payment Successful');
		redirect(base_url() . 'home/dash_poster', 'refresh');
}
		
	}
	
	function pjob_completed($param1 = '',$param2 = '',$param3 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if($param1 == 'update')
		{
			        $data1['jcomplete_date']       = date('Y-m-d');
			 		$data1['jcomplete_amt']       = $this->input->post('amt');
					$data1['jfeedback']       = $this->input->post('jfeedback');
					$data1['jwork_status']       = $this->input->post('jwork_status');
                                        $data_bid['job_status']       = $this->input->post('jwork_status');
					$data1['jposter_fav']       = $this->input->post('jposter_fav');
					$data1['jstar_rate']       = $this->input->post('jstar_rate');
					$this->db->where('job_id',  $this->input->post('job_id'));
					$this->db->update('jobs', $data1);
 $data['workid'] =$this->db->query("SELECT * FROM jobs WHERE job_id=".$data['job_id'])->row()->jaccept_workid;
                                        $this->db->where('job_id',  $this->input->post('job_id'));
                                        $this->db->where('userid',  $data['workid']);
					$this->db->update('job_bids', $data_bid);
					$data['job_id'] =$this->input->post('job_id');
					$data['jobname'] =$this->db->query("SELECT * FROM jobs WHERE job_id=".$data['job_id'])->row()->jname;
					$data['workid'] =$this->db->query("SELECT * FROM jobs WHERE job_id=".$data['job_id'])->row()->jaccept_workid;
					$data['emailid'] =$this->db->query("SELECT * FROM users WHERE user_id=".$data['workid'])->row()->emailid;
					//$data['email'] =$this->db->query("SELECT * FROM jobs WHERE job_id=".$data['job_id'])->row()->jname;	
					$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}					
					$this->email_model->job_complete($data1,$data);
					 $this->session->set_flashdata('flash_message','Job Completed Successfully');
			redirect(base_url() . 'home/jobs', 'refresh');
		}

		$page_data['job_completed'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		// $page_data['head']='dash_header.php';
  //       $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pjob_completed';
        $this->load->view('static/index',$page_data);
	}
	
	function ppast_jobworkers()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =  $this->session->userdata('user_id');
		//$this->db->where("jcomplete_date !='0000-00-00'");
		$page_data['recent_jobs'] = $this->db->get_where('jobs', array('jwork_status' => 6,'jposter_id' => $user_id))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'ppast_jobworkers';
        $this->load->view('static/index',$page_data);
	}
	function pjobworkers($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		//$this->db->where("jcomplete_date !='0000-00-00'");
		$page_data['recent_jobs'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pjobworkers';
        $this->load->view('static/index',$page_data);
	}
	function pfavorite_jobworkers()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$page_data['recent_jobs'] = $this->db->get_where('jobs', array('jposter_id' => $user_id,'jposter_fav'=>1,'jwork_status' => 6))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pfavorite_jobworkers';
        $this->load->view('static/index',$page_data);
	}
	
	function pview_jobworkers()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$page_data['users'] = $this->db->get_where('users', array('user_type' => 1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pview_jobworkers';
        $this->load->view('static/index',$page_data);
	}
	
	function directassign()
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$this->db->order_by("job_id", "desc");
		$this->db->where('jwork_status !=', '5');
		$this->db->where('jwork_status !=', '6'); 		
		$page_data['recent_jobs']   = $this->db->get_where('jobs', array('jbidding_type' => 2,'jposter_id' => $user_id))->result_array();
		$user_id =  $this->session->userdata('user_id');
		$page_data['users']   = $this->db->get_where('jobs', array('jposter_fav' => 1,'jposter_id' => $user_id))->result_array();
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pdirectassign';
        $this->load->view('static/index',$page_data);
    }
	
	function whofirst()
    {
    	if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$this->db->order_by("job_id", "desc");
		$this->db->where('jwork_status !=', '5');
		$this->db->where('jwork_status !=', '6');
		$page_data['recent_jobs']   = $this->db->get_where('jobs', array('jbidding_type' => 3,'jposter_id' => $user_id))->result_array();
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'pwhofirst';
        $this->load->view('static/index',$page_data);
    }
	
	function pjob_cancel($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		if(!empty($param1)){
		$data1['jcancel_date']       = date('Y-m-d');
		$data1['jwork_status']       = '5';
		$this->db->where('job_id', $param1);
		$this->db->update('jobs', $data1); 
		 $this->session->set_flashdata('flash_message','Job Canceled');
		redirect(base_url() . 'home/dashboard', 'refresh');

		}
		redirect(base_url() . 'home/dashboard', 'refresh');
	}
	
	
	function wrecent_jobs()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$this->db->order_by("job_id", "desc");
		$page_data['recent_jobs']   = $this->db->get_where('jobs', array('jwork_status' => 1,'jbidding_type !=' => 2),10)->result_array();
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wrecent_jobs';
        $this->load->view('static/index',$page_data);
	}
	
	function wold_jobs()
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$this->db->order_by("job_id", "desc");
		$this->db->where('jwork_status', '1'); 
		$page_data['recent_jobs'] = $this->db->get('jobs')->result_array();			
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wold_jobs';
        $this->load->view('static/index',$page_data);
	}
	
	function wjob_view($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$page_data['job_view'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wjob_view';
        $this->load->view('static/index',$page_data);
	}
	
	function wjob_bid($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		
		$page_data['job_bid'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wjob_bid';
        $this->load->view('static/index',$page_data);
	}
	
	function wbid($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$this->db->trans_start();
    		$job=$this->input->post('job_id');
        		if(!empty($job)){
				
				$first_bid = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jbidding_type;
				
				if($first_bid=='3'){
					$data1['jassign_date']       = date('Y-m-d');
			 		$data1['jassign_amt']       = $this->input->post('bid_price');
			   		$data1['jwork_status']       = 2;
				        
					$data1['jassign_to_work_id']       = $this->session->userdata('user_id');
					$this->db->where('job_id', $job);
					$this->db->update('jobs', $data1); 

                                $data1['jname'] = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jname;
                                $data1['bid_amt']       = $this->input->post('bid_price');
				$poster_id = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jposter_id;
				$data1['poster_email'] = $this->db->query("SELECT * FROM users WHERE user_id=".$poster_id)->row()->emailid;
				$data1['jassign_to_work_id']       = $this->session->userdata('user_id');
				$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
				$this->email_model->job_bid($data1);	
				$this->session->set_flashdata('flash_message','Bid Completed');
				redirect(base_url() . 'home/dash_worker', 'refresh');
				}
                if($first_bid=='1'){

                                $data['job_id']       =$this->input->post('job_id');
				$data['userid']       = $this->session->userdata('user_id');
				$data['bid_amt']       = $this->input->post('bid_price');
				$data['biding_dt']       = date('Y-m-d');
                                $data['job_status']       = 1;
				$data['bid_commands']       = $this->input->post('commands');
				$this->db->insert('job_bids', $data);
				$this->db->trans_complete();
                                $data1['jassign_date']       = date('Y-m-d');
			 	$data1['jassign_amt']       = $this->input->post('bid_price');
			   	$data1['jwork_status']       = 1;	
					// $data1['jassign_to_work_id']       = $this->session->userdata('user_id');	
					$this->db->where('job_id', $job);
					$this->db->update('jobs', $data1); 
					$data1['jname'] = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jname;
					$data1['bid_amt']       = $this->input->post('bid_price');
				$poster_id = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jposter_id;
				$data1['poster_email'] = $this->db->query("SELECT * FROM users WHERE user_id=".$poster_id)->row()->emailid;
				 $data1['jassign_to_work_id']       = $this->session->userdata('user_id');
				 $site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
				$this->email_model->job_bid($data1);	
				$this->session->set_flashdata('flash_message','Bid Completed');
				redirect(base_url() . 'home/dash_worker', 'refresh');
                                }
                  if($first_bid=='2'){
                                $data['job_id']       =$this->input->post('job_id');
				$data['userid']       = $this->session->userdata('user_id');
				$data['bid_amt']       = $this->input->post('bid_price');
				$data['biding_dt']       = date('Y-m-d');
				$data['bid_commands']       = $this->input->post('commands');
				$this->db->insert('job_bids', $data);
				$this->db->trans_complete();
                                $data1['jassign_date']       = date('Y-m-d');
			 	$data1['jassign_amt']       = $this->input->post('bid_price');
			   	$data1['jwork_status']       = '1';	
					// $data1['jassign_to_work_id']       = $this->session->userdata('user_id');	
				$this->db->where('job_id', $job);
				$this->db->update('jobs', $data1); 
				$data1['jname'] = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jname;
				$data1['bid_amt']       = $this->input->post('bid_price');
				$poster_id = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jposter_id;
				$data1['poster_email'] = $this->db->query("SELECT * FROM users WHERE user_id=".$poster_id)->row()->emailid;
				 $data1['jassign_to_work_id']       = $this->session->userdata('user_id');
				 $site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
				$this->email_model->job_bid($data1);	
				$this->session->set_flashdata('flash_message','Bid Completed');
				redirect(base_url() . 'home/dash_worker', 'refresh');
                                }
				
				// $data1['jassign_to_work_id']       = $this->session->userdata('user_id');
				
			}else{
				$this->session->set_flashdata('flash_message','Something Went Wrong');
				redirect(base_url() . 'home/dash_worker', 'refresh');
			}
		
	}
	
	function wjob_assigned($param1 = '',$param2 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =  $this->session->userdata('user_id');
		
		$page_data['recent_jobs'] = $this->db->get_where('jobs',array('jassign_to_work_id' => $user_id,'jwork_status' => '2'))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wjob_assigned';
        $this->load->view('static/index',$page_data);
	}
	
	function directassign_change(){
			if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$job_id      = $this->input->post('job_id');
		$data['jassign_to_work_id']       = $this->input->post('work_id');
		$data['jwork_status']       = '2';
		$this->db->where('job_id', $job_id);
		$this->db->update('jobs', $data);
		$this->session->set_flashdata('flash_message','Successfully Updated');
			redirect(base_url() . 'home/dashboard', 'refresh');
	}
	
	function job_accept($param1 = '',$param2 = '',$param3 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    	$user_id =  $this->session->userdata('user_id');
    	
		if(($this->input->post('submit') == "Accept a Job")|| $param2 == 1)
		{
			if($param1 != '')
			{
				$job = $param1;
			}
			else
			{
			$job= $this->input->post('job_id');
			}
			if($param3 != '')
			{
				$amt = $param3;
			}
			else
			{
			$amt= $this->input->post('amount');
			}
			
		$data = array( 'jaccept_workid' => $user_id,'jaccept_date' => date('Y-m-d'),'jwork_status' => '1','jaccept_amt' => $amt );
                $data1['job_status'] = '3';
		$this->db->where('job_id', $job);
		$this->db->update('jobs', $data);
                $this->db->where('userid', $user_id);
                $this->db->where('job_id', $job);
		$this->db->update('job_bids', $data1);
		$data1['jname'] = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jname;
		$data1['jposter_id']= $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jposter_id;
		$data1['tomail'] = $this->db->query("SELECT * FROM users WHERE user_id=".$data1['jposter_id'])->row()->emailid;
		$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
		$this->email_model->job_accept($data1);	
		$this->session->set_flashdata('flash_message','Job Accepted Successfully');
		  redirect(base_url() . 'home/dash_worker', 'refresh');
		}else
		{
			if($param1 != '')
			{
				$job = $param1;
			}
			else
			{
			$job= $this->input->post('job_id');
			}
			if($param3 != '')
			{
				$amt = $param3;
			}
			else
			{
			$amt= $this->input->post('amount');
			}
			$comments = $this->input->post('comments');
		$data = array( 'jaccept_workid' => $user_id,'jreject_date' => date('Y-m-d'),'jwork_status' => '8','jreject_msg' =>$comments );
		$this->db->where('job_id', $job);
		$this->db->update('jobs', $data);
		$data1['jname'] = $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jname;
		$data1['jposter_id']= $this->db->query("SELECT * FROM jobs WHERE job_id=".$job)->row()->jposter_id;
		$data1['tomail'] = $this->db->query("SELECT * FROM users WHERE user_id=".$data1['jposter_id'])->row()->emailid;
		$site_logo	=	$this->db->get_where('sitesettings' , array('type'=>'site_logo'))->row()->description;
		if(!empty($site_logo)){
                $data['logo'] = "<img src='".base_url()."uploads/".$site_logo."' /> </a>";
            }
            else{
       $data['logo'] = "<img src='".base_url()."assets/img/logo.png'>";}
		$this->email_model->job_reject($data1);		
		$this->session->set_flashdata('flash_message','Job Rejected Successfully');
		  redirect(base_url() . 'home/dash_worker', 'refresh');
		}
		  
	}
	
	function wconfirm_towork($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		$user_id =  $this->session->userdata('user_id');		
		$page_data['recent_jobs'] = $this->db->get_where('jobs',array('jassign_to_work_id' => $user_id,'jwork_status' => '4'))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wconfirm_towork';
        $this->load->view('static/index',$page_data);
	}
	
	function wpast_jobs($param1 = '')
	{   if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		$page_data['pastjob'] = $this->db->get_where('jobs', array('jaccept_workid' => $user_id,'jcomplete_date !='=>'0000-00-00'))->result_array();				
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wpast_jobs';
        $this->load->view('static/index',$page_data);
	}
	
	function wjobworkers($param1 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
		
		$user_id =  $this->session->userdata('user_id');
		//$this->db->where("jcomplete_date !='0000-00-00'");
		$page_data['recent_jobs'] = $this->db->get_where('jobs', array('job_id' => $param1))->result_array();		
		$page_data['head']='dash_header.php';
        $page_data['foot']='dash_footer.php';
		$page_data['page_name']  = 'wjobworkers';
        $this->load->view('static/index',$page_data);
	}
	
		function wselect_categories($param1 = '')
	{		
			 if ($this->session->userdata('user_login') != 1)
       		 redirect(base_url() . 'home/login', 'refresh');
			 
			$this->db->where('category_job_type', $param1);
			$page_data['categories']   = $this->db->get('job_category')->result_array();
			$page_data['head']='dash_header.php';
			$page_data['foot']='dash_footer.php';
			$page_data['page_name']  = 'job_type_categories';
			$page_data['sel_category']  = $param1;
			$this->load->view('static/index',$page_data);
			
	}

	function wcategory_jobs($param1 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
			//$page_data['recent_jobs']   = $this->db->get_where('jobs', array('category_id' => $param1,'jwork_status' => 1,'jwork_status !=' => 2,'jbidding_ed_date >' => date("Y-m-d")))->result_array();
			$page_data['recent_jobs']   = $this->db->get_where('jobs', array('category_id' => $param1,'jwork_status' => 1,'jwork_status !=' => 2))->result_array();
			$page_data['head']='dash_header.php';
			$page_data['foot']='dash_footer.php';
			$page_data['page_name']  = 'wcategory_jobs';
			$this->load->view('static/index',$page_data);
	
	}
	function task_step1($param1 = '',$param2 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
			//$page_data['recent_jobs']   = $this->db->get_where('jobs', array('category_id' => $param1,'jwork_status' => 1,'jwork_status !=' => 2,'jbidding_ed_date >' => date("Y-m-d")))->result_array();
			$page_data['recent_jobs']   = $this->db->get_where('jobs', array('category_id' => $param1,'jwork_status' => 1,'jwork_status !=' => 2))->result_array();
			// $page_data['head']='dash_header.php';
			// $page_data['foot']='dash_footer.php';
			$page_data['category_id']  = $param1;
			$page_data['category_type']  = $param2;
			$page_data['page_name']  = 'task_step1';
			$this->load->view('static/index',$page_data);
	
	}
function task_step2($param1){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    	$data['address'] = $this->input->post('address');
    	$data['street_num']       = $this->input->post('street_num');
    	$data['jname']       = $this->input->post('jname');
    	$data['street_name']       = $this->input->post('street_name');
    	$data['city']       = $this->input->post('city');
    	$data['state']       = $this->input->post('state');
    	$data['country']       = $this->input->post('country');
    	$data['zip']       = str_replace(' ', '', $this->input->post('zip'));
    	$data['lat']       = $this->input->post('lat');
    	$data['lon']       = $this->input->post('lon');
    	$data['daddress'] = $this->input->post('daddress');
    	$data['dstreet_num']       = $this->input->post('dstreet_num');
    	$data['gender']     = $this->input->post('gender');
    	$data['ethincity']   =  $this->input->post('ethincity');
    	$data['target_veterans']   =  $this->input->post('target_veterans');
    	$data['target_twins']     = $this->input->post('target_twins');
    	$data['inclusion']    = $this->input->post('inclusion');
    	$data['exclusion']     = $this->input->post('exclusion');
    	$data['add_info'] = $this->input->post('add_info');
    	$data['title_study']     = $this->input->post('title_study');
    	$data['desc_study']     = $this->input->post('desc_study');
    	$data['duration']     = $this->input->post('duration');
    	$data['duration_types']     = $this->input->post('duration_types');
    	$data['inperson_visits']     = $this->input->post('inperson_visits');
    	$data['duration_visits']     = $this->input->post('duration_visits');
    	$data['duration_time']     = $this->input->post('duration_time');
    	$data['total_compensation']     = $this->input->post('total_compensation');
    	$data['travel_expense']     = $this->input->post('travel_expense');
    	$data['zipcode']     = $this->input->post('zipcode');
    	$data['dstreet_name']       = $this->input->post('dstreet_name');
    	$data['dcity']       = $this->input->post('dcity');
    	$data['dstate']       = $this->input->post('dstate');
    	$data['dcountry']       = $this->input->post('dcountry');
    	$data['dzip']       = str_replace(' ', '', $this->input->post('dzip'));
    	$data['dlat']       = $this->input->post('dlat');
    	$data['dlon']       = $this->input->post('dlon');
    	$data['cat_id']	   = $this->input->post('cat_id');
    	$data['task_desc']       = $this->input->post('task_desc');
    	$this->session->set_userdata('step1',$data);
    		$zip = $data['zip'];
$tasker = $this->db->query("SELECT * FROM `users` WHERE `zipcode` = '$zipcode' AND `user_type` = '1'")->result_array();
$taskerss = array();
 foreach ($tasker as $task) {
    $tasker = $this->db->query("SELECT * FROM user_profiles WHERE user_id=".$task['user_id'])->row();
     $selected = explode(',', $tasker->selected_catagories);
     $categories = $this->db->query("SELECT * FROM `job_category` WHERE `category_id` = $param1")->row();
     $cat_name= $categories->category_name;
     if (in_array($cat_name,$selected))
     {
     	$taskerss[] = $tasker->user_id;
 	 }
}
    	$page_data['zip'] = str_replace(' ', '', $this->input->post('zipcode'));
    	$page_data['sel_taskers'] = $taskerss;
    	$page_data['category_name'] = str_replace(" ", "_", $cat_name);
		$page_data['category_id']  = $this->input->post('cat_id');

		$page_data['page_name']  = 'task_step2';

		$this->load->view('static/index',$page_data);
	
	}

function task_step3($param1 = '', $param2= '', $param3 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    $bidding_type = $this->input->post('jbidding_type');
    if($bidding_type == 2)
    {			
    		$tasker = $this->db->query("SELECT * FROM `users` WHERE `user_type` = '1'")->result_array();
			$taskerss = array();
 			foreach ($tasker as $task) {
    		$tasker = $this->db->query("SELECT * FROM user_profiles WHERE user_id=".$task['user_id'])->row();
    		$selected = explode(',', $tasker->selected_catagories);
     		$categories = $this->db->query("SELECT * FROM `job_category` WHERE `category_id` = $param1")->row();
     		$cat_name= $categories->category_name;
     		if (in_array($cat_name,$selected))
     			{
     				$taskerss[] = $tasker->user_id;
 	 			}
			}
    		$page_data['zip'] = $param2;
    		$page_data['sel_taskers'] = $taskerss;
    		$page_data['category_name'] = str_replace(" ", "_", $cat_name);
			$page_data['category_id']  = $param1;
    		$page_data['recent_jobs']   = $this->db->get_where('jobs', array('category_id' => $param1,'jwork_status' => 1,'jwork_status !=' => 2))->result_array();
			$page_data['bidding_type'] = 2;
			$page_data['page_name']  = 'task_step3';
			$page_data['user_id'] =  $this->session->userdata('user_id');
			$this->load->view('static/index',$page_data);
		}
		else if($bidding_type == 3)
		{
			$page_data['bidding_type'] = 3;
			$page_data['page_name']  = 'task_step3';
			$page_data['category_id'] = $param1;
			$page_data['user_id'] =  $this->session->userdata('user_id');
			$this->load->view('static/index',$page_data);
		}
		else if($bidding_type == 1)
		{
			
			$page_data['bidding_type'] = 1;
			$page_data['page_name']  = 'task_step3';
			$page_data['category_id'] = $param1;
			$page_data['user_id'] =  $this->session->userdata('user_id');
			$this->load->view('static/index',$page_data);
		}
	
	}
	public function task_confirm($param1 = '', $param2 = '', $param3 = '', $param4 = '')
	{
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    	$step1 = $this->session->userdata('step1');
    	$user_id =  $this->session->userdata('user_id');
		$data['jposter_id']=$this->session->userdata('user_id');
		$data['jdescription'] =$step1['task_desc'];
		$data['jaddress']  = $step1['address'].$step1['zip'];
		$data['djaddress']  = $step1['daddress'].$step1['dzip'];
		$data['jzipcode'] = $step1['zip'];
		$data['djzipcode'] = $step1['dzip'];
		$data['jposted_date'] =date('Y-m-d');
		$data['jact_status']       = '1';
                $cat_id = $step1['cat_id'];
		$category_name = $this->db->query("SELECT * FROM `job_category` WHERE category_id =$cat_id")->row()->category_name;
		$data['jname'] = $category_name;
		$data['jbilling_by']       = $this->input->post('jbilling_by');
		$data['jbilling_no_hours']       = $this->input->post('jbilling_no_hours');
		$data['jbilling_no_days'] =	$this->input->post('jbilling_no_days');
		$data['jlongitude']   = $step1['lon'];
		$data['jlatitude']   = $step1['lat'];
		$data['djlongitude']   = $step1['dlon'];
		$data['djlatitude']   = $step1['dlat'];
		$data['category_id'] = $step1['cat_id'];
		$data['gender']     = $step1['gender'];
    		$data['ethincity']   =  $step1['ethincity'];
    		$data['target_veterans']   =  $step1['target_veterans'];
    		$data['target_twins']     = $step1['target_twins'];
    		$data['inclusion']    = $step1['inclusion'];
    		$data['exclusion']     = $step1['exclusion'];
    		$data['add_info'] = $step1['add_info'];
    		$data['title_study']     = $step1['title_study'];
    		$data['desc_study']     = $step1['desc_study'];
    		$data['duration']     = $step1['duration'];
    		$data['duration_types']     = $step1['duration_types'];
    		$data['inperson_visits']     = $step1['inperson_visits'];
    		$data['duration_visits']     = $step1['duration_visits'];
    		$data['duration_time']     = $step1['duration_time'];
    		$data['total_compensation']     = $step1['total_compensation'];
    		$data['travel_expense']     = $step1['travel_expense'];
    		$data['zipcode']     = $step1['zipcode'];
		$price="";
			   if($this->input->post('jbilling_by')==1) {
				    $data['jposter_price']       = $this->input->post('hour_price');
				    $price= $this->input->post('hour_price')*$this->input->post('jbilling_no_hours') ;
			   }elseif($this->input->post('jbilling_by')==2){
			   		$data['jposter_price']       = $this->input->post('day_price');
					$price=$this->input->post('day_price')*$this->input->post('jbilling_no_days');
			   }
		
		if($param4 == 2)
		{
			
			$data['jassign_to_work_id'] = $param1;
			$data['jbidding_type'] = $param4;
			$data['jassign_date']      = date('Y-m-d');
		    
			$data['jwork_status']     = '2';	
			if($this->input->post('jbilling_by')==1) {
				    $data['jposter_price']       = $this->input->post('price');
				    $price= $this->input->post('price')*$this->input->post('jbilling_no_hours') ;
			   }elseif($this->input->post('jbilling_by')==2){
			   		$data['jposter_price']       = $this->input->post('price');
					$price=$this->input->post('price')*($this->input->post('jbilling_no_days')*8);
			   }
			   $data['jassign_amt']       = $price;

		}
		if($this->input->post('bidding_type') == 1)
		{
			$data['jbidding_st_date'] = date("Y-m-d", strtotime($this->input->post('jbidding_st_date')));
			$data['jbidding_ed_date'] = date("Y-m-d", strtotime($this->input->post('jbidding_ed_date')));
			$data['jbidding_type'] = '1';
			$data['jwork_status']      = '1';
		}
		if($this->input->post('bidding_type') == 3)
		{
			
			$data['jbidding_type'] = '3';
			$data['jwork_status']      = '1';
		}
		$jobss = $this->db->query("SELECT * FROM `users` WHERE `user_id` = $user_id")->row()->jobs_pending;
		$data1['jobs_pending'] = $jobss-1;
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data1);
		$this->db->insert('jobs', $data);
		 $zip = $step1['zip'];
		$zipDB = $this->db->query("SELECT *FROM `users` WHERE `zipcode` ='".$zip."' AND `user_type` = 1")->result_array();
		 	  foreach ($zipDB as $zips) 
		 	  {
			  	$this->email_model->notification($zips);
			  }

	  			$this->session->set_flashdata('flash_message','Job posted Successfully');
				redirect(base_url() . 'home/dash_poster', 'refresh');

	}
	function sort_tasker(){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
    	$price = $this->input->post('price');
    	$date       = $this->input->post('date');
    	$time       = $this->input->post('time');
    	$page_data['zip'] = $data['zip'];
		$page_data['category_id']  = $this->input->post('cat_id');
		$page_data['page_name']  = 'task_step2';

		$this->load->view('static/index',$page_data);
	
	}

	// function testEmail()
	// {
	// 	$zip = 560099;
	// 	$zipDB = $this->db->query("SELECT *FROM `users` WHERE `zipcode` =".$zip." AND `user_type` = 1")->result_array();
	//   foreach ($zipDB as $zips) 
	//   {
	//   	$this->email_model->notification($zips);
	//   }
	// }
		
	
		// $sa= $this->input->post('sub_categroy');
		// 		if(empty($sa)){
		// 			 $data['category_id']       = $this->input->post('category');
		// 		}else {
		// 			 $data['category_id']       = $this->input->post('sub_categroy');
		// 		}
		// 	   $data['jposter_id']=$this->session->userdata('user_id');
		// 	   $data['jposted_date'] =date('Y-m-d');
		// 	   $data['jname'] = $this->input->post('jname');
		// 	   $data['jdescription'] =$this->input->post('jdescription');
		// 	   $data['jact_status']       = '1';			
		// 	   $data['jbilling_by']       = $this->input->post('jbilling_by');
		// 	   $data['jbilling_no_hours']       = $this->input->post('jbilling_no_hours');
		// 	   $data['jbilling_no_days'] =$this->input->post('jbilling_no_days');
		// 	   $price="";
		// 	   if($this->input->post('jbilling_by')==1) {
		// 		    $data['jposter_price']       = $this->input->post('hour_price');
		// 		    $price= $this->input->post('hour_price')*$this->input->post('jbilling_no_hours') ;
		// 	   }elseif($this->input->post('jbilling_by')==2){
		// 	   		$data['jposter_price']       = $this->input->post('day_price');
		// 			$price=$this->input->post('day_price')*$this->input->post('jbilling_no_days');
		// 	   }
		// 	  $data['jbidding_type']    = $this->input->post('jbidding_type');
		// 	  $data['jbidding_st_date'] =$this->input->post('jbidding_st_date');
		// 	  $data['jbidding_ed_date'] = $this->input->post('jbidding_ed_date');
		// 	  $data['jassign_to_work_id'] = $this->input->post('jassign_to_work_id');
		// 	  if($this->input->post('jbidding_type')=='2'){
		// 	  	$data['jassign_date']      = date('Y-m-d');
		// 	  	$data['jassign_amt']       = $price;
		// 	  	$data['jwork_status']     = '2';	
		// 	  }else {
		// 	  	$data['jassign_date']      = '0000-00-00';
		// 	  	$data['jassign_amt']       = '0';  
		// 	  	$data['jwork_status']      = '1';	
		// 	  }
		// 	  $data['jaccept_workid']      = '0';
		// 	  $data['jaccept_date' ]       = '0000-00-00';
		// 	  $data['jcancel_date']       =  '0000-00-00';
		// 	  $data['jcancel_msg']       =  '';
		// 	  $data['jaccept_amt']         = '0';
		// 	  $data['jconformed_date']      = '0000-00-00';
		// 	  $data['jcomplete_date']      = '0000-00-00';
		// 	  $data['jcomplete_amt']       ='0';
		// 	  $data['jfeedback']           = '';  
		// 	  $data['jposter_fav']         = '0';
		// 	  $data['jworker_fav']         = '0';
		// 	  $data['jreject_date']       =  '0000-00-00';
		// 	  $data['jreject_msg']       =  '';
		// 	  $data['jstar_rate']       =  '0';
		// 	  $data['job_pay']       =  '0';
		// 	  $data['jlongitude']   = $this->input->post('lon');
		// 	  $data['jlatitude']   = $this->input->post('lat');
		// 	  $addres = $this->input->post('address') .', '. $this->input->post('zip');
		// 	  $data['jaddress']  = $addres;
		// 	  $data['jzipcode'] = $this->input->post('zip');
		// 	  $this->db->insert('jobs', $data);
		// 	  $zip = $this->input->post('zip');

		// 	  $zipDB = $this->db->query("SELECT *FROM `users` WHERE `zipcode` =".$zip." AND `user_type` = 1")->result_array();
		// 	  foreach ($zipDB as $zips) 
		// 	  {
		// 	  	$this->email_model->notification($zips);
		// 	  }

		// 	  $this->session->set_flashdata('flash_message','job posted Successfully');
		// 		redirect(base_url() . 'home/dashboard', 'refresh');
			
		// }
	



	//Chat

	function chat($param1 = '',$param2 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
			$chats = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param1 AND `user` = $param2")->result_array();
			$page_data['page_name']  = 'chat';
			$page_data['tasker']  = $param1;
			$page_data['user']  = $param2;
			$page_data['chats']  = $chats;
			$this->load->view('static/index',$page_data);
	
	}
	function chatting(){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
			// $chats = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param1 AND `user` = $param2")->result_array();
			$page_data['page_name']  = 'chatting';
			// $page_data['tasker']  = $param1;
			// $page_data['user']  = $param2;
			// $page_data['chats']  = $chats;
			$this->load->view('static/index',$page_data);
	
	}
	function chat_task($param1 = '',$param2 = ''){
		if ($this->session->userdata('user_login') != 1)
        redirect(base_url() . 'home/login', 'refresh');
			$chats = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param1 AND `user` = $param2")->result_array();
			$page_data['page_name']  = 'chat_task';
			$page_data['tasker']  = $param1;
			$page_data['user']  = $param2;
			$page_data['jobs_assigned'] = $jobs_assigned;
			$page_data['jobs_open'] = $jobs_open;
			$page_data['jobs_first'] = $jobs_first;
			$page_data['chats']  = $chats;
			$this->load->view('static/index',$page_data);
	
	}
	function chatting_user($param1 = '',$param2 = ''){
			if ($this->session->userdata('user_login') != 1)
        	redirect(base_url() . 'home/login', 'refresh');
        $data['user'] = $param1;
        $data['tasker'] = $param2;
        $data['message'] = $this->input->post('message');
        $data['chat_at'] =date("Y-m-d H:i:s");
        $status = $this->db->query("UPDATE `chat` SET `status` = 'read' WHERE `tasker` = $param2 AND `user` = $param1");
        $data['status'] = "new";
        $this->db->insert('chat', $data);
    		$data['message'] =  $this->input->post('message');
			echo json_encode($data); 
	}
	function chatting_task($param1 = '',$param2 = ''){
			if ($this->session->userdata('user_login') != 1)
        	redirect(base_url() . 'home/login', 'refresh');
        $data['user'] = $param1;
        $data['tasker'] = $param2;
        $data['message_tasker'] = $this->input->post('message');
        $data['chat_at'] =date("Y-m-d H:i:s");
        $status = $this->db->query("UPDATE `chat` SET `status_task` = 'read' WHERE `tasker` = $param2 AND `user` = $param1");
        $data['status_task'] = "new";
        $this->db->insert('chat', $data);
    		$data['message'] =  $this->input->post('message');
			echo json_encode($data);
	}
	public function chat_task_check($param1 = ''){
		
		$user_id =$this->session->userdata('user_id');
		 $status['chat'] = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param1 AND `status`='new'")->row();
		 $zip = $this->db->query("SELECT * FROM `users` WHERE `user_id` = '$param1'")->row()->zipcode;
		 $status['jobs_assigned'] = $this->db->query("SELECT * FROM `jobs` WHERE `jassign_to_work_id` = $param1 AND `jwork_status` = 2 AND jbidding_type = 2")->num_rows();
		// $status['jobs_open'] = $this->db->query("SELECT * FROM `jobs` WHERE `jwork_status` = 1 AND jbidding_type = 1")->num_rows();
		$status['jobs_first'] = $this->db->query("SELECT * FROM `jobs` WHERE `jwork_status` = 1 AND jbidding_type = 3 ")->num_rows();
			$status['jobs_open'] = 0;
			$assigned = $this->db->query("SELECT * FROM `jobs` WHERE `jbidding_type`=1 AND `jwork_status`=1 ")->result_array();
            foreach ($assigned as $assign) {
                $job_id = $assign['job_id'];
                $bidded = $this->db->query("SELECT * FROM `job_bids` WHERE `job_id`=$job_id AND `userid`=$user_id")->row();
                if($job_id != $bidded->job_id){
                	$status['jobs_open'] = $status['jobs_open'] + 1;
                }
            }
		 
       
           echo json_encode($status); 
		}
		public function chat_user_check($param1 = ''){
		
		// $user_id =$this->session->userdata('user_id');
		 $status = $this->db->query("SELECT * FROM `chat` WHERE `user` = $param1 AND `status_task`='new'")->row();
		 
       
           echo json_encode($status); 
		}
		public function chat_user_content($param1 = '',$param2 = ''){
		 $chats = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param2 AND `user` = $param1")->result_array();
$user_img = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = $param1")->row()->user_image;
$user_name = $this->db->query("SELECT * FROM `users` WHERE `user_id` = $param1")->row()->full_name;
$tasker_img = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = $param2")->row()->user_image;
$tasker_name = $this->db->query("SELECT * FROM `users` WHERE `user_id` = $param2")->row()->full_name;

		// $user_id =$this->session->userdata('user_id');
	ob_start();

// normal output

							echo '<h3>Chat with '.$tasker_name.' </h3>	
							<div class="chat-outer">';
                            foreach ($chats as $chat) {
                                if($chat["message_tasker"]!=""){
                            echo '<div class="chat-item left clearfix">
									<div class="arrow_box">
										'.$chat["message_tasker"].'</div>
									<div class="user-info pull-left">
										';
										if($tasker_img != "") {
											echo '<img src="'. base_url().'uploads/profiles/'.$tasker_img .' " class="users">
                    ';
                     } else { echo ' <img src="'. base_url().'uploads/profiles/user-default.jpg" class="users">
                   '; } echo '
										<h5 style=""> - '.$tasker_name.'</h5>
									</div>
								</div>
								<br /> '; } if($chat["message"]!=""){ echo '
								<div class="chat-item right clearfix">
									<div class="arrow_box">
										'.$chat["message"].'
										</div>
									<div class="user-info pull-right">
'; if($user_img != "") { echo '
                    <img src="'.base_url().'uploads/profiles/'.$user_img.'" class="users">
                    '; } else { 
                    echo '<img src="'.base_url().'uploads/profiles/user-default.jpg" class="users">
                   '; } echo '
										<h5> - '. $user_name.'</h5>
									</div>
								</div>
								<br />
								'; } }
       $status = ob_get_clean();
           echo json_encode($status); 
		}
		public function chat_task_content($param1 = '',$param2 = ''){
		 $chats = $this->db->query("SELECT * FROM `chat` WHERE `tasker` = $param2 AND `user` = $param1")->result_array();
$user_img = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = $param1")->row()->user_image;
$user_name = $this->db->query("SELECT * FROM `users` WHERE `user_id` = $param1")->row()->full_name;
$tasker_img = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_id` = $param2")->row()->user_image;
$tasker_name = $this->db->query("SELECT * FROM `users` WHERE `user_id` = $param2")->row()->full_name;

		// $user_id =$this->session->userdata('user_id');
	ob_start();

// normal output

							echo '<h3>Chat with '.$user_name.' </h3>	
							<div class="chat-outer">';
                            foreach ($chats as $chat) {
                                if($chat["message"]!=""){
                            echo '<div class="chat-item left clearfix">
									<div class="arrow_box">
										'.$chat["message"].'</div>
									<div class="user-info pull-left">
										';
										if($user_img != "") {
											echo '<img src="'. base_url().'uploads/profiles/'.$user_img .' " class="users">
                    ';
                     } else { echo ' <img src="'. base_url().'uploads/profiles/user-default.jpg" class="users">
                   '; } echo '
										<h5 style=""> - '.$user_name.'</h5>
									</div>
								</div>
								<br /> '; } if($chat["message_tasker"]!=""){ echo '
								<div class="chat-item right clearfix">
									<div class="arrow_box">
										'.$chat["message_tasker"].'
										</div>
									<div class="user-info pull-right">
'; if($tasker_img != "") { echo '
                    <img src="'.base_url().'uploads/profiles/'.$tasker_img.'" class="users">
                    '; } else { 
                    echo '<img src="'.base_url().'uploads/profiles/user-default.jpg" class="users">
                   '; } echo '
										<h5> - '. $tasker_name.'</h5>
									</div>
								</div>
								<br />
								'; } }
       $status = ob_get_clean();
           echo json_encode($status); 
		}
}